const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const contacts = require("./routes/api/contact");
// const contacts = require("./routes/api/mongoose");
const cors = require("cors");
const mongoose = require("mongoose");
const logger = require("morgan");
const config = require("config");

// const mongoURL = config.get("mongoURL");

// mongoose.connect(mongoURL, { useNewUrlParser: true }, err => {
//   if (err) throw err;
//   console.log("Successfully connected to mongodb");
// });

app.use(bodyParser.json());
app.use(cors());

app.use(
	bodyParser.urlencoded({
		extended: true
	})
);

app.use(logger("common"));
app.use("/api/contacts/", contacts);

const port = process.env.port || 5000;

if (process.env.NODE_ENV !== "test")
	app.listen(port, () => console.log(`Server listening on port ${port}`));

module.exports = app;
