const express = require("express");
const router = express.Router();
const pg = require("pg-promise")();
const db = pg("postgre://postgres:123456@localhost:5432/contacts");

router.get("/all", async (req, res) => {
	const result = await db.any("select * from contacts_info");
	res.status(200).json({
		status: 200,
		data: result,
		message: "Retrieved all contacts successfully"
	});
});

router.post("/new", async (req, res) => {
	const result = await db.any(
		"insert into contacts_info(name,phone,email) values(${name},${phone},${email}) returning id",
		req.body
	);
	res.status(200).json({
		status: 200,
		data: result,
		message: "Created one contact successfully"
	});
});

router.put("/update/:id", async (req, res) => {
	const id = req.params.id;
	await db.any(
		`update contacts_info set name = '${req.body.name}', phone = '${
			req.body.phone
		}', email = '${req.body.email}' where id = '${id}'`
	);
	res.status(200).json({
		status: 200,
		message: "Updated one contact successfully"
	});
});

router.delete("/delete/:id", async (req, res) => {
	const id = req.params.id;
	await db.any(`delete from contacts_info where id = '${id}'`);
	res.status(200).json({
		status: 200,
		message: "Deleted one contact successfully"
	});
});

module.exports = router;
