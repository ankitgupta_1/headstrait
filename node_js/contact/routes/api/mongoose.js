const router = require("express").Router();

const Contacts = require("../../models/Contact");

router.get("/", (req, res) => {
  res.json({ message: "App is running" });
});

router.get("/all", async (req, res) => {
  const result = await Contacts.find();
  res.status(200).json({
    status: 200,
    data: result,
    message: "All contacts retrieved successfully"
  });
});

router.post("/new", async (req, res) => {
  let contact = new Contacts();
  contact.name = req.body.name;
  contact.phone = req.body.phone;
  contact.email = req.body.email;

  const result = await contact.save();

  res.status(200).json({
    status: 200,
    data: result,
    message: "Created one contact successfully"
  });
});

router.put("/update/:id", async (req, res) => {
  await Contacts.findOneAndUpdate(
    { _id: req.params.id },
    {
      name: req.body.name,
      phone: req.body.phone,
      email: req.body.email
    }
  );

  res.status(200).json({
    status: 200,
    message: "Updated one contact successfully"
  });
});

router.delete("/delete/:id", async (req, res) => {
  await Contacts.findOneAndDelete({ _id: req.params.id });

  res.status(200).json({
    status: 200,
    message: "Deleted one contact successfully"
  });
});

module.exports = router;
