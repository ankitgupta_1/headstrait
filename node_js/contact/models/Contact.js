const mongoose = require("mongoose");

const contactSchema = mongoose.Schema(
  {
    name: { type: String, required: true },
    phone: { type: Number, required: true },
    email: { type: String, required: true }
  },
  { collection: "contacts_info" }
);

const Contacts = mongoose.model("Contacts", contactSchema);

module.exports = Contacts;
