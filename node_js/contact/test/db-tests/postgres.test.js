const pg = require("pg-promise")();
const db = pg("postgre://postgres:123456@localhost:5432/contacts");

describe("Testing postgres DB after INIT", () => {
  it("should have a contact_info table", async () => {
    var data = await db.any("select * from contacts_info limit 1");
    expect(data).toStrictEqual(expect.any(Array));
  });
});
