const request = require("supertest");

const app = require("../../index");

describe("Testing Contacts API", () => {
  // GET Method
  it("should return status code 200 for GET method, the body should be an object, a message in the body, the data should be an array", done => {
    request(app)
      .get("/api/contacts/all")
      .then(response => {
        expect(response.statusCode).toBe(200);
        expect(response.body).toEqual(expect.any(Object));
        expect(response.body.data).toEqual(expect.any(Array));
        expect(response.body.message).toBe(
          "All contacts retrieved successfully"
        );
        done();
      });
  });
  let id;
  //  POST method
  it("should return status code 200 for POST method, the body should be an object, a message in the body, the data should be an array", done => {
    const data = {
      name: "Ankit Gupta",
      phone: 79621003645,
      email: "ankit@test.com"
    };
    const payload = JSON.stringify(data);
    request(app)
      .post("/api/contacts/new")
      .send(payload)
      .set("Content-type", "application/JSON")
      .then(response => {
        id = response.body.data._id;
        expect(response.statusCode).toBe(200);
        expect(response.body).toEqual(expect.any(Object));
        expect(response.body.data).toEqual(expect.any(Object));
        expect(response.body.message).toBe("Created one contact successfully");
        done();
      });
  });

  //  PUT method
  it("should return status code 200 for PUT method, the body should be an object, a message in the body, the data should be an array", done => {
    const data = {
      name: "Ankit G",
      phone: 79621003645,
      email: "ankit@test.com"
    };
    const payload = JSON.stringify(data);
    request(app)
      .put("/api/contacts/update/" + id)
      .send(payload)
      .set("Content-type", "application/JSON")
      .then(response => {
        expect(response.statusCode).toBe(200);
        expect(response.body).toEqual(expect.any(Object));
        expect(response.body.message).toBe("Updated one contact successfully");
        done();
      });
  });

  //  DELETE method
  it("should return status code 200 for DELETE method, the body should be an object, a message in the body, the data should be an array", done => {
    request(app)
      .delete("/api/contacts/delete/" + id)
      .then(response => {
        expect(response.statusCode).toBe(200);
        expect(response.body).toEqual(expect.any(Object));
        expect(response.body.message).toBe("Deleted one contact successfully");
        done();
      });
  });
});
