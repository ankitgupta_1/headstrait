drop database if exists contacts;

create database contacts;

\c contacts;

create table contacts_info(id serial, name varchar, email varchar, phone varchar);