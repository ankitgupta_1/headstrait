const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const postgres = require("./routes/api/postgres_products");
const cors = require("cors");
const logger = require("morgan");
const config = require("config");

const error = require("./middlewares/error");

app.use(bodyParser.json());
app.use(cors());
app.use(
	bodyParser.urlencoded({
		extended: false
	})
);

app.use(logger("common"));
app.use("/api/products/", postgres);

app.use(error);

const port = process.env.port || 5001;

if (process.env.NODE_ENV !== "test")
	app.listen(port, () =>
		console.log(`Server is listening at http://localhost:${port}`)
	);

module.exports = app;
