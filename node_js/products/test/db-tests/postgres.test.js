const config = require("config");
const postgresURL = config.get("postgresURL");

const pg = require("pg-promise")();
const db = pg("postgre://postgres@localhost:5432/products");

describe("Testing Postgres DB", () => {
	it("should have products table", async () => {
		let data = await db.any("select * from products limit 1");
		expect(data).toStrictEqual(expect.any(Array));
	});
	it("should have columns id, name, description, price category", async () => {
		let data = await db.any(
			"select id, name, description, price, category from products limit 1"
		);
		expect(data).toStrictEqual(expect.any(Array));
	});
	// it('should have id and price of datatype integer and name, description and category of daatatype character', async () => {
	//     let data = await db.any("select column_name, data_type from information_schema")
	// })
});
