const express = require("express");
const router = express.Router();
const pg = require("pg-promise")();
const config = require("config");

const postgresURL = config.get("postgresURL");
const db = pg("postgre://postgres@localhost:5432/products");

router.get("/all", async (req, res) => {
	const result = await db.any("select * from products");
	res.status(200).json({
		status: 200,
		data: result,
		message: "All products retrieved successfully"
	});
});

router.get("/:id", async (req, res, next) => {
	try {
		const result = await db.any(
			`select * from products where id = ${req.params.id}`
		);
		if (result < 1)
			throw {
				statusCode: 404,
				customMessage: "Cannot find product for given id"
			};
		res.status(200).json({
			status: 200,
			data: result,
			message: "All products retrieved successfully"
		});
	} catch (err) {
		next(err);
	}
});

router.post("/new", async (req, res, next) => {
	try {
		if (
			req.body.name == "" ||
			req.body.price == "" ||
			req.body.category == "" ||
			req.body.name == " " ||
			req.body.price == " " ||
			req.body.category == " "
		)
			throw {
				statusCode: 404,
				customMessage: "Details are not correct"
			};
		const result = await db.any(
			"insert into products(name, description, price, category) values(${name},${description},${price},${category}) returning id, name, description, price, category",
			req.body
		);
		res.status(200).json({
			status: 200,
			data: result,
			message: "Created one product successfully"
		});
	} catch (err) {
		next(err);
	}
});

router.put("/update/:id", async (req, res, next) => {
	const id = req.params.id;
	try {
		if (
			req.body.name == "" ||
			req.body.price == "" ||
			req.body.category == "" ||
			req.body.name == " " ||
			req.body.price == " " ||
			req.body.category == " "
		)
			throw {
				statusCode: 404,
				customMessage: "Details are not correct"
			};
		let result = await db.any(
			`update products set name = '${req.body.name}', description = '${
				req.body.description
			}', price = '${req.body.price}', category = '${
				req.body.category
			}' where id = '${id}' returning id`
		);
		if (result < 1)
			throw {
				statusCode: 404,
				customMessage: "Cannot find product for given id"
			};
		res.status(200).json({
			status: 200,
			message: "Updated one product successfully"
		});
	} catch (err) {
		next(err);
	}
});

router.delete("/delete/:id", async (req, res, next) => {
	const id = req.params.id;
	try {
		let result = await db.any(
			`delete from products where id = '${id}' returning id`
		);
		console.log(result);
		if (result < 1)
			throw {
				statusCode: 404,
				customMessage: "Cannot find product for given id"
			};
		res.status(200).json({
			status: 200,
			message: "Deleted one product successfully"
		});
	} catch (err) {
		next(err);
	}
});

module.exports = router;
