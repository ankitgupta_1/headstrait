drop database if exists products;

create database products;

\c products;

create table products_info
(
    id serial primary key,
    name varchar(30) not null,
    description varchar(200),
    price integer not null,
    category varchar(30) not null
)

create table user_details
(
    id serial primary key,
    email varchar(30) not null,
    password varchar(30) not null,
    role varchar(30) not null default 0
)