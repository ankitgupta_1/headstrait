const express = require("express");
const app = express();

app.get("/", (req, res) => res.send("Hello this is express"));
app.get("/name", (req, res) => res.send("Hey Ankit, this is express page"));
app.get("/email", (req, res) => res.send("Hey ankit@mail.com this is express"));

app.listen(5000, () => console.log("Server is listening on port 5000"));
