import {
	GET_PLAYER_INFO,
	GET_PLAYER_TIMELINE,
	GET_PLAYER_STRIKERATE
} from "../Type";
import * as actions from "../cricket";
import moxios from "moxios";
import configureMockStore from "redux-mock-store";
import thunk from "redux-thunk";

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe("Testing all the actions for player", () => {
	beforeEach(() => {
		moxios.install();
	});
	afterEach(() => {
		moxios.uninstall();
	});
	it("should check for action type GET_PLAYER_INFO", () => {
		const responseOfApi = {};
		moxios.stubRequest("http://192.168.1.28:5000/api/players/16600", {
			status: 200,
			response: responseOfApi
		});
		const store = mockStore({});
		const expectedActions = [
			{
				type: GET_PLAYER_INFO,
				payload: responseOfApi
			}
		];
		return store.dispatch(actions.getPlayerInfo()).then(() => {
			expect(store.getActions()).toStrictEqual(expectedActions);
		});
	});
	it("should check for action type GET_PLAYER_TIMELINE", () => {
		const responseOfApi = [{}, {}, {}];
		const name = { name: "" };
		moxios.stubRequest("http://192.168.1.28:5000/api/players/timeline", {
			status: 200,
			response: responseOfApi
		});
		const store = mockStore({});
		const expectedActions = [
			{
				type: GET_PLAYER_TIMELINE,
				payload: responseOfApi
			}
		];
		return store.dispatch(actions.getPlayerTimeline()).then(() => {
			expect(store.getActions()).toStrictEqual(expectedActions);
		});
	});
	it("should check for action type GET_PLAYER_STRIKERATE", () => {
		const responseOfApi = "";
		const strikeRate = { runs: "", balls: "" };
		moxios.stubRequest("http://192.168.1.28:5000/api/players/strike-rate", {
			status: 200,
			response: responseOfApi
		});
		const store = mockStore({});
		const expectedActions = [
			{
				type: GET_PLAYER_STRIKERATE,
				payload: responseOfApi
			}
		];
		return store.dispatch(actions.getPlayerStrikerate()).then(() => {
			expect(store.getActions()).toStrictEqual(expectedActions);
		});
	});
});
