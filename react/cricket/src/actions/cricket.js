import {
	GET_PLAYER_INFO,
	GET_PLAYER_TIMELINE,
	GET_PLAYER_STRIKERATE
} from "./Type";
import axios from "axios";

const url = "http://192.168.1.28:5000/api/players/";

export const getPlayerInfo = () => dispatch => {
	return axios
		.get(url + "16600")
		.then(res => {
			dispatch({
				type: GET_PLAYER_INFO,
				payload: res.data.data
			});
		})
		.catch(err => {
			console.log(err);
		});
};
export const getPlayerTimeline = () => dispatch => {
	const name = { name: "R Dravid" };
	return axios
		.post(url + "timeline", name)
		.then(res => {
			dispatch({
				type: GET_PLAYER_TIMELINE,
				payload: res.data.data
			});
		})
		.catch(err => {
			console.log(err);
		});
};
export const getPlayerStrikerate = () => dispatch => {
	const strikeRate = { runs: 7589, balls: 12684 };
	return axios
		.post(url + "strike-rate", strikeRate)
		.then(res => {
			dispatch({
				type: GET_PLAYER_STRIKERATE,
				payload: res.data.strikeRate
			});
		})
		.catch(err => {
			console.log(err);
		});
};
