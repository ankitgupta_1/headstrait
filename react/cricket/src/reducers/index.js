import { combineReducers } from "redux";
import cricket from "./cricket";

export default combineReducers({
	cricketReducer: cricket
});
