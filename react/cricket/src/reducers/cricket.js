import {
	GET_PLAYER_INFO,
	GET_PLAYER_TIMELINE,
	GET_PLAYER_STRIKERATE
} from "../actions/Type";

const initialState = {
	playerInfo: {},
	playerTimeline: [],
	strikeRate: ""
};

export default function(state = initialState, action) {
	switch (action.type) {
		case GET_PLAYER_INFO:
			return {
				...state,
				playerInfo: action.payload
			};
		case GET_PLAYER_TIMELINE:
			return {
				...state,
				playerTimeline: action.payload
			};
		case GET_PLAYER_STRIKERATE:
			return {
				...state,
				strikeRate: action.payload
			};
		default:
			return state;
	}
}
