import {
	GET_PLAYER_INFO,
	GET_PLAYER_TIMELINE,
	GET_PLAYER_STRIKERATE
} from "../../actions/Type";
import cricketReducer from "../cricket";

// Test for GET_PLAYER_INFO
describe("Testing cricket reducer for GET_PLAYER_INFO", () => {
	//with initialState
	it("should return a state object with cricket player info equal to the payload in the action of action type GET_PLAYER_INFO", () => {
		const action = {
			type: GET_PLAYER_INFO,
			payload: {}
		};
		const returnedState = cricketReducer(undefined, action);
		expect(returnedState).toEqual({
			playerInfo: action.payload,
			playerTimeline: [],
			strikeRate: ""
		});
	});
});
describe("Testing cricket reducer for GET_PLAYER_TIMELINE", () => {
	//with initialState
	it("should return a state object with cricket player info equal to the payload in the action of action type GET_PLAYER_INFO", () => {
		const action = {
			type: GET_PLAYER_TIMELINE,
			payload: [{}, {}, {}]
		};
		const returnedState = cricketReducer(undefined, action);
		expect(returnedState).toEqual({
			playerInfo: {},
			playerTimeline: action.payload,
			strikeRate: ""
		});
	});
});
describe("Testing cricket reducer for GET_PLAYER_STRIKERATE", () => {
	//with initialState
	it("should return a state object with cricket player info equal to the payload in the action of action type GET_PLAYER_INFO", () => {
		const action = {
			type: GET_PLAYER_STRIKERATE,
			payload: 20
		};
		const returnedState = cricketReducer(undefined, action);
		expect(returnedState).toEqual({
			playerInfo: {},
			playerTimeline: [],
			strikeRate: action.payload
		});
	});
});
