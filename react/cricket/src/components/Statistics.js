import React, { Component } from "react";
import "../css/Statistics.css";

export default class Statistics extends Component {
	render() {
		return (
			<>
				{this.props.stats.map(stat => (
					<div className="stats-tab">
						<div className="year">
							<span>{stat.year}</span>
						</div>
						<div className="year-stats">
							<div className="year-item">
								<span className="light-text">POTM</span>
								<span className="bold-text">{stat.potm}</span>
							</div>
							<div className="year-item">
								<span className="light-text">Runs</span>
								<span className="bold-text">
									{stat.totalruns}
								</span>
							</div>
							<div className="year-item">
								<span className="light-text">4s</span>
								<span className="bold-text">
									{stat.totalfours}
								</span>
							</div>
							<div className="year-item">
								<span className="light-text">6s</span>
								<span className="bold-text">
									{stat.totalsixes}
								</span>
							</div>
							<div className="year-item">
								<span className="light-text">Overs</span>
								<span className="bold-text">
									{stat.totalballs / 6}
								</span>
							</div>
							<div className="year-item">
								<span className="light-text">Wickets</span>
								<span className="bold-text">
									{stat.totalwickets}
								</span>
							</div>
						</div>
					</div>
				))}
			</>
		);
	}
}
