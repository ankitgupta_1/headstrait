import React, { Component } from "react";
import "../css/Main.css";
import dhoni from "../dhoni.jpg";
import Statistics from "./Statistics";
import { connect } from "react-redux";
import {
	getPlayerInfo,
	getPlayerTimeline,
	getPlayerStrikerate
} from "../actions/cricket";

export class Main extends Component {
	componentWillMount() {
		this.props.getPlayerInfo();
		this.props.getPlayerTimeline();
		this.props.getPlayerStrikerate();
	}
	render() {
		return (
			<div className="parent">
				<div className="player-details">
					<span className="player-name">
						{this.props.playerInfo.name}
					</span>
					<span className="highlight">HIGHLIGHTS</span>
					<div className="highlights-tab">
						<div className="highlights-details">
							<div className="highlight-item">
								<span className="light-text">Runs</span>
								<span className="bold-text">
									{this.props.playerInfo.totalruns}
								</span>
							</div>
							<div className="highlight-item">
								<span className="light-text">Wickets</span>
								<span className="bold-text">
									{this.props.playerInfo.totalwickets}
								</span>
							</div>
							<div className="highlight-item">
								<span className="light-text">4s</span>
								<span className="bold-text">
									{this.props.playerInfo.totalsixes}
								</span>
							</div>
							<div className="highlight-item">
								<span className="light-text">6s</span>
								<span className="bold-text">
									{this.props.playerInfo.totalfours}
								</span>
							</div>
							<div className="highlight-item">
								<span className="light-text">Strike Rate</span>
								<span className="bold-text">
									{this.props.strikeRate}
								</span>
							</div>
						</div>
						<div className="highlights-details">
							<div className="highlight-item">
								<span className="light-text">D. O. B.</span>
								<span className="bold-text">07-07-1997</span>
							</div>
							<div className="highlight-item">
								<span className="light-text">Gender</span>
								<span className="bold-text">Male</span>
							</div>
							<div className="highlight-item">
								<span className="light-text">Batting Hand</span>
								<span className="bold-text">Right</span>
							</div>
							<div className="highlight-item">
								<span className="light-text">Bowling Hand</span>
								<span className="bold-text">Right</span>
							</div>
						</div>
						<img src={dhoni} />
					</div>
				</div>
				<div className="statistics-title">Statistics</div>
				<Statistics stats={this.props.playerTimeline} />
			</div>
		);
	}
}

const mapStateToProps = state => ({
	playerInfo: state.cricketReducer.playerInfo,
	playerTimeline: state.cricketReducer.playerTimeline,
	strikeRate: state.cricketReducer.strikeRate
});

export default connect(
	mapStateToProps,
	{ getPlayerInfo, getPlayerTimeline, getPlayerStrikerate }
)(Main);
