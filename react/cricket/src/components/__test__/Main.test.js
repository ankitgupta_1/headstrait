import React from "react";
import { shallow } from "enzyme";
import { Main } from "../Main";

const getPlayerInfo = jest.fn();
const getPlayerTimeline = jest.fn();
const getPlayerStrikerate = jest.fn();
const stats = [{}, {}, {}];
const mainWrapper = shallow(
	<Main
		getPlayerInfo={getPlayerInfo}
		getPlayerTimeline={getPlayerTimeline}
		getPlayerStrikerate={getPlayerStrikerate}
		stats={stats}
	/>
);

describe("Testing the highlights of the player", () => {
	it("should check no of div tags present on the page", () => {
		expect(mainWrapper.find("div").length).toBe(15);
	});
	it("should check no of img tags present on the page", () => {
		expect(mainWrapper.find("img").length).toBe(1);
	});
	it("should check no of span tags present on the page", () => {
		expect(mainWrapper.find("span").length).toBe(20);
	});
	it("should check no of player-name class present on the page", () => {
		expect(mainWrapper.find(".player-name").length).toBe(1);
	});
	it("should check no of light-text class present on the page", () => {
		expect(mainWrapper.find(".light-text").length).toBe(9);
	});
	it("should check no of bold-text class present on the page", () => {
		expect(mainWrapper.find(".bold-text").length).toBe(9);
	});
	it("should check no of highlight class present on the page", () => {
		expect(mainWrapper.find(".highlight").length).toBe(1);
	});
});
