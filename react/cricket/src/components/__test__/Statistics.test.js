import React from "react";
import { mount } from "enzyme";
import Statistics from "../Statistics";

const statisticsWrapper = mount(<Statistics />);

describe("Testing a year stats of the player", () => {
	it("should check no of div tags present on the page", () => {
		expect(statisticsWrapper.find("div").length).toBe(9);
	});
	it("should check no of span tags present on the page", () => {
		expect(statisticsWrapper.find("span").length).toBe(13);
	});
	it("should check no of year class present on the page", () => {
		expect(statisticsWrapper.find(".year").length).toBe(1);
	});
	it("should check no of light-text class present on the page", () => {
		expect(statisticsWrapper.find(".light-text").length).toBe(6);
	});
	it("should check no of bold-text class present on the page", () => {
		expect(statisticsWrapper.find(".bold-text").length).toBe(6);
	});
});
