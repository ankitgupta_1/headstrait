import React, { Component } from 'react'

export default class ThirdComponent extends Component {
    componentWillMount() {
        console.log("Component Will mount for Third Component");
    }
    componentDidMount() {
        console.log("Component Did mount for Third Component");
    }
    componentWillReceiveProps(nextProps) {
        console.log(nextProps);
        console.log("Component Will recieve props for Third Component");
    }
    componentWillUpdate() {
        console.log("Component Will update for Third Component");
    }
    componentDidUpdate() {
        console.log("Component Did update for Third Component");
    }
    render() {

        console.log("Render for Third Component");
        return (
            < div >
                <button onClick={this.props.handleButtonClick} className="btn">Click to modify</button>
            </div >
        )
    }
}
