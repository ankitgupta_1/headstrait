import React, { Component } from "react";
import "../css/FirstComponent.css";
// import SecondComponent from "./SecondComponent";
import ThirdComponent from "./ThirdComponent";

export default class FirstComponent extends Component {
	state = {
		name: "Virat Kohli",
		batsmanType: "Non Striker",
		parent: "First Component",
		inputField: ""
	};
	componentWillMount() {
		console.log("Component Will mount for First Component");
	}
	componentDidMount() {
		console.log("Component Did mount for First Component");
	}
	componentWillUpdate() {
		console.log("Component Will update for First Component");
	}
	componentDidUpdate() {
		console.log("Component Did update for First Component");
	}
	handleButtonClick = () => {
		this.setState({ parent: this.state.inputField });
		// this.setState({ inputField: "" });
	};
	handleInput = e => {
		this.setState({ [e.target.name]: e.target.value });
	};
	render() {
		console.log("Render for First Component");
		return (
			<div className="parent">
				<h2>{this.props.striker} is a Striker</h2>
				Your entered text : {this.state.parent}
				<div className="inputGroup">
					<input
						onChange={this.handleInput}
						name="inputField"
						className="inputField"
						value={this.state.inputField}
						placeholder="Change above text"
					/>
				</div>
				<ThirdComponent
					parent={this.state.parent}
					handleButtonClick={this.handleButtonClick}
				/>
				{/* <SecondComponent nonStriker={this.state.name} /> */}
			</div>
		);
	}
}
