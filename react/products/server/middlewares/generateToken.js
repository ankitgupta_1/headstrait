const jwt = require("jsonwebtoken");
const config = require("config");
module.exports = (payload, expiresIn) => {
	return jwt.sign(payload, config.get("jwtPrivateKey"), { expiresIn });
};
