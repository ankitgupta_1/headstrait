const generateToken = require("./authToken").generateToken;

module.exports.loginUser = userDetails => {
	let expiresIn = "2h";
	if (userDetails.role == 1) expiresIn = "4h";

	let token = generateToken(userDetails, expiresIn);
	return token;
};

module.exports.logout = () => {};

module.exports.isLoggedIn = () => {};
