const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const products = require("./routes/api/products");
const users = require("./routes/api/users");
const cors = require("cors");
const logger = require("morgan");
const config = require("config");

const error = require("./middlewares/error");
console.log(config.get("postgresURL"));
app.use(bodyParser.json());
app.use(cors());
app.use(
	bodyParser.urlencoded({
		extended: false
	})
);

app.use(logger("common"));
app.use("/api/products/", products);
app.use("/api/users/", users);

app.use(error);

const port = process.env.port || 5000;

if (process.env.NODE_ENV !== "test")
	app.listen(port, () =>
		console.log(`Server is listening at http://192.168.0.6:${port}`)
	);

module.exports = app;
