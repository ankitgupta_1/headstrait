const express = require("express");
const router = express.Router();
const pg = require("pg-promise")();
const config = require("config");
const bcrypt = require("bcrypt");
const validateUser = require("../../models/validation").validateUser;
const nodemailer = require("nodemailer");
const generateToken = require("../../middlewares/generateToken");
const validateToken = require("../../middlewares/validateToken");

let token;
const postgresURL = config.get("postgresURL");
const db = pg(config.get("postgresURL"));

let otp;

router.get("/all", validateToken, async (req, res, next) => {
	try {
		const result = await db.any("select * from user_info");
		if (result.length < 1)
			throw {
				statusCode: 404,
				errorMessage: "No user found"
			};
		res.status(200).json({
			status: 200,
			users: result,
			successMessage: "All users retrieved successfully"
		});
	} catch (err) {
		next(err);
	}
});

router.get("/:user_id", validateToken, async (req, res, next) => {
	try {
		const result = await db.any(
			`select * from user_info where user_id = ${req.params.user_id}`
		);

		if (result.length < 1)
			throw {
				statusCode: 404,
				errorMessage: "Cannot find product for given id"
			};
		res.status(200).json({
			status: 200,
			user: result,
			successMessage: "All products retrieved successfully"
		});
	} catch (err) {
		next(err);
	}
});

router.post("/new", async (req, res, next) => {
	try {
		let db_result = await db.any(
			`select * from user_info where user_email = '${req.body.user_email}'`
		);
		// console.log(db_result);
		if (db_result.length != 1) {
			let user_details = {
				user_name: req.body.user_name,
				user_email: req.body.user_email,
				user_phone: req.body.user_phone,
				user_password: req.body.user_password,
				isAdmin: req.body.isAdmin
			};
			// console.log(req.body);
			validateUser(user_details)
				.then(async () => {
					const salt = await bcrypt.genSalt(10);
					const user_password_hashed = await bcrypt.hash(
						user_details.user_password,
						salt
					);
					// console.log(user_details);
					await db.any(
						`insert into user_info(user_name, user_email, user_phone, user_password, isAdmin) values('${user_details.user_name}', '${user_details.user_email}', ${user_details.user_phone}, '${user_password_hashed}', ${user_details.isAdmin}) returning user_id, user_name, user_email, user_phone, user_password, isAdmin`
					);
					res.status(200).json({
						status: 200,
						successMessage: "Created one user successfully"
					});
				})
				.catch(error => {
					error.errorMessage = error.details[0].message;
					next(error);
				});
		} else {
			throw {
				statusCode: 404,
				errorMessage: "Email already exists"
			};
		}
	} catch (error) {
		next(error);
	}
});

router.put("/update/:user_id", validateToken, async (req, res, next) => {
	const user_id = req.params.user_id;
	try {
		const salt = await bcrypt.genSalt(10);
		const user_password_hashed = await bcrypt.hash(
			req.body.user_password,
			salt
		);
		// console.log(req.body);
		const result = await db.any(
			`update user_info set user_name = '${req.body.user_name}', user_email = '${req.body.user_email}', user_password = '${user_password_hashed}', isAdmin = '${req.body.isAdmin}' where user_id = '${user_id}' returning user_id`
		);
		if (result.length != 1)
			throw {
				statusCode: 404,
				errorMessage: "Cannot find user for given id"
			};
		res.status(200).json({
			status: 200,
			data: result,
			successMessage: "Updated one user successfully"
		});
	} catch (err) {
		next(err);
	}
});

router.delete("/delete/:user_id", validateToken, async (req, res, next) => {
	const user_id = req.params.user_id;
	try {
		let result = await db.any(
			`delete from user_info where user_id = '${user_id}' returning user_id`
		);
		if (result.length != 1)
			throw {
				statusCode: 404,
				errorMessage: "Cannot find user for given id"
			};
		res.status(200).json({
			status: 200,
			successMessage: "Deleted one user successfully"
		});
	} catch (err) {
		next(err);
	}
});

router.post("/login", async (req, res, next) => {
	try {
		console.log(req.body.user_email);
		const db_result = await db.any(
			`select * from user_info where user_email = '${req.body.user_email}'`
		);
		if (db_result.length != 1)
			throw {
				statusCode: 404,
				errorMessage: "Email or Password is invalid"
			};

		let compare_result = await bcrypt.compare(
			req.body.user_password,
			db_result[0].user_password
		);

		if (!compare_result)
			throw {
				statusCode: 404,
				errorMessage: "Email or Password is invalid"
			};

		let expiresIn = "2h";
		if (db_result[0].isadmin == 1) expiresIn = "4h";
		let userDetails = {
			user_id: db_result[0].user_id,
			user_name: db_result[0].user_name,
			isAdmin: db_result[0].isadmin
		};
		token = generateToken(userDetails, expiresIn);
		console.log(token);

		res.status(200).json({
			status: 200,
			successMessage: "Logged in successful",
			token: token,
			loggedInUser: userDetails
		});
	} catch (err) {
		next(err);
	}
});

router.put("/resetPassword", async (req, res, next) => {
	try {
		let email = await db.any(
			`select user_email from user_info where user_email = '${req.body.user_email}'`
		);
		console.log(email.length);
		if (email.length == 1) {
			if (req.body.user_password != req.body.user_confirmPassword)
				throw {
					statusCode: 404,
					errorMessage: "Password does not match"
				};
			const salt = await bcrypt.genSalt(10);
			const user_password_hashed = await bcrypt.hash(
				req.body.user_password,
				salt
			);
			const result = await db.any(
				`update user_info set user_password = '${user_password_hashed}' where user_email = '${req.body.user_email}' returning user_id`
			);
			if (result.length != 1)
				throw {
					statusCode: 404,
					errorMessage: "Some error occurred"
				};
			res.status(200).json({
				status: 200,
				successMessage: "Your password is updated successfully"
			});
		} else {
			throw {
				statusCode: 404,
				errorMessage: "Email not found"
			};
		}
	} catch (error) {
		next(error);
	}
});

router.post("/sendOTP", async (req, res, next) => {
	try {
		// console.log(req.body);
		// let email = await db.any(
		// 	`select user_email from user_info where user_email = '${req.body.user_email}'`
		// );
		otp = Math.floor(1000 + Math.random() * 9000);
		// if (email.length != 1)
		// 	throw {
		// 		statusCode: 404,
		// 		errorMessage: "Email is not registered"
		// 	};
		const mailPass = config.get("mailPass");
		const mailId = config.get("mailId");

		var transport = nodemailer.createTransport({
			service: "Gmail",
			auth: {
				user: mailId,
				pass: mailPass
			}
		});

		let mailResponse = await transport.sendMail({
			from: mailId,
			to: req.body.user_email,
			subject: "OTP for Reset Password",
			text:
				"Your one time password to reset password is " +
				otp +
				".\nDO not share with anyone."
		});

		console.log("message", mailResponse.messageId);
		// console.log("send otp : " + otp);
		res.status(200).json({
			status: 200,
			successMessage: "OTP mail sent successfully"
		});
	} catch (error) {
		next(error);
	}
});

router.post("/verifyOTP", async (req, res, next) => {
	try {
		// console.log("Body OTP : " + req.body.otp);
		// console.log("OTP : " + otp);
		if (otp != req.body.otp)
			throw {
				statusCode: 404,
				errorMessage: "Invalid OTP"
			};

		res.status(200).json({
			status: 200,
			successMessage: "OTP verified successfully"
		});
	} catch (error) {
		next(error);
	}
});

module.exports = router;
