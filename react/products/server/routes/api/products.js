const express = require("express");
const router = express.Router();
const pg = require("pg-promise")();
const config = require("config");
const validateProduct = require("../../models/validation").validateProduct;
const validateToken = require("../../middlewares/validateToken");

const postgresURL = config.get("postgresURL");
const db = pg(postgresURL);

router.get("/all", validateToken, async (req, res, next) => {
	try {
		const result = await db.any("select * from products_info");
		if (!result)
			throw {
				statusCode: 404,
				errorMessage: "No products found"
			};
		res.status(200).json({
			status: 200,
			products: result,
			successMessage: "All products retrieved successfully"
		});
	} catch (err) {
		next(err);
	}
});

router.get("/:product_id", validateToken, async (req, res, next) => {
	try {
		const result = await db.any(
			`select * from products_info where product_id = ${req.params.product_id}`
		);
		if (result < 1)
			throw {
				statusCode: 404,
				errorMessage: "Cannot find product for given id"
			};
		res.status(200).json({
			status: 200,
			product: result,
			successMessage: "All products retrieved successfully"
		});
	} catch (err) {
		next(err);
	}
});

router.post("/new", validateToken, async (req, res, next) => {
	try {
		let product_details = {
			product_name: req.body.product_name,
			product_price: req.body.product_price,
			product_description: req.body.product_description,
			product_quantity: req.body.product_quantity,
			product_category: req.body.product_category
		};
		validateProduct(product_details)
			.then(async val_res => {
				await db.any(
					"insert into products_info(product_name, product_description, product_price, product_category, product_quantity) values(${product_name},${product_description},${product_price},${product_category}, ${product_quantity}) returning product_id, product_name, product_description, product_price, product_category, product_quantity",
					req.body
				);
				res.status(200).json({
					status: 200,
					successMessage: "Created one product successfully"
				});
			})
			.catch(error => {
				error.errorMessage = error.details[0].message;
				next(error);
			});
	} catch (err) {
		next(err);
	}
});

router.put("/update/:product_id", validateToken, async (req, res, next) => {
	const product_id = req.params.product_id;
	try {
		let product_details = {
			product_name: req.body.product_name,
			product_price: req.body.product_price,
			product_description: req.body.product_description,
			product_quantity: req.body.product_quantity,
			product_category: req.body.product_category
		};
		validateProduct(product_details)
			.then(async val_res => {
				let result = await db.any(
					`update products_info set product_name = '${req.body.product_name}', product_description = '${req.body.product_description}', product_price = '${req.body.product_price}', product_quantity = '${req.body.product_quantity}', product_category = '${req.body.product_category}' where product_id = '${product_id}' returning product_id`
				);
				if (result < 1)
					throw {
						statusCode: 404,
						errorMessage: "Cannot find product for given id"
					};
				res.status(200).json({
					status: 200,
					successMessage: "Updated one product successfully"
				});
			})
			.catch(error => {
				// error.errorMessage = error.details[0].message;
				next(error);
			});
	} catch (err) {
		next(err);
	}
});

router.delete("/delete/:product_id", validateToken, async (req, res, next) => {
	const product_id = req.params.product_id;
	try {
		let result = await db.any(
			`delete from products_info where product_id = '${product_id}' returning product_id`
		);
		console.log(result);
		if (result < 1)
			throw {
				statusCode: 404,
				errorMessage: "Cannot find product for given id"
			};
		res.status(200).json({
			status: 200,
			successMessage: "Deleted one product successfully"
		});
	} catch (err) {
		next(err);
	}
});

module.exports = router;
