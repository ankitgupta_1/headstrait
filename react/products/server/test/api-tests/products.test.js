const request = require("supertest");

const app = require("../../index");

let id;
let token;
beforeAll(done => {
	request(app)
		.post("/api/users/login")
		.send({ user_email: "ankitgp07@gmail.com", user_password: "ankit123" })
		.end((err, res) => {
			token = res.body.token;
			done();
		});
});

describe("Testing Products API", () => {
	it("should return status code 200 for GET method, the body should be an Object, a message in the body, body should have data which should be an array", done => {
		request(app)
			.get("/api/products/all")
			.set("Authorization", "Bearer " + token)
			.set("referer", "http://localhost:5000/api/products/all")
			.then(response => {
				expect(response.statusCode).toBe(200);
				expect(response.body).toEqual(expect.any(Object));
				expect(response.body.data).toEqual(expect.any(Array));
				expect(response.body.message).toBe(
					"All products retrieved successfully"
				);
				done();
			});
	});

	it("should return status code 200 for POST method, the body should be an Object, a message in the body, body should have data which should be an array", done => {
		const data = {
			name: "One Plus 3",
			description: "Smart and Elegant look",
			price: 1679,
			category: "Smartphones",
			quantity: 100
		};
		const payload = JSON.stringify(data);
		request(app)
			.post("/api/products/new")
			.set("Authorization", "Bearer " + token)
			.set("referer", "http://localhost:5000/api/products/all")
			.send(payload)
			.set("Content-type", "application/JSON")
			.then(response => {
				id = response.body.data[0].id;
				expect(response.statusCode).toBe(200);
				expect(response.body).toEqual(expect.any(Object));
				expect(response.body.data).toEqual(expect.any(Array));
				expect(response.body.message).toBe(
					"Created one product successfully"
				);
				done();
			});
	});

	it("should return status code 400 for POST method, and a response text", done => {
		const data = {
			name: " ",
			description: "Smart and Elegant look",
			price: 16999,
			category: " ",
			quantity: 100
		};
		const payload = JSON.stringify(data);
		request(app)
			.post("/api/products/new")
			.set("Authorization", "Bearer " + token)
			.set("referer", "http://localhost:5000/api/products/new")
			.send(payload)
			.set("Content-type", "application/JSON")
			.then(response => {
				expect(response.statusCode).toBe(404);
				expect(response.text).toBe("Details are not correct");
				done();
			});
	});

	it("should return status code 200 for PUT method, the body should be an object, a message in the body", done => {
		const data = {
			name: "MI notee 4",
			description: "Gets heated",
			price: 8000,
			category: "Smartphone",
			quantity: 100
		};
		const payload = JSON.stringify(data);
		request(app)
			.put("/api/products/update/" + id)
			.set("Authorization", "Bearer " + token)
			.set("referer", "http://localhost:5000/api/products/update")
			.send(payload)
			.set("Content-type", "application/JSON")
			.then(response => {
				expect(response.statusCode).toBe(200);
				expect(response.body).toEqual(expect.any(Object));
				expect(response.body.message).toBe(
					"Updated one product successfully"
				);
				done();
			});
	});

	it("should return status code 404 for PUT method and a response text for wrong input", done => {
		const data = {
			name: "",
			description: "Gets heated",
			price: 8000,
			category: "Smartphone",
			quantity: 100
		};
		const payload = JSON.stringify(data);
		request(app)
			.put("/api/products/update/" + id)
			.set("Authorization", "Bearer " + token)
			.set("referer", "http://localhost:5000/api/products/update")
			.send(payload)
			.set("Content-type", "application/JSON")
			.then(response => {
				expect(response.statusCode).toBe(404);
				expect(response.text).toBe("Details are not correct");
				done();
			});
	});

	it("should return status code 404 for PUT method and a response text for invalid id", done => {
		const data = {
			name: "Some brand",
			description: "Gets heated",
			price: 8000,
			category: "Smartphone",
			quantity: 100
		};
		const payload = JSON.stringify(data);
		request(app)
			.put("/api/products/update/-1")
			.set("Authorization", "Bearer " + token)
			.set("referer", "http://localhost:5000/api/products/update")
			.send(payload)
			.set("Content-type", "application/JSON")
			.then(response => {
				expect(response.statusCode).toBe(404);
				expect(response.text).toBe("Cannot find product for given id");
				done();
			});
	});

	it("should return status code 200 for DELETE method, a message in the body", done => {
		request(app)
			.delete("/api/products/delete/" + id)
			.set("Authorization", "Bearer " + token)
			.set("referer", "http://localhost:5000/api/products/delete")
			.then(response => {
				expect(response.statusCode).toBe(200);
				expect(response.body.message).toBe(
					"Deleted one product successfully"
				);
				done();
			});
	});

	it("should return status code 404 for DELETE method, a message in the body", done => {
		request(app)
			.delete("/api/products/delete/-1")
			.set("Authorization", "Bearer " + token)
			.set("referer", "http://localhost:5000/api/products/delete")
			.then(response => {
				expect(response.statusCode).toBe(404);
				expect(response.text).toBe("Cannot find product for given id");
				done();
			});
	});
});
