const request = require("supertest");

const app = require("../../index");

let id;
let token;

beforeAll(done => {
	request(app)
		.post("/api/users/login")
		.send({ user_email: "ankitgp07@gmail.com", user_password: "ankit123" })
		.end((err, res) => {
			token = res.body.token;
			done();
		});
});

describe("Testing users API", () => {
	it("should return status 200, the body should be an object, the data should be an array, a message in the body", done => {
		request(app)
			.get("/api/users/all")
			.set("Authorization", "Bearer " + token)
			.set("referer", "http://localhost:5000/api/users/all")
			.then(response => {
				expect(response.statusCode).toBe(200);
				expect(response.body).toEqual(expect.any(Object));
				expect(response.body.users).toEqual(expect.any(Array));
				expect(response.body.successMessage).toBe(
					"All users retrieved successfully"
				);
				done();
			});
	});

	it("should return status 200, the body should be an object, the data should be an array, a message in the body", done => {
		request(app)
			.get("/api/users/all")
			.set("Authorization", "Bearer " + token)
			.set("referer", "http://localhost:5000/api/users/all")
			.then(response => {
				response = {
					statusCode: 404,
					text: "No user found"
				};

				expect(response.statusCode).toBe(404);
				expect(response.text).toBe("No user found");
				done();
			});
	});

	it("should return status 200, the body should be an object, the data should be an array, a message in the body", done => {
		const data = {
			user_name: "Ankit Gupta",
			user_email: "ankit@mail.com",
			user_password: "something",
			user_phone: 9784630149,
			isAdmin: true
		};
		let payload = JSON.stringify(data);
		request(app)
			.post("/api/users/new")
			.set("Authorization", "Bearer " + token)
			.set("referer", "http://localhost:5000/api/users/new")
			.send(payload)
			.set("Content-type", "application/JSON")
			.then(response => {
				console.log("db result : " + response.body);
				id = response.body.data[0].id;
				expect(response.status).toBe(200);
				expect(response.body).toEqual(expect.any(Object));
				expect(response.body.data).toEqual(expect.any(Array));
				expect(response.body.successMessage).toBe(
					"Created one user successfully"
				);
				done();
			});
	});

	it("should return status 200, a message in the body", done => {
		const data = {
			user_name: "Ankit Gupta",
			user_email: "test@testing.com",
			user_password: "correct",
			user_phone: 7896201453,
			isAdmin: true
		};
		let payload = JSON.stringify(data);
		request(app)
			.put("/api/users/update/" + id)
			.set("Authorization", "Bearer " + token)
			.set("referer", "http://localhost:5000/api/users/update")
			.send(payload)
			.set("Content-type", "application/JSON")
			.then(response => {
				expect(response.statusCode).toBe(200);
				expect(response.body.successMessage).toBe(
					"Updated one user successfully"
				);
				done();
			});
	});

	it("should return status 404, an error message", done => {
		const data = {
			user_name: "Ankit Gupta",
			user_email: "akit@mail.com",
			user_password: "something",
			isAdmin: true
		};
		let payload = JSON.stringify(data);
		request(app)
			.put("/api/users/update/1000000")
			.set("Authorization", "Bearer " + token)
			.set("referer", "http://localhost:5000/api/users/update")
			.send(payload)
			.set("Content-type", "application/JSON")
			.then(response => {
				expect(response.statusCode).toBe(404);
				expect(response.text).toBe("Cannot find user for given id");
				done();
			});
	});

	it("should return status 200, a message in the body", done => {
		console.log(id);
		request(app)
			.delete("/api/users/delete/" + id)
			.set("Authorization", "Bearer " + token)
			.set("referer", "http://localhost:5000/api/users/delete")
			.then(response => {
				expect(response.statusCode).toBe(200);
				expect(response.body.successMessage).toBe(
					"Deleted one user successfully"
				);
				done();
			});
	});

	it("should return status 404, an error message", done => {
		request(app)
			.delete("/api/users/delete/10000")
			.set("Authorization", "Bearer " + token)
			.set("referer", "http://localhost:5000/api/users/delete")
			.then(response => {
				expect(response.statusCode).toBe(404);
				expect(response.text).toBe("Cannot find user for given id");
				done();
			});
	});

	it("should return status 400 for no matching password of the user present in db, an error message in the body", done => {
		const data = {
			user_email: "test@invalid.com",
			user_password: "invalid"
		};
		let payload = JSON.stringify(data);
		request(app)
			.post("/api/users/login")
			.send(payload)
			.set("Content-type", "application/JSON")
			.then(response => {
				expect(response.statusCode).toBe(404);
				expect(response.text).toBe("Email or Password is invalid");
				done();
			});
	});

	it("should return status 400 for no matching password of the user present in db, an error message in the body", done => {
		const data = {
			user_email: "test@testing.com",
			user_password: "wrong"
		};
		let payload = JSON.stringify(data);
		request(app)
			.post("/api/users/login")
			.send(payload)
			.set("Content-type", "application/JSON")
			.then(response => {
				expect(response.statusCode).toBe(404);
				expect(response.text).toBe("Email or Password is invalid");
				done();
			});
	});

	it("should return status 200 for matching password of the user present in db, an success message in the body", done => {
		const data = {
			user_email: "test@testing.com",
			user_password: "correct"
		};
		let payload = JSON.stringify(data);
		request(app)
			.post("/api/users/login")
			.send(payload)
			.set("Content-type", "application/JSON")
			.then(response => {
				console.log(response.body);
				expect(response.statusCode).toBe(200);
				expect(response.body.successMessage).toBe(
					"Logged in successful"
				);
				done();
			});
	});
});

describe("Testing for validation while creating a new user", () => {
	it("should return error as email already exist in db", done => {
		const data = {
			user_name: "Ankit",
			user_email: "test@testing.com",
			user_password: "something",
			user_phone: 9642036574,
			isAdmin: true
		};
		let payload = JSON.stringify(data);
		request(app)
			.post("/api/users/new")
			.send(payload)
			.set("Content-type", "application/JSON")
			.then(response => {
				expect(response.statusCode).toBe(404);
				expect(response.text).toBe("Email already exists");
				done();
			});
	});

	it("should return error as name is required", done => {
		const data = {
			user_email: "akit@mail.com",
			user_password: "something",
			user_phone: 3021479635,
			isAdmin: true
		};
		let payload = JSON.stringify(data);
		request(app)
			.post("/api/users/new")
			.send(payload)
			.set("Content-type", "application/JSON")
			.then(response => {
				expect(response.statusCode).toBe(500);
				expect(response.text).toBe('"user_name" is required');
				done();
			});
	});

	it("should return error as email is required", done => {
		const data = {
			user_name: "Ankit",
			user_password: "something",
			user_phone: 9845320179,
			isAdmin: true
		};
		let payload = JSON.stringify(data);
		request(app)
			.post("/api/users/new")
			.send(payload)
			.set("Content-type", "application/JSON")
			.then(response => {
				expect(response.statusCode).toBe(500);
				expect(response.text).toBe('"user_email" is required');
				done();
			});
	});

	it("should return error as password is required", done => {
		const data = {
			user_name: "Ankit",
			user_email: "some@somtheing.com",
			user_phone: 6321478960,
			isAdmin: true
		};
		let payload = JSON.stringify(data);
		request(app)
			.post("/api/users/new")
			.send(payload)
			.set("Content-type", "application/JSON")
			.then(response => {
				expect(response.statusCode).toBe(500);
				expect(response.text).toBe('"user_password" is required');
				done();
			});
	});

	it("should return error as phone is required", done => {
		const data = {
			user_name: "Ankit",
			user_email: "some@somtheing.com",
			user_password: "something",
			isAdmin: true
		};
		let payload = JSON.stringify(data);
		request(app)
			.post("/api/users/new")
			.send(payload)
			.set("Content-type", "application/JSON")
			.then(response => {
				expect(response.statusCode).toBe(500);
				expect(response.text).toBe('"user_phone" is required');
				done();
			});
	});

	it("should return error as isAdmin is required", done => {
		const data = {
			user_name: "Ankit",
			user_email: "some@somtheing.com",
			user_password: "12345678",
			user_phone: 8742013698
		};
		let payload = JSON.stringify(data);
		request(app)
			.post("/api/users/new")
			.send(payload)
			.set("Content-type", "application/JSON")
			.then(response => {
				expect(response.statusCode).toBe(500);
				expect(response.text).toBe('"isAdmin" is required');
				done();
			});
	});

	it("should return error as name length must be at least 3 characters long", done => {
		const data = {
			user_name: "An",
			user_email: "some@somtheing.com",
			user_password: "12345678",
			user_phone: 8453201697,
			isAdmin: true
		};
		let payload = JSON.stringify(data);
		request(app)
			.post("/api/users/new")
			.send(payload)
			.set("Content-type", "application/JSON")
			.then(response => {
				expect(response.statusCode).toBe(500);
				expect(response.text).toBe(
					'"user_name" length must be at least 3 characters long'
				);
				done();
			});
	});

	it("should return error as name length must be less than or equal to 30 characters long", done => {
		const data = {
			user_name: "Ankit Ankit Ankit Ankit Ankit Ankit",
			user_email: "some@somtheing.com",
			user_password: "12345678",
			user_phone: 674583201,
			isAdmin: true
		};
		let payload = JSON.stringify(data);
		request(app)
			.post("/api/users/new")
			.send(payload)
			.set("Content-type", "application/JSON")
			.then(response => {
				expect(response.statusCode).toBe(500);
				expect(response.text).toBe(
					'"user_name" length must be less than or equal to 30 characters long'
				);
				done();
			});
	});

	it("should return error as email must be a valid email", done => {
		const data = {
			user_name: "Ankit",
			user_email: "some.somtheing.com",
			user_password: "12345678",
			isAdmin: true
		};
		let payload = JSON.stringify(data);
		request(app)
			.post("/api/users/new")
			.send(payload)
			.set("Content-type", "application/JSON")
			.then(response => {
				expect(response.statusCode).toBe(500);
				expect(response.text).toBe(
					'"user_email" must be a valid email'
				);
				done();
			});
	});

	it("should return error as password length must be at least 6 characters long", done => {
		const data = {
			user_name: "Ankit",
			user_email: "some@somtheing.com",
			user_password: "12345",
			user_phone: 6897452016,
			isAdmin: true
		};
		let payload = JSON.stringify(data);
		request(app)
			.post("/api/users/new")
			.send(payload)
			.set("Content-type", "application/JSON")
			.then(response => {
				expect(response.statusCode).toBe(500);
				expect(response.text).toBe(
					'"user_password" length must be at least 6 characters long'
				);
				done();
			});
	});

	it("should return error as password length must be less than or equal to 30 characters long", done => {
		const data = {
			user_name: "Ankit",
			user_email: "some@somtheing.com",
			user_password: "1234567890 abcd 1234567890 abcd 123456789",
			user_phone: 8754120369,
			isAdmin: false
		};
		let payload = JSON.stringify(data);
		request(app)
			.post("/api/users/new")
			.send(payload)
			.set("Content-type", "application/JSON")
			.then(response => {
				expect(response.statusCode).toBe(500);
				expect(response.text).toBe(
					'"user_password" must only contain alpha-numeric characters'
				);
				done();
			});
	});

	it("should return error as name must be a string", done => {
		const data = {
			user_name: 1234567,
			user_email: "some@somtheing.com",
			user_password: "1234567890 1234567890 1234567890",
			user_phone: 8754123690,
			isAdmin: false
		};
		let payload = JSON.stringify(data);
		request(app)
			.post("/api/users/new")
			.send(payload)
			.set("Content-type", "application/JSON")
			.then(response => {
				expect(response.statusCode).toBe(500);
				expect(response.text).toBe('"user_name" must be a string');
				done();
			});
	});

	it("should return error as email must be a string", done => {
		const data = {
			user_name: "Ankit",
			user_email: 1234567,
			user_password: "1234567890 1234567890 1234567890",
			user_phone: 8741296350,
			isAdmin: false
		};
		let payload = JSON.stringify(data);
		request(app)
			.post("/api/users/new")
			.send(payload)
			.set("Content-type", "application/JSON")
			.then(response => {
				expect(response.statusCode).toBe(500);
				expect(response.text).toBe('"user_email" must be a string');
				done();
			});
	});

	it("should return error as password must be a string", done => {
		const data = {
			user_name: "Ankit",
			user_email: "tese@ttesting.com",
			user_password: 1234567890,
			user_phone: 8745031692,
			isAdmin: false
		};
		let payload = JSON.stringify(data);
		request(app)
			.post("/api/users/new")
			.send(payload)
			.set("Content-type", "application/JSON")
			.then(response => {
				expect(response.statusCode).toBe(500);
				expect(response.text).toBe('"user_password" must be a string');
				done();
			});
	});
});

describe("Testing for reset password", () => {
	it("should return a status 404 and an error as email id is not in db", done => {
		const data = {
			user_email: "tes@testing.com",
			user_password: "1234567890ab",
			user_confirmPassword: "1234567890ab"
		};
		let payload = JSON.stringify(data);
		request(app)
			.put("/api/users/resetPassword")
			.send(payload)
			.set("Content-type", "application/JSON")
			.then(response => {
				expect(response.statusCode).toBe(404);
				expect(response.text).toBe("Email not found");
				done();
			});
	});

	it("should return status 200 and returns success", done => {
		const data = {
			user_email: "test@testing.com",
			user_password: "correct",
			user_confirmPassword: "correct"
		};
		let payload = JSON.stringify(data);
		request(app)
			.put("/api/users/resetPassword")
			.send(payload)
			.set("Content-type", "application/JSON")
			.then(response => {
				expect(response.statusCode).toBe(200);
				expect(response.body.successMessage).toBe(
					"Your password is updated successfully"
				);
				done();
			});
	});

	it("should return status 404 for password not matching", done => {
		const data = {
			user_email: "test@testing.com",
			user_password: "correct",
			user_confirmPassword: "incorrect"
		};
		let payload = JSON.stringify(data);
		request(app)
			.put("/api/users/resetPassword")
			.send(payload)
			.set("Content-type", "application/JSON")
			.then(response => {
				expect(response.statusCode).toBe(404);
				expect(response.text).toBe("Password does not match");
				done();
			});
	});
});
