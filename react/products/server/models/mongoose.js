const mongoose = require("mongoose");

const productSchema = mongoose.Schema(
  {
    name: { type: String, unique: true, required: true },
    description: { type: String },
    price: { type: Number, required: true },
    category: { type: String, required: true }
  },
  { collection: "products_collection" }
);

const Products = mongoose.model("Products", productSchema);

module.exports = Products;
