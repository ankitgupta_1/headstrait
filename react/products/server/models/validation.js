const joi = require("@hapi/joi");

module.exports.validateUser = user => {
	const userSchema = {
		user_name: joi
			.string()
			.min(3)
			.max(30)
			.required(),
		user_email: joi
			.string()
			.email()
			.required(),
		user_phone: joi
			.number()
			.precision(10)
			.required(),
		user_password: joi
			.string()
			.alphanum()
			.min(6)
			.max(30)
			.required(),
		isAdmin: joi.boolean().required()
	};
	return joi.validate(user, userSchema);
};

module.exports.validateProduct = product => {
	const productSchema = {
		product_name: joi
			.string()
			.min(10)
			.max(30)
			.required(),
		product_price: joi.number().required(),
		product_description: joi
			.string()
			.min(10)
			.max(300)
			.required(),
		product_quantity: joi.number().required(),
		product_category: joi
			.string()
			.min(10)
			.max(30)
			.required()
	};
	return joi.validate(product, productSchema);
};
