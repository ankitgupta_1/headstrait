import {
	GET_PRODUCTS,
	GET_PRODUCT,
	CREATE_PRODUCT,
	UPDATE_PRODUCT,
	DELETE_PRODUCT,
	ERROR_TYPE
} from "../actions/Types";

const initialState = {
	products: [],
	product: "",
	successMessage: "",
	errorMessage: ""
};

export default function(state = initialState, action) {
	switch (action.type) {
		case GET_PRODUCTS:
			return {
				...state,
				products: action.payload.products,
				successMessage: action.payload.successMessage
			};
		case GET_PRODUCT:
			return {
				...state,
				product: action.payload.product,
				successMessage: action.payload.successMessage
			};
		case CREATE_PRODUCT:
			return {
				...state,
				successMessage: action.payload.successMessage
			};
		case UPDATE_PRODUCT:
			return {
				...state,
				successMessage: action.payload.successMessage
			};
		case DELETE_PRODUCT:
			return {
				...state,
				successMessage: action.payload.successMessage
			};
		case ERROR_TYPE:
			return {
				...state,
				errorMessage: action.payload
			};
		default:
			return state;
	}
}
