import usersReducer from "../usersReducer";
import {
	GET_USERS,
	CREATE_USER,
	UPDATE_USER,
	DELETE_USER
} from "../../actions/Types";

// Test for GET USERS

describe("Testing users reducer for type GET_USERS", () => {
	// test with initialState
	it("should return a state object with USERs array equal to the payload in action of type GET_USERS (when the state is in initial state)", () => {
		const action = {
			type: GET_USERS,
			payload: [{}, {}, {}]
		};
		const returnedState = usersReducer(undefined, action);
		expect(returnedState).toEqual({
			users: action.payload,
			success: "",
			error: ""
		});
	});

	// test without initialState
	it("should return a state object with products array equal to the payload in action of type GET_USERS (when the state is not in initial state)", () => {
		const initialState = {
			users: [1, 2, 3]
		};
		const action = {
			type: GET_USERS,
			payload: [{}, {}, {}]
		};
		const returnedState = usersReducer(initialState, action);
		expect(returnedState).toEqual({ users: action.payload });
	});
});

// Test for CREATE USER

describe("Testing users reducer for type CREATE_USER", () => {
	// test with initialState
	it("should return a state object with users array equal to the payload in action of type CREATE_USER (when the state is in initial state)", () => {
		const action = {
			type: CREATE_USER
		};
		const returnedState = usersReducer(undefined, action);
		expect(returnedState).toEqual({ users: [], success: "", error: "" });
	});

	// test without initialState
	it("should return a state object with users array equal to the payload in action of type CREATE_USER (when the state is not in initial state)", () => {
		const initialState = {
			users: [1, 2, 3]
		};
		const action = {
			type: CREATE_USER
		};
		const returnedState = usersReducer(initialState, action);
		expect(returnedState).toEqual({ users: initialState.users });
	});
});

// Test for UPDATE USER
describe("Testing users reducer for type UPDATE_USER", () => {
	// test with initialState
	it("should return a state object with users array equal to the payload in action of type UPDATE_USER (when the state is in initial state)", () => {
		const action = {
			type: UPDATE_USER
		};
		const returnedState = usersReducer(undefined, action);
		expect(returnedState).toEqual({ users: [], success: "", error: "" });
	});

	// test without initialState
	it("should return a state object with users array equal to the payload in action of type UPDATE_USER (when the state is not in initial state)", () => {
		const initialState = {
			users: [1, 2, 3]
		};
		const action = {
			type: UPDATE_USER
		};
		const returnedState = usersReducer(initialState, action);
		expect(returnedState).toEqual({ users: initialState.users });
	});
});

// Test for DELETE USER
describe("Testing users reducer for type DELETE_USER", () => {
	// test with initialState
	it("should return a state object with users array equal to the payload in action of type DELETE_USER (when the state is in initial state)", () => {
		const action = {
			type: DELETE_USER
		};
		const returnedState = usersReducer(undefined, action);
		expect(returnedState).toEqual({ users: [], success: "", error: "" });
	});

	// test without initialState
	it("should return a state object with users array equal to the payload in action of type DELETE_user (when the state is not in initial state)", () => {
		const initialState = {
			users: [1, 2, 3]
		};
		const action = {
			type: DELETE_USER
		};
		const returnedState = usersReducer(initialState, action);
		expect(returnedState).toEqual({ users: initialState.users });
	});
});

// not mentioned type or does not concerns to reducer
describe("Testing users Reducer for not mentioned type or type which does not concerns the reducer", () => {
	// test with initialState
	it("should return an initial state object when the action type is not mentioned or does not concerns the reducer (when the state is initial state)", () => {
		let action = {
			payload: [{}, {}, {}]
		};
		let returnedState = usersReducer(undefined, action);
		expect(returnedState).toEqual({ users: [], success: "", error: "" });

		action = {
			type: "SOME_TYPE",
			payload: [{}, {}, {}]
		};
		returnedState = usersReducer(undefined, action);
		expect(returnedState).toEqual({ users: [] });
	});

	// test without initialState
	it("should return an initial state object when the action type is not mentioned or does not concerns the reducer (when the state is not in initial state)", () => {
		let initialState = {
			users: [1, 2, 3]
		};
		let action = {
			payload: [{}, {}, {}]
		};
		let returnedState = usersReducer(initialState, action);
		expect(returnedState).toEqual({ users: initialState.users });

		action = {
			type: "SOME_TYPE",
			payload: [{}, {}, {}]
		};
		returnedState = usersReducer(initialState, action);
		expect(returnedState).toEqual({ users: initialState.users });
	});
});
