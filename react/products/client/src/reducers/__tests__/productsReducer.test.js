import productsReducer from "../productsReducer";
import {
	GET_PRODUCTS,
	CREATE_PRODUCT,
	UPDATE_PRODUCT,
	DELETE_PRODUCT
} from "../../actions/Types";

// Test for GET PRODUCTS

describe("Testing products reducer for type GET_PRODUCTS", () => {
	// test with initialState
	it("should return a state object with products array equal to the payload in action of type GET_PRODUCTS (when the state is in initial state)", () => {
		const action = {
			type: GET_PRODUCTS,
			payload: [{}, {}, {}]
		};
		const returnedState = productsReducer(undefined, action);
		expect(returnedState).toEqual({ products: action.payload });
	});

	// test without initialState
	it("should return a state object with products array equal to the payload in action of type GET_PRODUCTS (when the state is not in initial state)", () => {
		const initialState = {
			products: [1, 2, 3]
		};
		const action = {
			type: GET_PRODUCTS,
			payload: [{}, {}, {}]
		};
		const returnedState = productsReducer(initialState, action);
		expect(returnedState).toEqual({ products: action.payload });
	});
});

// Test for CREATE PRODUCT

describe("Testing products reducer for type CREATE_PRODUCT", () => {
	// test with initialState
	it("should return a state object with products array equal to the payload in action of type CREATE_PRODUCT (when the state is in initial state)", () => {
		const action = {
			type: CREATE_PRODUCT
		};
		const returnedState = productsReducer(undefined, action);
		expect(returnedState).toEqual({ products: [] });
	});

	// test without initialState
	it("should return a state object with products array equal to the payload in action of type CREATE_PRODUCT (when the state is not in initial state)", () => {
		const initialState = {
			products: [1, 2, 3]
		};
		const action = {
			type: CREATE_PRODUCT
		};
		const returnedState = productsReducer(initialState, action);
		expect(returnedState).toEqual({ products: initialState.products });
	});
});

// Test for UPDATE PRODUCT
describe("Testing products reducer for type UPDATE_PRODUCT", () => {
	// test with initialState
	it("should return a state object with products array equal to the payload in action of type UPDATE_PRODUCT (when the state is in initial state)", () => {
		const action = {
			type: UPDATE_PRODUCT
		};
		const returnedState = productsReducer(undefined, action);
		expect(returnedState).toEqual({ products: [] });
	});

	// test without initialState
	it("should return a state object with products array equal to the payload in action of type UPDATE_PRODUCT (when the state is not in initial state)", () => {
		const initialState = {
			products: [1, 2, 3]
		};
		const action = {
			type: UPDATE_PRODUCT
		};
		const returnedState = productsReducer(initialState, action);
		expect(returnedState).toEqual({ products: initialState.products });
	});
});

// Test for DELETE PRODUCT
describe("Testing products reducer for type DELETE_PRODUCT", () => {
	// test with initialState
	it("should return a state object with products array equal to the payload in action of type DELETE_PRODUCT (when the state is in initial state)", () => {
		const action = {
			type: DELETE_PRODUCT
		};
		const returnedState = productsReducer(undefined, action);
		expect(returnedState).toEqual({ products: [] });
	});

	// test without initialState
	it("should return a state object with products array equal to the payload in action of type DELETE_PRODUCT (when the state is not in initial state)", () => {
		const initialState = {
			products: [1, 2, 3]
		};
		const action = {
			type: DELETE_PRODUCT
		};
		const returnedState = productsReducer(initialState, action);
		expect(returnedState).toEqual({ products: initialState.products });
	});
});

// not mentioned type or does not concerns to reducer
describe("Testing products Reducer for not mentioned type or type which does not concerns the reducer", () => {
	// test with initialState
	it("should return an initial state object when the action type is not mentioned or does not concerns the reducer (when the state is initial state)", () => {
		let action = {
			payload: [{}, {}, {}]
		};
		let returnedState = productsReducer(undefined, action);
		expect(returnedState).toEqual({ products: [] });

		action = {
			type: "SOME_TYPE",
			payload: [{}, {}, {}]
		};
		returnedState = productsReducer(undefined, action);
		expect(returnedState).toEqual({ products: [] });
	});

	// test without initialState
	it("should return an initial state object when the action type is not mentioned or does not concerns the reducer (when the state is not in initial state)", () => {
		let initialState = {
			products: [1, 2, 3]
		};
		let action = {
			payload: [{}, {}, {}]
		};
		let returnedState = productsReducer(initialState, action);
		expect(returnedState).toEqual({ products: initialState.products });

		action = {
			type: "SOME_TYPE",
			payload: [{}, {}, {}]
		};
		returnedState = productsReducer(initialState, action);
		expect(returnedState).toEqual({ products: initialState.products });
	});
});
