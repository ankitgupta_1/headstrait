import {
	GET_USERS,
	GET_USER,
	CREATE_USER,
	UPDATE_USER,
	DELETE_USER,
	LOGIN_USER,
	RESET_PASSWORD,
	SEND_OTP,
	VERIFY_OTP,
	ERROR_TYPE
} from "../actions/Types";

const initialState = {
	users: [],
	user: "",
	successMessage: "",
	errorMessage: "",
	loggedInUser: {}
};

export default function(state = initialState, action) {
	switch (action.type) {
		case GET_USERS:
			return {
				...state,
				users: action.payload.users,
				successMessage: action.payload.successMessage
			};
		case GET_USER:
			return {
				...state,
				user: action.payload.user,
				successMessage: action.payload.successMessage
			};
		case CREATE_USER:
			return {
				...state,
				successMessage: action.payload.successMessage
			};
		case UPDATE_USER:
			return {
				...state,
				successMessage: action.payload.successMessage
			};
		case DELETE_USER:
			return {
				...state,
				successMessage: action.payload.successMessage
			};
		case LOGIN_USER:
			return {
				...state,
				loggedInUser: action.payload,
				successMessage: action.payload.successMessage
			};
		case RESET_PASSWORD:
			return {
				...state,
				successMessage: action.payload.successMessage
			};
		case SEND_OTP:
			return {
				...state,
				successMessage: action.payload.successMessage
			};
		case VERIFY_OTP:
			return {
				...state,
				successMessage: action.payload.successMessage
			};
		case ERROR_TYPE:
			// return {
			// 	...state,
			// 	errorMessage: action.payload.errorMessage
			// };
			return state;
		default:
			return state;
	}
}
