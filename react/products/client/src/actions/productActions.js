import {
	GET_PRODUCTS,
	GET_PRODUCT,
	CREATE_PRODUCT,
	UPDATE_PRODUCT,
	DELETE_PRODUCT
} from "./Types";

import axios from "axios";

const url = "http://localhost:5000/api/products/";

export const getProducts = () => dispatch => {
	return axios
		.get(url + "all", {
			headers: {
				Authorization: `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => {
			dispatch({
				type: GET_PRODUCTS,
				payload: {
					products: res.data.products,
					successMessage: res.data.successMessage
				}
			});
		})
		.catch(res => {
			// dispatch({
			// 	type: ERROR_TYPE,
			// 	payload: { errorMessage: res.data.errorMessage }
			// });
			console.log(res);
		});
};

export const getProduct = product_id => dispatch => {
	return axios
		.get(url + product_id, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => {
			dispatch({
				type: GET_PRODUCT,
				payload: {
					product: res.data.product,
					successMessage: res.data.successMessage
				}
			});
		})
		.catch(res => {
			// dispatch({
			// 	type: ERROR_TYPE,
			// 	payload: { errorMessage: res.data.errorMessage }
			// });
			console.log(res);
		});
};

export const createProduct = product => dispatch => {
	axios
		.post(url + "new", product, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => {
			dispatch({
				type: CREATE_PRODUCT,
				payload: { successMessage: res.data.successMessage }
			});
			dispatch(getProducts());
		})
		.catch(res => {
			// dispatch({
			// 	type: ERROR_TYPE,
			// 	payload: { errorMessage: res.data.errorMessage }
			// });
			console.log(res);
		});
};

export const updateProduct = updatedProduct => dispatch => {
	axios
		.put(url + "update/" + updatedProduct.id, updatedProduct, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => {
			dispatch({
				type: UPDATE_PRODUCT,
				payload: { successMessage: res.data.successMessage }
			});
			dispatch(getProducts());
		})
		.catch(res => {
			// dispatch({
			// 	type: ERROR_TYPE,
			// 	payload: { errorMessage: res.data.errorMessage }
			// });
			console.log(res);
		});
};

export const deleteProduct = id => dispatch => {
	axios
		.delete(url + "delete/" + id, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => {
			dispatch({
				type: DELETE_PRODUCT,
				payload: { successMessage: res.data.successMessage }
			});
			dispatch(getProducts());
		})
		.catch(res => {
			// dispatch({
			// 	type: ERROR_TYPE,
			// 	payload: { errorMessage: res.data.errorMessage }
			// });
			console.log(res);
		});
};
