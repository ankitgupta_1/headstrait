import { GET_USERS, LOGIN_USER } from "../Types";

import * as action from "../userActions";
import moxios from "moxios";
import configureMockStore from "redux-mock-store";
import thunk from "redux-thunk";

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);
const url = "http://localhost:5000/api/users/";

describe("Testing for GET_USERS actions ", () => {
	beforeEach(() => {
		moxios.install();
	});

	afterEach(() => {
		moxios.uninstall();
	});

	it("should create an action of type GET_USERS and the payload should be same as the api response when the response is 200", () => {
		const responseOfApi = [
			{
				successMessage: undefined,
				users: undefined
			}
		];
		moxios.stubRequest(url + "all", {
			status: 200,
			response: { data: responseOfApi }
		});
		const store = mockStore({});
		const expectedActions = [
			{
				type: GET_USERS,
				payload: responseOfApi
			}
		];
		return store.dispatch(action.getUsers()).then(() => {
			expect(store.getActions()).toEqual(expectedActions);
		});
	});

	it("should create an action of type GET_USERS and the payload should be same as the api response when the response is 200", () => {
		const responseOfApi = [{}, {}, {}];
		moxios.stubRequest(url + "all", {
			status: 404,
			response: { data: responseOfApi }
		});
		const store = mockStore({});
		const expectedActions = [];
		return store.dispatch(action.getUsers()).then(() => {
			expect(store.getActions()).toEqual(expectedActions);
		});
	});
});

describe("Testing for LOGIN_USER actions ", () => {
	beforeEach(() => {
		moxios.install();
	});

	afterEach(() => {
		moxios.uninstall();
	});

	it("should create an action of type LOGIN_USER and the payload should be same as the api response when the response is 200", () => {
		const credentials = {
			name: "test@testing.com",
			password: "correct"
		};
		moxios.stubRequest(url + "login", credentials, {
			status: 200
		});
		const store = mockStore({});
		const expectedActions = [
			{
				type: LOGIN_USER
			}
		];
		return store.dispatch(action.loginUser()).then(() => {
			expect(store.getActions()).toEqual(expectedActions);
		});
	});

	it("should create an action of type LOGIN_USER and the payload should be same as the api response when the response is 200", () => {
		const credentials = {
			name: "test@testing.com",
			password: "wrong"
		};
		moxios.stubRequest(url + "login", credentials, {
			status: 404
		});
		const store = mockStore({});
		const expectedActions = [];
		return store.dispatch(action.loginUser()).then(() => {
			expect(store.getActions()).toEqual(expectedActions);
		});
	});
});
