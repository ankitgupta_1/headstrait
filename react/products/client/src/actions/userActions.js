import {
	GET_USERS,
	GET_USER,
	CREATE_USER,
	UPDATE_USER,
	DELETE_USER,
	LOGIN_USER,
	RESET_PASSWORD,
	VERIFY_OTP,
	SEND_OTP
} from "./Types";

import axios from "axios";

const url = "http://localhost:5000/api/users/";

export const getUsers = () => dispatch => {
	return axios
		.get(url + "all", {
			headers: {
				Authorization: `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => {
			dispatch({
				type: GET_USERS,
				payload: {
					users: res.data.users,
					successMessage: res.data.successMessage
				}
			});
		})
		.catch(res => {
			// dispatch({
			// 	type: ERROR_TYPE,
			// 	payload: { errorMessage: res.data.errorMessage }
			// });
			console.log(res);
		});
};

export const getUser = user_id => dispatch => {
	return axios
		.get(url + user_id, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => {
			dispatch({
				type: GET_USER,
				payload: {
					user: res.data.user,
					successMessage: res.data.successMessage
				}
			});
		})
		.catch(res => {
			// dispatch({
			// 	type: ERROR_TYPE,
			// 	payload: { errorMessage: res.data.errorMessage }
			// });
			console.log(res);
		});
};

export const createUser = user => dispatch => {
	axios
		.post(url + "new", user)
		.then(res => {
			dispatch({
				type: CREATE_USER,
				payload: { successMessage: res.data.successMessage }
			});
			dispatch(getUsers());
		})
		.catch(res => {
			// dispatch({
			// 	type: ERROR_TYPE,
			// 	payload: { errorMessage: res.data.errorMessage }
			// });
			console.log(res);
		});
};

export const updateUser = updatedUser => dispatch => {
	console.log(updateUser);
	axios
		.put(url + "update/" + updatedUser.user_id, updatedUser, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => {
			dispatch({
				type: UPDATE_USER,
				payload: { successMessage: res.data.successMessage }
			});
			dispatch(getUsers());
		})
		.catch(res => {
			// dispatch({
			// 	type: ERROR_TYPE,
			// 	payload: { errorMessage: res.data.errorMessage }
			// });
			console.log(res);
		});
};

export const deleteUser = id => dispatch => {
	axios
		.delete(url + "delete/" + id, {
			headers: {
				Authorization: `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => {
			dispatch({
				type: DELETE_USER,
				payload: { successMessage: res.data.successMessage }
			});
			dispatch(getUsers());
		})
		.catch(res => {
			// dispatch({
			// 	type: ERROR_TYPE,
			// 	payload: { errorMessage: res.data.errorMessage }
			// });
			console.log(res);
		});
};

export const loginUser = credentials => dispatch => {
	axios
		.post(url + "login", credentials)
		.then(res => {
			dispatch({
				type: LOGIN_USER,
				payload: {
					loggedInUser: res.data.loggedInUser,
					successMessage: res.data.successMessage
				}
			});
			localStorage.setItem("token", res.data.token);
		})
		.catch(res => {
			console.log(res);
			// dispatch({
			// type: ERROR_TYPE
			// payload: { errorMessage: res.data.errorMessage }
			// });
		});
};

export const resetPassword = credentials => dispatch => {
	axios
		.put(url + "resetPassword", credentials)
		.then(res => {
			dispatch({
				type: RESET_PASSWORD,
				payload: { successMessage: res.data.successMessage }
			});
		})
		.catch(res => {
			// dispatch({
			// 	type: ERROR_TYPE,
			// 	payload: { errorMessage: res.data.errorMessage }
			// });
			console.log(res);
		});
};

export const sendOTP = user_email => dispatch => {
	axios
		.post(url + "sendOTP", user_email)
		.then(res => {
			dispatch({
				type: SEND_OTP,
				payload: { successMessage: res.data.successMessage }
			});
		})
		.catch(res => {
			// dispatch({
			// 	type: ERROR_TYPE,
			// 	payload: { errorMessage: res.data.errorMessage }
			// });
			console.log(res);
		});
};

export const verifyOTP = otp => dispatch => {
	axios
		.post(url + "verifyOTP", otp)
		.then(res => {
			dispatch({
				type: VERIFY_OTP,
				payload: { successMessage: res.data.successMessage }
			});
		})
		.catch(res => {
			// dispatch({
			// 	type: ERROR_TYPE,
			// 	payload: { errorMessage: res.data.errorMessage }
			// });
			console.log(res);
		});
};
