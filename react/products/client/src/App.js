import React from "react";
import logo from "./logo.svg";
import "./App.css";
import { BrowserRouter as Router, Route } from "react-router-dom";
import Home from "./components/Home";
import Login from "./components/Login";
import Register from "./components/Register";
import ManageProducts from "./components/ManageProducts";
import ManageUsers from "./components/ManageUsers";
import ProductDetails from "./components/ProductDetails";
import ResetPassword from "./components/ResetPassword";
import UpdateUser from "./components/UpdateUser";

function App() {
	return (
		<Router>
			<Route exact path="/" component={Home} />
			<Route exact path="/login" component={Login} />
			<Route exact path="/register" component={Register} />
			<Route exact path="/manage-products" component={ManageProducts} />
			<Route exact path="/manage-users" component={ManageUsers} />
			<Route
				exact
				path="/product/:product_id"
				component={ProductDetails}
			/>
			<Route
				exact
				path="/update-user/:update_user"
				component={UpdateUser}
			/>
			<Route exact path="/reset-password" component={ResetPassword} />
		</Router>
	);
}

export default App;
