import React, { Component } from "react";
import { getProduct } from "../actions/productActions";
import { connect } from "react-redux";
import Navbar from "./Navbar";
import oneplus from "../oneplus.png";
import "./css/ProductDetails.css";

export class DisplayProduct extends Component {
	componentDidMount() {
		this.props.getProduct(this.props.match.params.product_id);
	}

	componentWillMount() {
		if (!localStorage.getItem("token")) {
			this.props.history.push("/login");
		}
	}
	render() {
		// console.log(this.props.match.params.product_id);

		return (
			<>
				{this.props.product ? (
					<div>
						<Navbar />
						<div className="display-details pt-nav">
							<img src={oneplus} width="100%" />
							<div className="product-details">
								<span className="titleText my-10">
									{this.props.product[0].product_name}
								</span>
								<span className="textSmall mb-10">
									{this.props.product[0].product_category}
								</span>

								<span className="textPrice mt-40">
									&#8377;{" "}
									{this.props.product[0].product_price}
								</span>
								<span className="text my-20">
									{this.props.product[0].product_description}
								</span>
							</div>
						</div>
					</div>
				) : (
					console.log("no data")
				)}
			</>
		);
	}
}

const mapStateToProps = state => ({
	product: state.productsReducer.product
});

export default connect(
	mapStateToProps,
	{ getProduct }
)(DisplayProduct);
