import React, { Component } from "react";
import { createUser, sendOTP, verifyOTP } from "../actions/userActions";
import { connect } from "react-redux";
import "./css/Form.css";
import { Link } from "react-router-dom";
import Navbar from "./Navbar";

export class Register extends Component {
	state = {
		inputFieldName: "",
		inputFieldEmail: "",
		inputFieldPhone: "",
		inputFieldPassword: "",
		inputFieldConfirmPassword: "",
		inputFieldOTP: "",
		showFields: false
	};

	handleInput = e => {
		this.setState({ [e.target.name]: e.target.value });
	};

	componentWillMount() {
		if (localStorage.getItem("token")) {
			this.props.history.push("/");
		}
	}

	sendOTP = () => {
		if (
			this.state.inputFieldName &&
			this.state.inputFieldPhone &&
			this.state.inputFieldEmail &&
			this.state.inputFieldPassword &&
			this.state.inputFieldConfirmPassword
		) {
			this.setState({ showFields: true });
			this.props.sendOTP({ user_email: this.state.inputFieldEmail });
		}
	};

	onRegisterButtonClick = async () => {
		console.log(this.state);
		let userDetails = {
			user_name: this.state.inputFieldName,
			user_email: this.state.inputFieldEmail,
			user_phone: +this.state.inputFieldPhone,
			user_password: this.state.inputFieldPassword,
			isAdmin: false
		};
		await this.props.verifyOTP({ otp: this.state.inputFieldOTP });
		this.props.createUser(userDetails);
	};

	render() {
		return (
			<div>
				<Navbar />
				<div className="formDiv pt-nav">
					<span className="titleText">Register</span>
					<form
						className="form p-40 my-30"
						onSubmit={this.onRegisterButtonClick}
					>
						<input
							className="text-field p-16 my-10"
							placeholder="Full name"
							type="text"
							name="inputFieldName"
							required={true}
							pattern=".{3,30}"
							title="3 to 30 characters"
							value={this.state.inputFieldName}
							onChange={this.handleInput}
						/>

						<input
							className="text-field p-16 my-10"
							placeholder="Email"
							type="email"
							name="inputFieldEmail"
							required={true}
							value={this.state.inputFieldEmail}
							onChange={this.handleInput}
						/>

						<input
							className="text-field p-16 my-10"
							placeholder="Phone"
							type="phone"
							name="inputFieldPhone"
							required={true}
							pattern="[0-9]{10}"
							title="Enter valid phone number"
							value={this.state.inputFieldPhone}
							onChange={this.handleInput}
						/>

						<input
							className="text-field p-16 my-10"
							placeholder="Password"
							type="password"
							name="inputFieldPassword"
							required={true}
							pattern=".{6,30}"
							title="6 to 30 characters"
							value={this.state.inputFieldPassword}
							onChange={this.handleInput}
						/>

						<input
							className="text-field p-16 my-10"
							placeholder="Confirm password"
							type="password"
							name="inputFieldConfirmPassword"
							required={true}
							pattern=".{6,30}"
							title="6 to 30 characters"
							value={this.state.inputFieldConfirmPassword}
							onChange={this.handleInput}
						/>

						<button
							className="btn btn-outline border-green p-16 mt-20 mb-20"
							onClick={this.sendOTP}
						>
							Send OTP
						</button>

						<input
							className="text-field p-16 mt-40 mb-10"
							style={{
								display: this.state.showFields
									? "block"
									: "none"
							}}
							placeholder="OTP"
							type="text"
							name="inputFieldOTP"
							required={true}
							pattern=".{4}"
							title="Enter valid OTP"
							value={this.state.inputFieldOTP}
							onChange={this.handleInput}
						/>

						<button
							className="btn btn-outline border-green p-16 mt-20 mb-20"
							style={{
								display: this.state.showFields
									? "block"
									: "none"
							}}
							type="submit"
						>
							Verify and Register
						</button>
						<span className="url-link mt-40">
							Alreay a member?{" "}
							<Link
								style={{ fontWeight: "600", color: "#2ec4b6" }}
								class="url-link"
								to="/login"
							>
								Login
							</Link>
						</span>
					</form>
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => ({
	users: state.usersReducer.users
});

export default connect(
	mapStateToProps,
	{ createUser, sendOTP, verifyOTP }
)(Register);
