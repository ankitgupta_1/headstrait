import React, { Component } from "react";
import { connect } from "react-redux";
import { updateUser, getUser } from "../actions/userActions";
import Navbar from "./Navbar";

export class UpdateUser extends Component {
	state = {
		user_id: this.props.location.state.user.user_id,
		user_name: this.props.location.state.user.user_name,
		user_email: this.props.location.state.user.user_email,
		user_phone: this.props.location.state.user.user_phone,
		user_password: this.props.location.state.user.user_password,
		isAdmin: this.props.location.state.user.isdmin
	};

	updateUser = async event => {
		event.preventDefault();
		console.log(this.state);
		// await this.props.updateUser(this.state);
		// this.props.history.push("/manage-users");
	};

	handleInput = e => {
		e.target.type === "checkbox"
			? this.setState({ [e.target.name]: e.target.checked })
			: this.setState({ [e.target.name]: e.target.value });
		e.preventDefault();
	};

	render() {
		// console.log(this.props.location.state.user);
		console.log(this.state);
		return (
			<div>
				<Navbar />
				<div className="formDiv pt-nav">
					<span className="titleText">Update User</span>
					<form
						className="form p-40 my-30"
						onSubmit={this.updateUser}
					>
						<input
							className="text-field p-16 my-10"
							placeholder="Full name"
							type="text"
							name="user_name"
							required={true}
							pattern=".{3,30}"
							title="3 to 30 characters"
							value={this.state.user_name}
							onChange={this.handleInput}
						/>
						<input
							className="text-field p-16 my-10"
							placeholder="Email"
							type="email"
							name="user_email"
							required={true}
							value={this.state.user_email}
							onChange={this.handleInput}
						/>
						<input
							className="text-field p-16 my-10"
							placeholder="Phone"
							type="phone"
							name="user_phone"
							required={true}
							pattern="[0-9]{10}"
							title="Enter valid phone number"
							value={this.state.user_phone}
							onChange={this.handleInput}
						/>
						<input
							// className="text-field p-16 my-10"
							// placeholder="Confirm password"
							style={{ display: "inline" }}
							type="checkbox"
							name="isAdmin"
							checked={this.state.isAdmin}
							onChange={this.handleInput}
						/>
						Give Admin privileges
						<button className="btn btn-outline border-green p-16 mt-40 mb-20">
							Update User
						</button>
					</form>
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => ({
	users: state.usersReducer.users
});

export default connect(
	mapStateToProps,
	{ updateUser, getUser }
)(UpdateUser);
