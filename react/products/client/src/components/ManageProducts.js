import React, { Component } from "react";
import DisplayProductsList from "./DisplayProductsList";
import Navbar from "./Navbar";

import { connect } from "react-redux";
import { getProducts, createProduct } from "../actions/productActions";

export class ManageProducts extends Component {
	state = {
		inputFieldName: "",
		inputFieldDescription: "",
		inputFieldCategory: "",
		inputFieldQuantity: "",
		inputFieldPrice: ""
	};

	handleInput = e => {
		this.setState({ [e.target.name]: e.target.value });
	};

	componentWillMount() {
		if (!localStorage.getItem("token")) {
			this.props.history.push("/login");
		}
		this.props.getProducts();
	}

	onCreateButtonClick = () => {
		console.log(this.state);
		let product = {
			product_name: this.state.inputFieldName,
			product_description: this.state.inputFieldDescription,
			product_category: this.state.inputFieldCategory,
			product_price: +this.state.inputFieldPrice,
			product_quantity: +this.state.inputFieldQuantity
		};
		this.props.createProduct(product);
	};

	render() {
		return (
			<div>
				<Navbar />
				<div className="pt-nav display-manage">
					<DisplayProductsList
						products={this.props.products}
						deleteProduct={this.props.deleteProduct}
						updateProduct={this.props.updateProduct}
					/>
					<div className="form-style">
						<span className="titleText">Create Product</span>
						<form
							className="form p-40"
							onSubmit={this.onCreateButtonClick}
						>
							<input
								className="text-field p-16 my-10"
								placeholder="Product name"
								type="text"
								name="inputFieldName"
								required={true}
								pattern=".{10,30}"
								title="10 to 30 characters"
								value={this.state.inputFieldName}
								onChange={this.handleInput}
							/>

							<input
								className="text-field p-16 my-10"
								placeholder="Description"
								type="text"
								name="inputFieldDescription"
								required={true}
								value={this.state.inputFieldDescription}
								onChange={this.handleInput}
							/>

							<input
								className="text-field p-16 my-10"
								placeholder="Category"
								type="text"
								name="inputFieldCategory"
								required={true}
								pattern=".{3,30}"
								title="3 to 30 characters"
								value={this.state.inputFieldCategory}
								onChange={this.handleInput}
							/>

							<input
								className="text-field p-16 my-10"
								placeholder="Price"
								type="text"
								name="inputFieldPrice"
								required={true}
								value={this.state.inputFieldPrice}
								onChange={this.handleInput}
							/>

							<input
								className="text-field p-16 my-10"
								placeholder="Quantity"
								type="text"
								name="inputFieldQuantity"
								required={true}
								value={this.state.inputFieldQuantity}
								onChange={this.handleInput}
							/>

							<button
								className="btn btn-outline border-green p-16 mt-40 mb-20"
								type="submit"
							>
								Create Product
							</button>
						</form>
					</div>
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => ({
	products: state.productsReducer.products
});

export default connect(
	mapStateToProps,
	{ getProducts, createProduct }
)(ManageProducts);
