import React from "react";
import { shallow } from "enzyme";
import DisplayUsers from "../DisplayUsers";
import { Provider } from "react-redux";
import store from "../../store";

const users = [{}, {}, {}];
const wrapper = shallow(
	// <Provider store={store}>
	<DisplayUsers users={users} />
	// </Provider>
);

describe("check the p tags", () => {
	it("should have atleast 3 p tags", () => {
		expect(wrapper.find("p").length).toBeGreaterThanOrEqual(3);
	});
});
