import React from "react";
import { shallow } from "enzyme";
import ManageUsers from "../ManageUsers";
import { Provider } from "react-redux";
import store from "../../store";

const getUsers = jest.fn();
const handleInput = jest.fn();
const users = [{}, {}, {}];
const wrapper = shallow(
	// <Provider store={store}>
	<ManageUsers handleInput={handleInput} />
	// </Provider>
);

describe("testing the input field properties in the state", () => {
	// it("should have input value as in the state in the of property inputFieldName", () => {
	// 	const e = {
	// 		target: {
	// 			name: "inputFieldName",
	// 			value: "modified test"
	// 		}
	// 	};
	// 	wrapper.instance().handleInput(e);
	// 	expect(wrapper.state().inputFieldName).toBe(e.target.value);
	// });
	// it("should have input value as in the state in the of property inputFieldEmail", () => {
	// 	const e = {
	// 		target: {
	// 			name: "inputFieldEmail",
	// 			value: "modified test"
	// 		}
	// 	};
	// 	wrapper.instance().handleInput(e);
	// 	expect(wrapper.state().inputFieldEmail).toBe(e.target.value);
	// });
	// it("should have input value as in the state in the of property inputFieldRole", () => {
	// 	const e = {
	// 		target: {
	// 			name: "inputFieldRole",
	// 			value: "modified test"
	// 		}
	// 	};
	// 	wrapper.instance().handleInput(e);
	// 	expect(wrapper.state().inputFieldRole).toBe(e.target.value);
	// });

	it("should have exactly 1 button adn 3 inputs", () => {
		expect(wrapper.find("button").length).toBeGreaterThanOrEqual(1);
		expect(wrapper.find("input").length).toBeGreaterThanOrEqual(3);
	});

	it("should have the exact text on the button and input fields", () => {
		expect(
			wrapper
				.find("button")
				.at(0)
				.text()
		).toBe("Create User");
		expect(
			wrapper
				.find("input")
				.at(0)
				.prop("placeholder")
		).toBe("Full name");
		expect(
			wrapper
				.find("input")
				.at(1)
				.prop("placeholder")
		).toBe("Email");
		expect(
			wrapper
				.find("input")
				.at(2)
				.prop("placeholder")
		).toBe("Role");
	});
});
