import React from "react";
import { shallow } from "enzyme";
import Login from "../Login";
import { Provider } from "react-redux";
import store from "../../store";

const getUsers = jest.fn();
// const handleInput = jest.fn();
const users = [{}, {}, {}];
const wrapper = shallow(
	// <Provider store={store}>
	<Login />
	// </Provider>
);
const instance = wrapper.instance();
describe("testing the input field properties in the state", () => {
	// it("should have input value as in the state in the of property inputFieldName", () => {
	// 	const e = {
	// 		target: {
	// 			name: "inputFieldName",
	// 			value: "modified test"
	// 		}
	// 	};
	// 	wrapper.instance().handleInput(e);
	// 	expect(wrapper.state().inputFieldName).toBe(e.target.value);
	// });

	// it("should have input value as in the state in the of property inputFieldPassword", () => {
	// 	const e = {
	// 		target: {
	// 			name: "inputFieldPassword",
	// 			value: "modified test"
	// 		}
	// 	};
	// 	wrapper.instance().handleInput(e);
	// 	expect(wrapper.state().inputFieldPassword).toBe(e.target.value);
	// });

	it("should have exactly 1 button and 2 inputs and one form tag", () => {
		expect(wrapper.find("button").length).toEqual(1);
		expect(wrapper.find("input").length).toEqual(2);
		expect(wrapper.find("form").length).toEqual(1);
	});

	it("should have the exact text on the button and input fields", () => {
		expect(wrapper.find("button").text()).toBe("Login");
		expect(
			wrapper
				.find("input")
				.at(0)
				.prop("placeholder")
		).toBe("Email");
		expect(
			wrapper
				.find("input")
				.at(1)
				.prop("placeholder")
		).toBe("Password");
	});
});
