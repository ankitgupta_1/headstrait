import React, { Component } from "react";
import DisplayProductsList from "./DisplayProductsList";
import Navbar from "./Navbar";
import { getProducts } from "../actions/productActions";
import { connect } from "react-redux";

export class Home extends Component {
	componentWillMount() {
		this.props.getProducts();
	}
	render() {
		return (
			<div>
				<Navbar />
				<DisplayProductsList products={this.props.products} />
			</div>
		);
	}
}

const mapStateToProps = state => ({
	products: state.productsReducer.products
});

export default connect(
	mapStateToProps,
	{ getProducts }
)(Home);
