import React, { Component } from "react";
import { loginUser } from "../actions/userActions";
import { connect } from "react-redux";
import "./css/Form.css";
import Navbar from "./Navbar";
import { Link } from "react-router-dom";

export class Register extends Component {
	state = {
		inputFieldName: "",
		inputFieldPassword: ""
	};

	handleInput = e => {
		this.setState({ [e.target.name]: e.target.value });
	};

	onLoginButtonClick = e => {
		// e.preventDefault();
		console.log(this.state);
		let credentials = {
			user_email: this.state.inputFieldEmail,
			user_password: this.state.inputFieldPassword
		};
		console.log(credentials);
		this.props.loginUser(credentials);
	};

	componentWillMount() {
		if (localStorage.getItem("token")) {
			this.props.history.push("/");
		}
	}

	render() {
		return (
			<div>
				<Navbar />
				<div className="formDiv pt-nav">
					<span className="titleText">Login</span>
					<form
						className="form p-40 my-30"
						onSubmit={this.onLoginButtonClick}
					>
						<input
							className="text-field p-16 my-10"
							placeholder="Email"
							type="email"
							name="inputFieldEmail"
							required={true}
							value={this.state.inputFieldEmail}
							onChange={this.handleInput}
						/>

						<input
							className="text-field p-16 my-10"
							placeholder="Password"
							type="password"
							name="inputFieldPassword"
							required={true}
							pattern=".{6,30}"
							title="6 to 30 characters"
							value={this.state.inputFieldPassword}
							onChange={this.handleInput}
						/>
						<button
							className="btn btn-outline border-green p-16 mt-40 mb-20"
							type="submit"
						>
							Login
						</button>
						<Link
							style={{ color: "#2ec4b6" }}
							className="url-link"
							to="/reset-password"
						>
							Forgot Password?
						</Link>
						<span className="url-link mt-40">
							Not a member?{" "}
							<Link
								style={{ fontWeight: "600", color: "#2ec4b6" }}
								className="url-link"
								to="/register"
							>
								Create Account
							</Link>
						</span>
					</form>
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => ({
	users: state.usersReducer.users
});

export default connect(
	mapStateToProps,
	{ loginUser }
)(Register);
