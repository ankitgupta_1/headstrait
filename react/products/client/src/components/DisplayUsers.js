import React, { Component } from "react";
import "./css/DisplayUsers.css";
import { Link } from "react-router-dom";
// import Button from "./common/Button";

export default class DisplayUsers extends Component {
	state = {
		showModal: false
	};
	openModal = () => {
		this.setState({ showModal: true });
	};

	closeModal = () => {
		this.setState({ showModal: false });
	};

	// updateUser = user => {
	// 	let user_details = {
	// 		user_id: user.updateFieldid,
	// 		user_name: user.updateFieldName,
	// 		user_email: user.updateFieldEmail,
	// 		user_phone: user.updateFieldPhone,
	// 		user_password: user.updateFieldPassword,
	// 		isAdmin: user.updateFieldisAdmin
	// 	};
	// 	console.log(user_details);
	// 	this.props.updateUser(user_details);
	// };

	render() {
		return (
			<div className="display">
				{this.props.users.map(user => (
					<div className="card p-20 m-20">
						{console.log(user)}
						<span style={{ fontSize: "24px", fontWeight: "600" }}>
							{user.user_name}
						</span>
						<span className="userText my-10">
							{user.user_email}
						</span>
						{user.isadmin ? (
							<span className="userHighlight bg-blue">Admin</span>
						) : (
							<span className="userHighlight bg-green">
								Member
							</span>
						)}

						<div className="buttonGroup">
							{/* <button
								className="btn btn-outline border-green p-16 mt-40 mb-20"
								onClick={() => {
									this.props.history.push(
										"/update-user/" + user.user_id,
										{ user }
									);
								}}
							>
								Edit
							</button> */}

							<Link
								className="btn btn-outline border-green p-16 mt-40 mb-20"
								to={{
									pathname: "/update-user/" + user.user_id,
									state: { user }
								}}
							>
								Edit
							</Link>

							<button
								className="btn btn-outline border-red p-16 mt-40 mb-20"
								onClick={() =>
									this.props.deleteUser(user.user_id)
								}
							>
								Delete
							</button>
						</div>
						{/* <ReactModal
							isOpen={this.state.showModal}
							className="modal"
						>
							<div
								style={{
									display: "flex",
									flexDirection: "column"
								}}
							>
								<span
									style={{ textAlign: "center" }}
									className="titleText"
								>
									Update User
								</span>
								<UpdateUser
									user_id={user.user_id}
									user_name={user.user_name}
									user_email={user.user_email}
									user_phone={user.user_phone}
									user_password={user.user_password}
									isAdmin={user.isadmin}
									updateUser={this.updateUser}
								/>
							</div>

							<button
								className="btn-outline border-red mt-40 mb-20 close-button"
								onClick={this.closeModal}
							>
								x
							</button>
						</ReactModal> */}
					</div>
				))}
			</div>
		);
	}
}
