import React, { Component } from "react";
import Navbar from "./Navbar";
import { sendOTP, verifyOTP, resetPassword } from "../actions/userActions";
import { connect } from "react-redux";

export class ResetPassword extends Component {
	state = {
		inputFieldEmail: "",
		inputFieldPassword: "",
		inputFieldConfirmPassword: "",
		inputFieldOTP: "",
		showFields: false
	};

	handleInput = e => {
		this.setState({ [e.target.name]: e.target.value });
	};

	sendOTP = () => {
		if (
			this.state.inputFieldEmail &&
			this.state.inputFieldPassword &&
			this.state.inputFieldConfirmPassword
		) {
			this.setState({ showFields: true });
			this.props.sendOTP({ user_email: this.state.inputFieldEmail });
		}
	};

	onResetButtonClick = async () => {
		console.log(this.state);
		await this.props.verifyOTP({ otp: this.state.inputFieldOTP });
		await this.props.resetPassword({
			user_email: this.state.inputFieldEmail,
			user_password: this.state.inputFieldPassword,
			user_confirmPassword: this.state.inputFieldConfirmPassword
		});
		this.props.history.push("/login");
	};

	render() {
		return (
			<div>
				<Navbar />
				<div className="formDiv pt-nav">
					<span className="titleText">Reset Password</span>
					<form
						className="form p-40 my-30"
						onSubmit={this.onResetButtonClick}
					>
						<input
							className="text-field p-16 my-10"
							placeholder="Email"
							type="email"
							name="inputFieldEmail"
							required={true}
							value={this.state.inputFieldEmail}
							onChange={this.handleInput}
						/>

						<input
							className="text-field p-16 my-10"
							placeholder="New Password"
							type="password"
							name="inputFieldPassword"
							required={true}
							pattern=".{6,30}"
							title="6 to 30 characters"
							value={this.state.inputFieldPassword}
							onChange={this.handleInput}
						/>

						<input
							className="text-field p-16 my-10"
							placeholder="Confirm Password"
							type="password"
							name="inputFieldConfirmPassword"
							required={true}
							pattern=".{6,30}"
							title="6 to 30 characters"
							value={this.state.inputFieldConfirmPassword}
							onChange={this.handleInput}
						/>

						<button
							className="btn btn-outline border-green p-16 mt-20 mb-20"
							onClick={this.sendOTP}
						>
							Send OTP
						</button>

						<input
							className="text-field p-16 mt-40 mb-10"
							style={{
								display: this.state.showFields
									? "block"
									: "none"
							}}
							placeholder="OTP"
							type="text"
							name="inputFieldOTP"
							required={true}
							pattern=".{4}"
							title="Enter valid OTP"
							value={this.state.inputFieldOTP}
							onChange={this.handleInput}
						/>

						<button
							className="btn btn-outline border-green p-16 mt-20 mb-20"
							style={{
								display: this.state.showFields
									? "block"
									: "none"
							}}
						>
							Verify and Reset Password
						</button>
					</form>
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => ({
	users: state.usersReducer.users
});

export default connect(
	mapStateToProps,
	{ sendOTP, verifyOTP, resetPassword }
)(ResetPassword);
