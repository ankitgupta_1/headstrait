import React, { Component } from "react";
import "./css/DisplayUsers.css";
import { Link } from "react-router-dom";

export default class DisplayProductsList extends Component {
	render() {
		return (
			<div className="																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																								">
				<span className="page-title my-40 ml-20">Products List</span>
				<div className="display">
					{this.props.products.map(product => (
						<div className="card p-20 m-20">
							<span className="titleText">
								{product.product_name}
							</span>
							<Link to={"/product/" + product.product_id}>
								<button
									style={{
										width: "100px",
										justifySelf: "center",
										alignSelf: "center"
									}}
									className="btn btn-outline border-green p-10 mt-40"
								>
									View
								</button>
							</Link>
						</div>
					))}
				</div>
			</div>
		);
	}
}
