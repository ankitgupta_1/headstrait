import React, { Component } from "react";
import { Link } from "react-router-dom";
import "./css/Navbar.css";

export default class Navbar extends Component {
	logout = () => {
		localStorage.removeItem("token");
	};
	render() {
		return (
			<div className="navbar-parent mb-40">
				<div className="navbar">
					<Link to="/" className="navbar-logo">
						Altriga
					</Link>
					<div className="nav-links">
						{localStorage.getItem("token") ? (
							<>
								<Link className="link" to="/manage-users">
									Users
								</Link>

								<Link className="link" to="/manage-products">
									Products
								</Link>

								<Link
									style={{ cursor: "pointer" }}
									className="link"
									to="/login"
									onClick={this.logout}
								>
									Logout
								</Link>
							</>
						) : (
							<>
								<Link className="link" to="/login">
									Login
								</Link>

								<Link className="link" to="/register">
									Register
								</Link>
							</>
						)}
					</div>
				</div>
			</div>
		);
	}
}
