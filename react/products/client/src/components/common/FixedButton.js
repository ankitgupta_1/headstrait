import React from "react";

export default function FixedButton(props) {
	return (
		<>
			<button className="reusable-fixed-button" onClick={props.onClick}>
				{props.children}
			</button>
		</>
	);
}
