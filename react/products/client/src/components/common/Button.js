import React from "react";

export default function Button(props) {
	return (
		<>
			<button className="reusable-button" onClick={props.onClick}>
				{props.children}
			</button>
		</>
	);
}
