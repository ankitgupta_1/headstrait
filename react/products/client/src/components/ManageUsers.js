import React, { Component } from "react";
import DisplayUsers from "./DisplayUsers";
import Navbar from "./Navbar";

import { connect } from "react-redux";
import {
	getUsers,
	createUser,
	deleteUser,
	updateUser
} from "../actions/userActions";

export class ManageUsers extends Component {
	state = {
		inputFieldName: "",
		inputFieldEmail: "",
		inputFieldRole: ""
	};

	handleInput = e => {
		this.setState({ [e.target.name]: e.target.value });
	};

	componentWillMount() {
		this.props.getUsers();
	}

	onCreateButtonClick = () => {
		console.log(this.state);
		let user = {
			name: this.state.inputFieldName,
			email: this.state.inputFieldEmail,
			role: +this.state.inputFieldRole,
			password: this.state.inputFieldName
		};
		this.props.createUser(user);
	};

	render() {
		return (
			<div>
				<Navbar />
				<div className="pt-nav display-manage">
					<DisplayUsers
						users={this.props.users}
						deleteUser={this.props.deleteUser}
						updateUser={this.props.updateUser}
					/>
					<div className="form-style">
						<span className="titleText">Create User</span>
						<form
							className="form p-40 my-30"
							onSubmit={this.onCreateButtonClick}
						>
							<input
								className="text-field p-16 my-10"
								placeholder="Full name"
								type="text"
								name="inputFieldName"
								required={true}
								pattern=".{3,30}"
								title="3 to 30 characters"
								value={this.state.inputFieldName}
								onChange={this.handleInput}
							/>

							<input
								className="text-field p-16 my-10"
								placeholder="Email"
								type="email"
								name="inputFieldEmail"
								required={true}
								value={this.state.inputFieldEmail}
								onChange={this.handleInput}
							/>

							<input
								className="text-field p-16 my-10"
								placeholder="Phone"
								type="phone"
								name="inputFieldPhone"
								required={true}
								pattern="[0-9]{10}"
								title="Enter valid phone number"
								value={this.state.inputFieldPhone}
								onChange={this.handleInput}
							/>

							<input
								className="text-field p-16 my-10"
								placeholder="Password"
								type="password"
								name="inputFieldPassword"
								required={true}
								pattern=".{6,30}"
								title="6 to 30 characters"
								value={this.state.inputFieldPassword}
								onChange={this.handleInput}
							/>

							<input
								className="text-field p-16 my-10"
								placeholder="Confirm password"
								type="password"
								name="inputFieldConfirmPassword"
								required={true}
								pattern=".{6,30}"
								title="6 to 30 characters"
								value={this.state.inputFieldConfirmPassword}
								onChange={this.handleInput}
							/>

							<button
								className="btn btn-outline border-green p-16 mt-40 mb-20"
								type="submit"
							>
								Register User
							</button>
						</form>
					</div>
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => ({
	users: state.usersReducer.users
});

export default connect(
	mapStateToProps,
	{ getUsers, createUser, deleteUser, updateUser }
)(ManageUsers);
