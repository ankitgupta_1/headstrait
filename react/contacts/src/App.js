import React from "react";
import "./App.css";
import { BrowserRouter as Router, Route } from "react-router-dom";

import HomeComponent from "./components/HomeComponent";
import OtherComponent from "./components/OtherComponent";

function App() {
	return (
		<Router>
			<Route path="/" component={HomeComponent} />
			<Route path="/other" component={OtherComponent} />
		</Router>
	);
}

export default App;
