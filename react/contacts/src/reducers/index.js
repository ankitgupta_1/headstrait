import { combineReducers } from "redux";
import contacts from "./contacts";
import loadingReducer from "./loadingReducer";

export default combineReducers({
	contactReducer: contacts,
	loadingReducer
});
