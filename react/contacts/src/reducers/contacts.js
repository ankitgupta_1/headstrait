import {
	GET_CONTACTS,
	CREATE_CONTACT,
	DELETE_CONTACT,
	UPDATE_CONTACT
} from "../actions/Type";

const initialState = {
	contacts: []
};

export default function(state = initialState, action) {
	switch (action.type) {
		case GET_CONTACTS:
			return {
				...state,
				contacts: action.payload
			};
		case DELETE_CONTACT:
			return state;
		case CREATE_CONTACT:
			return state;
		case UPDATE_CONTACT:
			return state;
		default:
			return state;
	}
}
