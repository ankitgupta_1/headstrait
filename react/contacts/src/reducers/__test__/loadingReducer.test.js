import { LOADING_START, LOADING_STOP } from "../../actions/Type";
import loadingReducer from "../loadingReducer";

describe("Testing Loading Reducer for type LOADING_START", () => {
	// test with initial state
	it("should return a state object with isLoading value in the action of type LOADING_START (when the state is initial state)", () => {
		const action = {
			type: LOADING_START
		};
		const returnedState = loadingReducer(false, action);
		expect(returnedState).toEqual({ isLoading: true });
	});
	// test with no initial state
	it("should return a state object with isLoading value in the action of type LOADING_START (when the state is not initial state)", () => {
		let initialState = {
			isLoading: false
		};
		const action = {
			type: LOADING_START
		};
		const returnedState = loadingReducer(initialState, action);
		expect(returnedState).toEqual({ isLoading: true });
	});
});

describe("Testing Loading Reducer for type LOADING_STOP", () => {
	// test with initial state
	it("should return a state object with isLoading value in the action of type LOADING_STOP (when the state is initial state)", () => {
		const action = {
			type: LOADING_STOP
		};
		const returnedState = loadingReducer(true, action);
		expect(returnedState).toEqual({ isLoading: false });
	});
	// test with no initial state
	it("should return a state object with isLoading value in the action of type LOADING_START (when the state is not initial state)", () => {
		let initialState = {
			isLoading: true
		};
		const action = {
			type: LOADING_STOP
		};
		const returnedState = loadingReducer(initialState, action);
		expect(returnedState).toEqual({ isLoading: false });
	});
});
