import contactsReducer from "../contacts";
import {
	GET_CONTACTS,
	UPDATE_CONTACT,
	DELETE_CONTACT,
	CREATE_CONTACT
} from "../../actions/Type";

// Test for GET_CONTACTS type
describe("Testing Contacts Reducer for type GET_CONTACTS", () => {
	// test with initial state
	it("should return a state object with contacts array equal to payload in the action of type GET_CONTACTS (when the state is initial state)", () => {
		const action = {
			type: GET_CONTACTS,
			payload: [{}, {}, {}]
		};
		const returnedState = contactsReducer(undefined, action);
		expect(returnedState).toEqual({ contacts: action.payload });
	});
	// test with no initial state
	it("should return a state object with contacts array equal to payload in the action of type GET_CONTACTS (when the state is not initial state)", () => {
		let initialState = {
			contacts: [1, 2, 3, 4, 5]
		};
		const action = {
			type: GET_CONTACTS,
			payload: [{}, {}, {}]
		};
		const returnedState = contactsReducer(initialState, action);
		expect(returnedState).toEqual({ contacts: action.payload });
	});
});

// Test for CREATE_CONTACT type
describe("Testing Contacts Reducer for type CREATE_CONTACT", () => {
	// test with initial state
	it("should return a state object with contacts array equal to payload in the action of type CREATE_CONTACT (when the state is initial state)", () => {
		const action = {
			type: CREATE_CONTACT
		};
		const returnedState = contactsReducer(undefined, action);
		expect(returnedState).toEqual({ contacts: [] });
	});
	// test with no initial state
	it("should return a state object with contacts array equal to payload in the action of type CREATE_CONTACT (when the state is not initial state)", () => {
		let initialState = {
			contacts: [1, 2, 3, 4, 5]
		};
		const action = {
			type: CREATE_CONTACT
		};
		const returnedState = contactsReducer(initialState, action);
		expect(returnedState).toEqual({ contacts: initialState.contacts });
	});
});

// Test for DELETE_CONTACT type
describe("Testing Contacts Reducer for type DELETE_CONTACT", () => {
	// test with initial state
	it("should return a state object with contacts array equal to payload in the action of type DELETE_CONTACT (when the state is initial state)", () => {
		const action = {
			type: DELETE_CONTACT
		};
		const returnedState = contactsReducer(undefined, action);
		expect(returnedState).toEqual({ contacts: [] });
	});
	// test with no initial state
	it("should return a state object with contacts array equal to payload in the action of type DELETE_CONTACT (when the state is not initial state)", () => {
		let initialState = {
			contacts: [1, 2, 3, 4, 5]
		};
		const action = {
			type: DELETE_CONTACT
		};
		const returnedState = contactsReducer(initialState, action);
		expect(returnedState).toEqual({ contacts: initialState.contacts });
	});
});

// Test for UPDATE_CONTACT type
describe("Testing Contacts Reducer for type UPDATE_CONTACT", () => {
	// test with initial state
	it("should return a state object with contacts array equal to payload in the action of type UPDATE_CONTACT (when the state is initial state)", () => {
		const action = {
			type: UPDATE_CONTACT
		};
		const returnedState = contactsReducer(undefined, action);
		expect(returnedState).toEqual({ contacts: [] });
	});
	// test with no initial state
	it("should return a state object with contacts array equal to payload in the action of type UPDATE_CONTACT (when the state is not initial state)", () => {
		let initialState = {
			contacts: [1, 2, 3, 4, 5]
		};
		const action = {
			type: UPDATE_CONTACT
		};
		const returnedState = contactsReducer(initialState, action);
		expect(returnedState).toEqual({ contacts: initialState.contacts });
	});
});

// not mentioned type or does not concerns the reducer
describe("Testing contacts Reducer for not mentioned type or type which does not concerns the reducer", () => {
	// test with initial state
	it("should return an initial state object when the action type is not mentioned or does not concerns the reducer (when the state is initial state)", () => {
		let action = {
			payload: [{}, {}, {}]
		};
		let returnedState = contactsReducer(undefined, action);
		expect(returnedState).toEqual({ contacts: [] });
		action = {
			type: "SOME_TYPE",
			payload: [{}, {}, {}]
		};
		returnedState = contactsReducer(undefined, action);
		expect(returnedState).toEqual({ contacts: [] });
	});
	// test with no initial state
	it("should return a state object with contacts array equal to payload in the action of type GET_CONTACTS (when the state is not initial state)", () => {
		let initialState = {
			contacts: [1, 2, 3, 4, 5]
		};
		let action = {
			payload: [{}, {}, {}]
		};
		let returnedState = contactsReducer(initialState, action);
		expect(returnedState).toEqual({ contacts: initialState.contacts });
		action = {
			type: "SOME_TYPE",
			payload: [{}, {}, {}]
		};
		returnedState = contactsReducer(initialState, action);
		expect(returnedState).toEqual({ contacts: initialState.contacts });
	});
});
