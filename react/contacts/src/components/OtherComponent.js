import React, { Component } from "react";

export default class OtherComponent extends Component {
	render() {
		return <div>This is routed to /other</div>;
	}
}
