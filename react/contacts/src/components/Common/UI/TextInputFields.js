import React from "react";

export default function TextInputFields(props) {
	return (
		<>
			<input
				className="reusable-input-field"
				placeholder={props.placeholder}
				name={props.name}
				value={props.value}
				onChange={props.onChange}
			/>
		</>
	);
}
