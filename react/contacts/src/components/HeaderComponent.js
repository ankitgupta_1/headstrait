import React from 'react'
import '../css/HeaderComponent.css'

export default function HeaderComponent() {
    return (
        <div className="header">
            <h4>Contacts Manager</h4>
        </div>
    )
}
