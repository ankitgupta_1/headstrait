import React, { Component } from "react";
import TextInputFields from "./Common/UI/TextInputFields";
import Button from "./Common/UI/Button";

export default class CreateComponent extends Component {
	render() {
		return (
			<div className="create">
				<TextInputFields
					placeholder="Full name"
					name="inputFieldName"
					value={this.props.name}
					onChange={this.props.handleInput}
				/>
				<TextInputFields
					placeholder="Mobile"
					name="inputFieldMobile"
					value={this.props.phone}
					onChange={this.props.handleInput}
				/>
				<TextInputFields
					placeholder="Email ID"
					name="inputFieldEmail"
					value={this.props.email}
					onChange={this.props.handleInput}
				/>
				<Button onClick={this.props.onCreateButtonClick}>
					Create Contact
				</Button>
			</div>
		);
	}
}
