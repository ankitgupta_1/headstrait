import React, { Component } from "react";
import TextInputFields from "./Common/UI/TextInputFields";
import Button from "./Common/UI/Button";

export default class UpdateComponent extends Component {
	state = {
		id: this.props.contact.id,
		name: this.props.contact.name,
		phone: this.props.contact.phone,
		email: this.props.contact.email
	};

	handleInput = e => {
		this.setState({ [e.target.name]: e.target.value });
	};

	render() {
		return (
			<div className="updateContact">
				<TextInputFields
					name="name"
					value={this.state.name}
					onChange={this.handleInput}
				/>
				<TextInputFields
					name="phone"
					value={this.state.phone}
					onChange={this.handleInput}
				/>
				<TextInputFields
					name="email"
					value={this.state.email}
					onChange={this.handleInput}
				/>
				<Button onClick={() => this.props.updateButton(this.state)}>
					Update
				</Button>
			</div>
		);
	}
}
