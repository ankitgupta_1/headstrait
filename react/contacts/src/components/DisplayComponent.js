import React, { Component } from "react";
import UpdateComponent from "./UpdateComponent";
import Button from "./Common/UI/Button";
import { connect } from "react-redux";
import spinner from "../loadingspinner.gif";

export class DisplayComponent extends Component {
	onUpdate = contact => {
		this.props.onUpdateButtonClick(contact);
	};
	render() {
		return (
			<div className="contactDisplay">
				{this.props.isLoading ? (
					<div
						style={{
							display: "flex",
							justifyContent: "center",
							alignItems: "center"
						}}
					>
						<img src={spinner} alt="Loading Contacts" />
					</div>
				) : (
					this.props.contacts.map(contact => (
						<div className="contactCard" key={contact.id}>
							<div className="display">
								<span>{contact.name}</span>
								<span>{contact.phone}</span>
								<span>{contact.email}</span>
								<Button
									onClick={() =>
										this.props.onDeleteButtonClick(
											contact.id
										)
									}
								>
									Delete
								</Button>
							</div>
							<UpdateComponent
								updateButton={update => this.onUpdate(update)}
								contact={contact}
							/>
							<p />
						</div>
					))
				)}
			</div>
		);
	}
}

const mapStateToProps = state => ({
	isLoading: state.loadingReducer.isLoading
});

export default connect(
	mapStateToProps,
	{}
)(DisplayComponent);
