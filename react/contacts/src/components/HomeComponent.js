import React, { Component } from "react";
import CreateComponent from "./CreateComponent";
import DisplayComponent from "./DisplayComponent";
import axios from "axios";
import { connect } from "react-redux";
import {
	getContacts,
	deleteContact,
	createContact,
	updateContact
} from "../actions/contacts";

export class HomeComponent extends Component {
	state = {
		inputFieldName: "",
		inputFieldMobile: "",
		inputFieldEmail: "",
		idForContacts: 1
	};
	componentWillMount() {
		this.props.getContacts();
	}
	onCreateButtonClick = () => {
		let contact = {
			name: this.state.inputFieldName,
			email: this.state.inputFieldEmail,
			phone: this.state.inputFieldMobile
		};
		this.props.createContact(contact);
	};
	onDeleteButtonClick = id => {
		this.props.deleteContact(id);
	};
	deleteAllContacts = () => {
		axios
			.get("http://192.168.1.32:5000/api/contacts/all")
			.then(res => {
				for (let c in res.data.data) {
					axios
						.delete(
							"http://192.168.1.32:5000/api/contacts/delete/" +
								res.data.data[c].id
						)
						.then(res => {
							this.props.getContacts();
						})
						.catch(err => console.log(err));
				}
			})
			.catch(err => console.log(err));
	};

	onUpdateButtonClick = contact => {
		this.props.updateContact(contact);
	};

	handleInput = e => {
		this.setState({ [e.target.name]: e.target.value });
	};
	render() {
		// console.log(this.state);
		return (
			<div className="homeComponent">
				<button onClick={this.deleteAllContacts}>Delete all</button>
				<CreateComponent
					handleInput={this.handleInput}
					onCreateButtonClick={this.onCreateButtonClick}
					name={this.state.inputFieldName}
					phone={this.state.inputFieldMobile}
					email={this.state.inputFieldEmail}
				/>

				<DisplayComponent
					contacts={this.props.contacts}
					onDeleteButtonClick={this.onDeleteButtonClick}
					onUpdateButtonClick={updatedContact =>
						this.onUpdateButtonClick(updatedContact)
					}
				/>
			</div>
		);
	}
}

const mapStateToProps = state => ({
	contacts: state.contactReducer.contacts
});
export default connect(
	mapStateToProps,
	{ getContacts, deleteContact, createContact, updateContact }
)(HomeComponent);
