import React from "react";
import { shallow, mount } from "enzyme";
import FormComponent from "../Form";

const wrapper = mount(<FormComponent />);

it("should have 1 div", () => {
	expect(wrapper.find("div").length).toBe(1);
});
it("should have 1 input", () => {
	expect(wrapper.find("input").length).toBe(1);
});
