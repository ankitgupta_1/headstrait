import React from "react";
import { shallow, mount } from "enzyme";
import UpdateComponent from "../UpdateComponent";

const updateContact = {
	id: "",
	name: "",
	phone: "",
	email: ""
};
const updateButton = jest.fn();
const updateWrapper = mount(
	<UpdateComponent contact={updateContact} updateButton={updateButton} />
);

describe("Test Create Contacts Component", () => {
	it("should render the component", () => {
		expect(updateWrapper).toMatchSnapshot();
	});

	it("should have exactly 1 button adn 3 inputs", () => {
		expect(updateWrapper.find("button").length).toBe(1);
		expect(updateWrapper.find("input").length).toBe(3);
	});

	it("should have the exact text Create Contact on the button", () => {
		expect(updateWrapper.find("button").text()).toBe("Update");
	});

	it("should have input value as in the state in the input fields", () => {
		expect(
			updateWrapper
				.find("input")
				.at(0)
				.prop("value")
		).toBe(updateWrapper.state().name);
		expect(
			updateWrapper
				.find("input")
				.at(1)
				.prop("value")
		).toBe(updateWrapper.state().phone);
		expect(
			updateWrapper
				.find("input")
				.at(2)
				.prop("value")
		).toBe(updateWrapper.state().email);
	});
	it("should have input value as in the state in the input fields", () => {
		const e = {
			target: {
				name: "name",
				value: "modified test"
			}
		};
		updateWrapper.instance().handleInput(e);
		expect(updateWrapper.state().name).toBe(e.target.value);
	});
});
