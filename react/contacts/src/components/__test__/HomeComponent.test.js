import React from "react";
import { shallow } from "enzyme";
import { HomeComponent } from "../HomeComponent";

const getContacts = jest.fn();
const contacts = [{}, {}, {}];

const wrapper = shallow(
	<HomeComponent getContacts={getContacts} contacts={contacts} />
);

describe("testing the input field properties in the state", () => {
	it("should have input value as in the state in the of property inputFieldName", () => {
		const e = {
			target: {
				name: "inputFieldName",
				value: "modified test"
			}
		};
		wrapper.instance().handleInput(e);
		expect(wrapper.state().inputFieldName).toBe(e.target.value);
	});
	it("should have input value as in the state in the of property inputFieldMobile", () => {
		const e = {
			target: {
				name: "inputFieldMobile",
				value: "modified test"
			}
		};
		wrapper.instance().handleInput(e);
		expect(wrapper.state().inputFieldMobile).toBe(e.target.value);
	});
	it("should have input value as in the state in the of property inputFieldEmail", () => {
		const e = {
			target: {
				name: "inputFieldEmail",
				value: "modified test"
			}
		};
		wrapper.instance().handleInput(e);
		expect(wrapper.state().inputFieldEmail).toBe(e.target.value);
	});
});
