import React from "react";
import { shallow, mount } from "enzyme";
import CreateComponent from "../CreateComponent";

const createContact = jest.fn();
const wrapper = mount(<CreateComponent createContact={createContact} />);

describe("Test Create Contacts Component", () => {
	it("should render the component", () => {
		expect(wrapper).toMatchSnapshot();
	});

	it("should have exactly 1 button adn 3 inputs", () => {
		expect(wrapper.find("button").length).toBe(1);
		expect(wrapper.find("input").length).toBe(3);
	});

	it("should have the exact text Create Contact on the button", () => {
		expect(wrapper.find("button").text()).toBe("Create Contact");
	});
});
