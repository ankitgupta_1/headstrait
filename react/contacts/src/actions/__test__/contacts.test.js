import {
	GET_CONTACTS,
	UPDATE_CONTACT,
	DELETE_CONTACT,
	CREATE_CONTACT
} from "../Type";

import * as action from "../contacts";
import moxios from "moxios";
import configureMockStore from "redux-mock-store";
import thunk from "redux-thunk";

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe("Testing contacts actions", () => {
	beforeEach(() => {
		moxios.install();
	});
	afterEach(() => {
		moxios.uninstall();
	});
	it("should create an action with type GET_CONTACTS and the payload should be same as the api response when the response is 20*", () => {
		const responseOfApi = [{}, {}, {}];
		moxios.stubRequest("http://192.168.1.32:5000/api/contacts/all", {
			status: 200,
			response: { data: responseOfApi }
		});
		const store = mockStore({});
		const expectedActions = [
			{
				type: GET_CONTACTS,
				payload: responseOfApi
			}
		];
		return store.dispatch(action.getContacts()).then(() => {
			expect(store.getActions()).toEqual(expectedActions);
		});
	});
	it("should create an action with type GET_CONTACTS and the payload should be same as the api response when the response is 20*", () => {
		const responseOfApi = [{}, {}, {}];
		moxios.stubRequest("http://192.168.1.32:5000/api/contacts/all", {
			status: 400,
			response: { data: responseOfApi }
		});
		const store = mockStore({});
		const expectedActions = [];
		return store.dispatch(action.getContacts()).then(() => {
			expect(store.getActions()).toEqual(expectedActions);
		});
	});
});
