import {
	GET_CONTACTS,
	CREATE_CONTACT,
	DELETE_CONTACT,
	UPDATE_CONTACT
} from "./Type";
import axios from "axios";
import { startLoading, stopLoading } from "./loadingAction";

export const getContacts = () => dispatch => {
	dispatch(startLoading());
	return axios
		.get("http://192.168.1.2:5000/api/contacts/all")
		.then(res => {
			dispatch(stopLoading());
			dispatch({
				type: GET_CONTACTS,
				payload: res.data.data
			});
		})
		.catch(err => {
			dispatch(stopLoading());
			console.log(err);
		});
};

export const deleteContact = id => dispatch => {
	axios
		.delete("http://192.168.1.32:5000/api/contacts/delete/" + id)
		.then(res => {
			dispatch({
				type: DELETE_CONTACT
			});
			dispatch(getContacts());
		})
		.catch(err => console.log(err));
};

export const createContact = contact => dispatch => {
	axios
		.post("http://192.168.1.32:5000/api/contacts/new", contact)
		.then(res => {
			dispatch({
				type: CREATE_CONTACT
			});
			dispatch(getContacts());
		})
		.catch(err => console.log(err));
};

export const updateContact = updatedContact => dispatch => {
	axios
		.put(
			"http://192.168.1.32:5000/api/contacts/update/" + updatedContact.id,
			updatedContact
		)
		.then(res => {
			dispatch({
				type: UPDATE_CONTACT
			});
			dispatch(getContacts());
		})
		.catch(err => console.log(err));
};
