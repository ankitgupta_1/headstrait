package RohanShah;

import java.util.*;
// import java.util.*;

import hibernate.Laptop;
import hibernate.Student;
import jdbc_exercise.query;
import json.ObjToJSON;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

// import RohanShah.Calculator;
// import pass.passByValue;
// import pass.Test;
// import pass.passByRefrence;
// import student.details;
// import demopkg.*;

public class App {
    public static void main(String args[]) {
        // Hibernate

        Student stud = new Student();
        stud.setSname("gupta");
        stud.setCollname("APSIT");

        Laptop lap = new Laptop();
        // lap.setLid(11);
        lap.setLname("Lenovo");
        lap.setStud(stud);
        // stud.getLap().add(lap);

        Laptop lap1 = new Laptop();
        // lap1.setLid(12);
        lap1.setLname("Dell");
        lap1.setStud(stud);

        Configuration config = new Configuration().configure().addAnnotatedClass(Student.class)
                .addAnnotatedClass(Laptop.class);
        SessionFactory sf = config.buildSessionFactory();
        Session session = sf.openSession();
        Transaction tx = session.beginTransaction();
        List<Object[]> obj = null;
        try {
            session.save(stud);
            session.save(lap);
            session.save(lap1);

            Query query = session.createQuery("from Student");
            obj = query.getResultList();

            System.out.println("JSON response: " + ObjToJSON.Obj2JSON(obj));

            tx.commit();

        } catch (Exception e) {
            tx.rollback();
            e.printStackTrace();
        }

        // System.out.println("Hello World!");
        // System.out.println("+++++++++++++++++++++");

        // // Pass by value
        // int x = 5;
        // passByValue pb = new passByValue();
        // int y = pb.change(x);
        // System.out.println("From current class: " + x);
        // System.out.println("From pass by value: " + y);

        // // pass by refrence
        // Test t = new Test(18);
        // passByRefrence.change(t);
        // System.out.println("From pass by refrence: " + t.x);
        // System.out.println("+++++++++++++++++++++");

        // details student1 = new details();
        // student1.studentName("Rohan Shah");
        // student1.studentRollNo(57);

        // public static void main(String args[]) {
        // Calculator ap = new Calculator();
        // ap.calculate();
        // }
        // int i = 5;
        // query q = new query();
        // // q.create_table();
        // System.out.println("Select required options\n 1:View table \n 2:Insert
        // values\n 3:Update value ");
        // Scanner sc = new Scanner(System.in);
        // while (i > 1) {
        // System.out.println("+++++++++++++++++++++++++++++++");
        // int option = sc.nextInt();

        // switch (option) {
        // case 1:
        // q.select();
        // break;
        // case 2:
        // q.insert();
        // break;
        // case 3:
        // q.update();
        // break;
        // default:
        // System.out.println("Invalid Option");
        // break;

        // }
        // System.out.println("+++++++++++++++++++++++++++++++");
        // i++;
        // }
        // sc.close();
        // }
    }
}
