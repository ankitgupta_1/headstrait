package RohanShah;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Calculator {

    public void calculate() {
        ExecutorService exec = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
        System.out.println(Runtime.getRuntime().availableProcessors());
        long st = System.currentTimeMillis();
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the First number: ");
        int num1 = sc.nextInt();
        System.out.println("Enter the Second number: ");
        int num2 = sc.nextInt();
        CompletableFuture<Integer> cf1 = CompletableFuture.supplyAsync(() -> addition(num1, num2), exec);
        CompletableFuture<Integer> cf2 = CompletableFuture.supplyAsync(() -> substraction(num1, num2), exec);
        CompletableFuture<Integer> cf3 = CompletableFuture.supplyAsync(() -> multiplication(num1, num2), exec);
        CompletableFuture<Integer> cf4 = CompletableFuture.supplyAsync(() -> division(num1, num2), exec);

        try {
            System.out.println("Addition of " + num1 + " and " + num2 + " is: " + cf1.get());
            System.out.println("Substraction of " + num1 + " and " + num2 + " is: " + cf2.get());
            System.out.println("Division of " + num1 + " and " + num2 + " is: " + cf4.get());
            System.out.println("Multiplication of " + num1 + " and " + num2 + " is: " + cf3.get());

        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        long et = System.currentTimeMillis();
        exec.shutdown();
        System.out.println("Start time: " + st + " End Time: " + et + "Difference: " + (et - st));
        sc.close();
    }

    public int addition(int num1, int num2) {
        try {
            Thread.sleep(1000);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return num1 + num2;
    }

    public int substraction(int num1, int num2) {
        try {
            Thread.sleep(2000);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return num1 - num2;
    }

    public int multiplication(int num1, int num2) {
        try {
            Thread.sleep(5000);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return num1 * num2;
    }

    public int division(int num1, int num2) {
        try {
            Thread.sleep(3000);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return num1 / num2;
    }
}