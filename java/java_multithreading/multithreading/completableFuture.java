package multithreading;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class completableFuture {

    int k = 0;

    public static void main(String args[]) {
        completableFuture ap = new completableFuture();
        ap.mockMain();
    }

    public void mockMain() {
        ExecutorService exec = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
        System.out.println(Runtime.getRuntime().availableProcessors());
        long st = System.currentTimeMillis();
        for (int i = 0; i < 100; i++) {
            // CompletableFuture.supplyAsync(() -> func1(k), exec).thenApply(op ->
            // func2(op)).thenApply(op -> func3(op))
            // .thenApply(op -> func4(op)).thenAccept(op -> {
            // try {
            // func5(op);
            // } catch (InterruptedException e) {
            // e.printStackTrace();
            // }
            // });
            CompletableFuture<String> cf1 = CompletableFuture.supplyAsync(() -> func1(k), exec);
            CompletableFuture<String> cf2 = CompletableFuture.supplyAsync(() -> func2("Rohan "), exec);
            CompletableFuture<String> cf3 = CompletableFuture.supplyAsync(() -> func3("Shah "), exec);
            CompletableFuture.allOf(cf1, cf2, cf3).join();
            try {
                System.out.println("CF1: " + cf1.get());
                System.out.println("CF2: " + cf2.get());
                System.out.println("CF3: " + cf3.get());

            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
            System.out.println("Loop: " + i);
        }
        long et = System.currentTimeMillis();
        exec.shutdown();
        System.out.println("Start time: " + st + "End Time: " + et + "Difference: " + (et - st));

    }

    public String func1(int i) {
        System.out.println("Task: " + i);
        try {
            Thread.sleep(8000);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ("Func()1 Reached");
    }

    public String func2(String str) {
        return str + ("Func()2 Reached");
    }

    public String func3(String str) {
        return str + ("Func()3 Reached");
    }

    public String func4(String str) {
        return str + ("Func()4 Reached");
    }

    public String func5(String str) throws InterruptedException {
        System.out.println("thread: " + Thread.currentThread().getName());
        k++;
        if (k % 2 == 0) {
            Thread.sleep(4000);
        }
        return str + ("Func()5 Reached");
    }

    public void calculate() {
    }
}