package com.exercise.products.service;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.exercise.products.repository.ProductsRepository;
import com.exercise.products.util.Products;
import com.exercise.products.util.ResponseCreator;

@Service
public class ProductsService {

	@Autowired
	ProductsRepository prs;

	public ResponseEntity<String> getProducts() {
		try {
			List<Products> s = prs.findAll();
			if (s.size() == 0) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ResponseCreator
						.response("Data retrieved for all the products", "No products present in database", 400));
			} else {
				return ResponseEntity.ok(ResponseCreator.response("Data retrieved for all the products", s, 200));
			}
		} catch (Exception e) {
			System.out.println("Exception from getProducts() : " + e);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body(ResponseCreator.response("server access error", null, 400));
		}
	}

	public ResponseEntity<Object> updateProduct(Products product) {
		try {
			HashMap m = Validation.validate(product);

			if (m.isEmpty()) {
				Optional<Products> p = prs.findById(product.getProductId());
				System.out.println(p);
				if (!p.isPresent()) {
					return ResponseEntity.status(HttpStatus.BAD_REQUEST)
							.body(ResponseCreator.response("Could not found product for given id", null, 400));
				} else {
					prs.save(product);
					return ResponseEntity.ok(ResponseCreator.response("Updated product for given id", product, 200));
				}
			} else {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ResponseCreator.response(m, null, 400));
			}

		} catch (Exception e) {
			System.out.println("Exception from updateProduct() : " + e);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body(ResponseCreator.response("server access error", null, 400));
		}
	}

	public ResponseEntity<Object> addProduct(Products product) {
		try {
			HashMap m = Validation.validate(product);
			if (m.isEmpty()) {
				Products p = prs.findByProductName(product.getProductName());
				System.out.println(p);
				if (p == null) {
					prs.save(product);
					return ResponseEntity.ok(ResponseCreator.response("Added new product", product, 200));
				} else {
					return ResponseEntity.status(HttpStatus.BAD_REQUEST)
							.body(ResponseCreator.response("Could not add new product", null, 400));
				}
			} else {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ResponseCreator.response(m, null, 400));
			}
		} catch (Exception e) {
			System.out.println("Exception from addProduct() : " + e);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body(ResponseCreator.response("server access error", null, 400));
		}
	}

	public ResponseEntity<String> deleteProduct(int product_id) {
		try {
			Optional<Products> p = prs.findById(product_id);
			if (!p.isPresent()) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST)
						.body(ResponseCreator.response("Could not find product for given id", null, 400));
			} else {
				prs.deleteById(product_id);
				return ResponseEntity.ok(ResponseCreator.response("Deleted product successfully", p, 200));
			}
		} catch (Exception e) {
			System.out.println("Exception from deleteProduct() : " + e);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body(ResponseCreator.response("server access error", null, 400));
		}
	}

}
