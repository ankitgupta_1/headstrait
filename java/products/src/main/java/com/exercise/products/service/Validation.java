package com.exercise.products.service;

import java.util.HashMap;
import java.util.Map;

import com.exercise.products.util.Products;
import com.exercise.products.util.Users;

public class Validation {
	public static HashMap validate(Products product) {
		HashMap<String, String> error = new HashMap<>();

		if (product.getProductName().length() < 6 || product.getProductName().length() > 50) {
			error.put("productName", "Product Name should be between 6 to 50 characters");
		}

		if (product.getProductDescription().length() < 10 || product.getProductDescription().length() > 500) {
			error.put("productDescription", "Product Description should be between 10 to 500 characters");
		}

		if (product.getProductCategory().length() < 3 || product.getProductCategory().length() > 30) {
			error.put("productCategory", "Product Category should be between 3 to 30 characters");
		}

		return error;
	}

	public static HashMap validate(Users user) {
		HashMap<String, String> error = new HashMap<>();
		if (user.getUserName().length() < 3 || user.getUserName().length() > 30) {
			error.put("userName", "User Name should be between 3 to 30 characters");
		}

		if (user.getUserPassword().length() < 6 || user.getUserPassword().length() > 30) {
			error.put("userPassword", "Password should be between 6 to 30 characters");
		}

		if (user.getUserEmail() == "") {
			error.put("userEmail", "Enter a valid email");
		}

		return error;
	}

	public static HashMap validate(Map<String, Object> data) {
		HashMap<String, String> error = new HashMap<>();
		if ((String) data.get("userEmail") == "") {
			error.put("userEmail", "Enter a valid email");
		}

		if (((String) data.get("userPassword")).length() < 6 || ((String) data.get("userPassword")).length() > 30) {
			error.put("userPassword", "Password should be between 6 to 30 characters");
		}

		if (!((String) data.get("userPassword")).equals(((String) data.get("userConfirmPassword")))) {
			error.put("userConfirmPassword", "Password does not match");
		}

		return error;
	}

}
