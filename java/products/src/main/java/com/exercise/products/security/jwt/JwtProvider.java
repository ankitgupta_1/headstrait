package com.exercise.products.security.jwt;

import io.jsonwebtoken.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Component
public class JwtProvider {

    private static final Logger logger = LoggerFactory.getLogger(JwtProvider.class);

    // @Value("${example.app.jwtSecret}")
    private String jwtSecret = "ankitg";

    // @Value("${example.app.jwtExpiration}")
    private int jwtExpiration = 86400;

    public String generateJwtToken(Authentication authentication) {

        UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();
        // user principal
        Map<String, Object> usr = new HashMap<>();
        usr.put("userId", userPrincipal.getUserId());
        usr.put("userName", userPrincipal.getUserName());
        usr.put("userEmail", userPrincipal.getUsername());
        usr.put("userPassword", userPrincipal.getPassword());
        usr.put("userPhone", userPrincipal.getUserPhone());
        usr.put("isAdmin", userPrincipal.isAdmin());
        // subject can be username also
        return Jwts.builder().setSubject((userPrincipal.getUsername()))// email/usernmae/any unique value
                .setClaims(usr)// seting the claims in the jwt payload
                .setIssuedAt(new Date()).setExpiration(new Date((new Date()).getTime() + jwtExpiration * 1000))
                .signWith(SignatureAlgorithm.HS512, jwtSecret).compact();
    }

    public boolean validateJwtToken(String authToken) {
        try {
            Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken);
            return true;
        } catch (SignatureException e) {
            logger.error("Invalid JWT signature -> Message: {} ", e);
        } catch (MalformedJwtException e) {
            logger.error("Invalid JWT token -> Message: {}", e);
        } catch (ExpiredJwtException e) {
            logger.error("Expired JWT token -> Message: {}", e);
        } catch (UnsupportedJwtException e) {
            logger.error("Unsupported JWT token -> Message: {}", e);
        } catch (IllegalArgumentException e) {
            logger.error("JWT claims string is empty -> Message: {}", e);
        }

        return false;
    }

    public String getEmailFromJwtToken(String token) {
        Claims c = Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(token).getBody();
        return (String) c.get("email");
    }
}