package com.exercise.products.security.jwt;

import com.exercise.products.util.Users;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import java.util.Collection;
import java.util.Objects;

public class UserPrincipal implements UserDetails {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    // ========================

    // create getters and setters + constructor + build method for all the
    // attributes in the user entity
    private int userId;

    private String userName;

    private String userEmail;
    @JsonIgnore
    private String userPassword;

    private long userPhone;

    private boolean isAdmin;

    public static UserPrincipal build(Users user) {
        System.out.println(user);
        return new UserPrincipal(user.getUserId(), user.getUserName(), user.getUserEmail(), user.getUserPassword(),
                user.getUserPhone(), user.isAdmin());
    }

    // ============================================================
    // overridding the methods of user details interface since we are implementing
    // it
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        UserPrincipal user = (UserPrincipal) o;
        return Objects.equals(userId, user.userId);
    }

    public UserPrincipal(int userId, String userName, String userEmail, String userPassword, long userPhone,
            boolean isAdmin) {
        this.userId = userId;
        this.userName = userName;
        this.userEmail = userEmail;
        this.userPassword = userPassword;
        this.userPhone = userPhone;
        this.isAdmin = isAdmin;
    }

    public int getUserId() {
        return userId;
    }

    public String getUserName() {
        return userName;
    }

    // public String getUserEmail() {
    // return userEmail;
    // }

    // public String getUserPassword() {
    // return userPassword;
    // }

    public long getUserPhone() {
        return userPhone;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    @Override
    public String getPassword() {
        return userPassword;
    }

    @Override
    public String getUsername() {
        return userEmail;
    }

}