package com.exercise.products.repository;

import com.exercise.products.util.Products;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductsRepository extends JpaRepository<Products, Integer> {

	Products findByProductName(String productName);

}
