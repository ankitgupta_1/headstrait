package com.exercise.products.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

import com.exercise.products.repository.UsersRepository;
import com.exercise.products.security.jwt.JwtProvider;
import com.exercise.products.security.jwt.JwtResponse;
import com.exercise.products.util.Users;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthRestAPIs {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UsersRepository userRepository;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    JwtProvider jwtProvider;

    // ---------------------------------------------------------------------------------------------------//
    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@RequestBody Map<String, Object> req) {

        // provide username/email and password to the
        // UsernamePasswordAuthenticationToken class
        // which is then applied used to initialize the authentication manager
        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                (String) req.get("userEmail"), (String) req.get("userPassword")));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        // setting the authentication object in the security context to use it in the
        // application
        String jwt = jwtProvider.generateJwtToken(authentication);
        // generate jwt by using the authentication manager class
        return ResponseEntity.ok(new JwtResponse(jwt));
    }
    // ---------------------------------------------------------------------------------------------------//

    @PostMapping("/signup")
    public ResponseEntity<String> registerUser(@RequestBody Map<String, Object> req) {
        if (userRepository.existsByUserName((String) req.get("userName"))) {
            // return responsecreator response
            return new ResponseEntity<String>("Fail -> Username is already taken!", HttpStatus.BAD_REQUEST);
        }

        if (userRepository.existsByUserEmail((String) req.get("userEmail"))) {
            // return responsecreator response
            return new ResponseEntity<String>("Fail -> Email is already in use!", HttpStatus.BAD_REQUEST);
        }

        // Creating user's account
        Users user = new Users((String) req.get("userName"), (String) req.get("userEmail"),
                encoder.encode((String) req.get("userPassword")), (int) req.get("userPhone"),
                (boolean) req.get("admin"));
        userRepository.save(user);

        return ResponseEntity.ok().body("User registered successfully!");
    }
}