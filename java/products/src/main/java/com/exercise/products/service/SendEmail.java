package com.exercise.products.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class SendEmail {

    @Autowired
    private JavaMailSender mailSender;

    public int sendOTP(Map<String, Object> credentials) {

        // Generate 4 digit random OTP
        try {
            int otp = (int) (Math.random() * 9000) + 1000;
            System.out.println("otp generated : " + otp);

            SimpleMailMessage message = new SimpleMailMessage();

            message.setTo((String) credentials.get("userEmail"));
            message.setSubject("OTP for Reset Password");

            String text = "Your one time password is <b>" + String.valueOf(otp) + "</b>.\nDO not share with anyone.";
            message.setText(text);

            System.out.println("message : " + message);
            // System.out.println("Message : " + message);

            mailSender.send(message);
            return otp;

        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }

    }

}