package com.exercise.products.util;

import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;

public class ObjToJSON {
    public static String obj2JSON(Object obj) {
        ObjectMapper o = new ObjectMapper();
        try {
            String jsonStr = o.writeValueAsString(obj);
            return jsonStr;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
