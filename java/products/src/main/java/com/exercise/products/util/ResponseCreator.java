package com.exercise.products.util;

import java.util.HashMap;
import java.util.List;

public class ResponseCreator {
	public static String response(String msg, List<Object> data, int status) {
		MyObject o = new MyObject();
		o.setMessage(msg);
		o.setListData(data);
		o.setStatus(status);
		System.out.println("obj: " + ObjToJSON.obj2JSON(o));
		return ObjToJSON.obj2JSON(o);
	}

	public static String response(String msg, Object data, int status) {
		MyObject o = new MyObject();
		o.setMessage(msg);
		o.setData(data);
		o.setStatus(status);
		System.out.println("obj: " + ObjToJSON.obj2JSON(o));
		return ObjToJSON.obj2JSON(o);
	}

	public static Object response(HashMap msg, Object data, int status) {
		MyObject o = new MyObject();
		o.setHashMessage(msg);
		o.setData(data);
		o.setStatus(status);
		System.out.println("obj: " + ObjToJSON.obj2JSON(o));
		return ObjToJSON.obj2JSON(o);
	}
}
