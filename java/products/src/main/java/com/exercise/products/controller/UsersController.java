package com.exercise.products.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.exercise.products.util.Users;
import com.exercise.products.service.SendEmail;
import com.exercise.products.service.UsersService;

@RestController
@RequestMapping("/api")
public class UsersController {

	@Autowired
	UsersService userService;

	@Autowired
	SendEmail emailService;

	Map<String, Object> resetCredentials;

	@GetMapping("/getUsers")
	public ResponseEntity<String> getUsers() {
		return userService.getUsers();
	}

	@PutMapping("/updateUser/{id}")
	public ResponseEntity<Object> updateUser(@RequestBody Users user, @PathVariable("id") int user_id) {
		user.setUserId(user_id);
		return userService.updateUser(user);
	}

	@PostMapping("/addUser")
	public ResponseEntity<Object> addUser(@RequestBody Users user) {
		return userService.addUser(user);
	}

	@DeleteMapping("/deleteUser/{id}")
	public ResponseEntity<String> deleteUser(@PathVariable("id") int user_id) {
		return userService.deleteUser(user_id);
	}

	@PostMapping("/login")
	public ResponseEntity<Object> login(@RequestBody Map<String, Object> credentials) {
		return userService.login(credentials);
	}

	@PostMapping("/sendOTP")
	public ResponseEntity<Object> sendOTP(@RequestBody Map<String, Object> credentials) {
		resetCredentials = credentials;
		return userService.sendOTP(resetCredentials);
	}

	@PostMapping("/verifyOTP")
	public ResponseEntity<Object> verifyOTP(@RequestBody Map<String, Object> userOTP) {
		return userService.verifyOTP((int) userOTP.get("userOTP"));
	}

	@GetMapping("/resetPassword")
	public ResponseEntity<Object> resetPassword() {
		return userService.resetPassword(resetCredentials);
	}

}
