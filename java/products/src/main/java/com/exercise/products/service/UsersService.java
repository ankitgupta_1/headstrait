package com.exercise.products.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;

import com.exercise.products.repository.UsersRepository;
import com.exercise.products.security.jwt.UserPrincipal;
import com.exercise.products.util.ResponseCreator;
import com.exercise.products.util.Users;

@Service
public class UsersService implements UserDetailsService {
	@Autowired
	private UsersRepository urs;
	// @Autowired
	// private SendEmail sendEmail;
	@Autowired
	private JavaMailSender mailSender;

	int otp;

	public ResponseEntity<String> getUsers() {
		try {
			List<Users> s = urs.findAll();
			if (s.size() == 0) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ResponseCreator
						.response("Data retrieved for all the users", "No user present in database", 400));
			} else {
				return ResponseEntity.ok(ResponseCreator.response("Data retrieved for all the users", s, 200));
			}
		} catch (Exception e) {
			System.out.println("Exception from getUsers() : " + e);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body(ResponseCreator.response("server access error", null, 400));
		}
	}

	public ResponseEntity<Object> updateUser(Users user) {
		try {
			HashMap m = Validation.validate(user);
			if (m.isEmpty()) {
				Optional<Users> u = urs.findById(user.getUserId());
				if (!u.isPresent()) {
					return ResponseEntity.status(HttpStatus.BAD_REQUEST)
							.body(ResponseCreator.response("Could not found user for given id", null, 400));
				} else {
					urs.save(user);
					return ResponseEntity.ok(ResponseCreator.response("Updated user for given id", user, 200));
				}
			} else {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ResponseCreator.response(m, null, 400));
			}
		} catch (Exception e) {
			System.out.println("Exception from updateUser() : " + e);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body(ResponseCreator.response("server access error", null, 400));
		}
	}

	public ResponseEntity<Object> addUser(Users user) {
		try {
			System.out.println("call to addUser");
			HashMap m = Validation.validate(user);
			if (m.isEmpty()) {
				Users u = urs.findByUserEmail(user.getUserEmail());
				if (u == null) {
					user.setUserPassword(BCrypt.hashpw(user.getUserPassword(), BCrypt.gensalt(10)));
					System.out.println("hash password: " + user.getUserPassword());
					urs.save(user);
					return ResponseEntity.ok(ResponseCreator.response("Added new user", user, 200));
				} else {
					return ResponseEntity.status(HttpStatus.BAD_REQUEST)
							.body(ResponseCreator.response("Could not add new user", null, 400));
				}
			} else {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ResponseCreator.response(m, null, 400));
			}
		} catch (Exception e) {
			System.out.println("Exception from addUser() : " + e);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body(ResponseCreator.response("server access error", null, 400));
		}
	}

	public ResponseEntity<String> deleteUser(int user_id) {
		try {
			Optional<Users> u = urs.findById(user_id);
			if (!u.isPresent()) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST)
						.body(ResponseCreator.response("Could not find user for given id", null, 400));
			} else {
				urs.deleteById(user_id);
				return ResponseEntity.ok(ResponseCreator.response("Deleted user successfully", u, 200));
			}
		} catch (Exception e) {
			System.out.println("Exception from deleteUser() : " + e);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body(ResponseCreator.response("server access error", null, 400));
		}
	}

	public ResponseEntity<Object> login(Map<String, Object> credentials) {
		try {
			// sendEmail.sendOTP((String) credentials.get("userEmail"));
			HashMap m = Validation.validate(credentials);
			if (m.isEmpty()) {
				Users u = urs.findByUserEmail((String) credentials.get("userEmail"));
				if (u == null) {
					return ResponseEntity.status(HttpStatus.BAD_REQUEST)
							.body(ResponseCreator.response("Invalid email or password", null, 400));
				} else {
					if (BCrypt.checkpw((String) credentials.get("userPassword"), u.getUserPassword()))
						return ResponseEntity.ok(ResponseCreator.response("Login in successful", null, 200));
					else
						return ResponseEntity.status(HttpStatus.BAD_REQUEST)
								.body(ResponseCreator.response("Invalid email or password", null, 400));
				}
			} else {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ResponseCreator.response(m, null, 400));
			}
		} catch (Exception e) {
			System.out.println("Exception from login() : " + e);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body(ResponseCreator.response("server access error", null, 400));
		}
	}

	public ResponseEntity<Object> resetPassword(Map<String, Object> credentials) {
		try {
			HashMap m = Validation.validate(credentials);
			if (m.isEmpty()) {
				Users u = urs.findByUserEmail((String) credentials.get("userEmail"));
				if (u == null) {
					return ResponseEntity.status(HttpStatus.BAD_REQUEST)
							.body(ResponseCreator.response("Invalid email", null, 400));
				} else {
					System.out.println("password : " + (String) credentials.get("userPassword"));
					String hashedPassword = BCrypt.hashpw((String) credentials.get("userPassword"), BCrypt.gensalt(10));
					u.setUserPassword(hashedPassword);
					System.out.println(u.getUserEmail() + " " + u.getUserPassword());
					urs.save(u);
					return ResponseEntity.ok(ResponseCreator.response("Reset password successful", null, 200));
				}
			} else {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ResponseCreator.response(m, null, 400));
			}
		} catch (Exception e) {
			System.out.println("Exception from login() : " + e);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body(ResponseCreator.response("server access error", null, 400));
		}
	}

	public ResponseEntity<Object> sendOTP(Map<String, Object> credentials) {
		try {
			Users u = urs.findByUserEmail((String) credentials.get("userEmail"));
			if (u == null) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST)
						.body(ResponseCreator.response("Invalid email", null, 400));
			} else {
				otp = (int) (Math.random() * 9000) + 1000;
				System.out.println("otp generated : " + otp);

				SimpleMailMessage message = new SimpleMailMessage();

				message.setTo((String) credentials.get("userEmail"));
				message.setSubject("OTP for Reset Password");

				String text = "Your one time password is <b>" + String.valueOf(otp)
						+ "</b>.\nDO not share with anyone.";
				message.setText(text);

				System.out.println("message : " + message);
				// System.out.println("Message : " + message);

				mailSender.send(message);
				return ResponseEntity.ok(ResponseCreator.response("OTP sent successfully", null, 200));
			}

		} catch (Exception e) {
			System.out.println("Exception from sendOTP() : " + e);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body(ResponseCreator.response("server access error", null, 400));
		}
	}

	public ResponseEntity<Object> verifyOTP(int userOTP) {
		try {
			if (userOTP == otp)
				return ResponseEntity.ok(ResponseCreator.response("OTP matched", null, 200));
			else
				return ResponseEntity.status(HttpStatus.BAD_REQUEST)
						.body(ResponseCreator.response("OTP not matched", null, 400));

		} catch (Exception e) {
			System.out.println("Exception from verifyOTP() : " + e);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body(ResponseCreator.response("server access error", null, 400));
		}
	}

	@Transactional
	public UserDetails loadUserByEmail(String email) throws UsernameNotFoundException {

		Users user = urs.findByUserEmail(email)
				.orElseThrow(() -> new UsernameNotFoundException("email Not Found with -> email : " + email));
		System.out.println(user.getUserEmail() + "   " + email);
		return UserPrincipal.build(user);
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Users user = urs.findByUserEmail(username)
				.orElseThrow(() -> new UsernameNotFoundException("username Not Found with -> username : " + username));
		System.out.println(user.getUserName() + " " + username);
		return UserPrincipal.build(user);
	}

}
