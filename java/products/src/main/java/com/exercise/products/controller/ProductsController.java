package com.exercise.products.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.exercise.products.service.ProductsService;
import com.exercise.products.util.Products;

@RestController
@RequestMapping("/api")
public class ProductsController {
	@Autowired
	ProductsService productService;

	@GetMapping("/getProducts")
	public ResponseEntity<String> getProducts() {
		return productService.getProducts();
	}

	@PutMapping("/updateProduct/{id}")
	public ResponseEntity<Object> updateProduct(@RequestBody Products product, @PathVariable("id") int product_id) {
		product.setProductId(product_id);
		return productService.updateProduct(product);
	}

	@PostMapping("/addProduct")
	public ResponseEntity<Object> addProduct(@RequestBody Products product) {
		return productService.addProduct(product);
	}

	@DeleteMapping("/deleteProduct/{id}")
	public ResponseEntity<String> deleteProduct(@PathVariable("id") int product_id) {
		return productService.deleteProduct(product_id);
	}

}
