import java.util.Scanner;

class DaysOfWeek {
    public static void main(String args[]) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter a num from 1 to 7 to get day : ");
        int dayNumber = sc.nextInt();
        sc.close();
        switch (dayNumber) {
        case 1:
            System.out.println("It's Monday");
            break;
        case 2:
            System.out.println("It's Tuesday");
            break;
        case 3:
            System.out.println("It's Wednesday");
            break;
        case 4:
            System.out.println("It's Thursday");
            break;
        case 5:
            System.out.println("It's Friday");
            break;
        case 6:
            System.out.println("It's Saturday");
            break;
        case 7:
            System.out.println("It's Sunday");
            break;
        default:
            System.out.println("You sure that you entered correct number?");
        }
    }
}