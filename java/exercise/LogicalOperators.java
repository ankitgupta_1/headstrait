import java.util.Scanner;

class LogicalOperators {
    public static void main(String args[]) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter two numbers: ");
        int num1 = sc.nextInt();
        int num2 = sc.nextInt();
        sc.close();
        System.out.println("Enter two boolean values: ");
        boolean bool1 = sc.nextBoolean();
        boolean bool2 = sc.nextBoolean();
        System.out.println();
        if (bool1 && bool2)
            System.out.println("AND operation is TRUE");
        else
            System.out.println("AND operation is FALSE");
        if (bool1 || bool2)
            System.out.println("OR operation is TRUE");
        else
            System.out.println("OR operation is FALSE");

        System.out.println();
        if (num1 != num2)
            System.out.println("Numbers are not Equal");

        if (num1 > num2)
            System.out.println(num1 + " is greater than " + num2);
        else if (num1 < num2)
            System.out.println(num1 + " is less than " + num2);
        else if (num1 == num2)
            System.out.println("Numbers are Equal");

        if (num1 >= num2)
            System.out.println(num1 + " is greater than or equal to" + num2);
        else if (num1 <= num2)
            System.out.println(num1 + " is less than or equal to " + num2);

    }
}