import java.util.Scanner;

class Gcd {
    public static void main(String args[]) {
        // int num = 13;
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter two numbers to find its GCD : ");
        int num1 = sc.nextInt();
        int num2 = sc.nextInt();
        sc.close();
        if (num1 < num2) {
            for (int i = num1; i >= 2; i--) {
                if (num1 % i == 0 && num2 % i == 0) {
                    System.out.println("GCD for " + num1 + " and " + num2 + " is " + i);
                    break;
                }
            }
        }

        else
            for (int i = num2; i >= 2; i--) {
                if (num1 % i == 0 && num2 % i == 0) {
                    System.out.println("GCD for " + num1 + " and " + num2 + " is " + i);
                    break;
                }
            }
    }

}