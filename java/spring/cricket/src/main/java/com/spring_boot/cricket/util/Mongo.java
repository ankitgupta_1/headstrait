package com.spring_boot.cricket.util;

import java.util.ArrayList;
import java.util.List;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;

public class Mongo {
	
	public static void main(String args[]) {
		MongoClient mongo = new MongoClient("localhost", 27017);
		DB db = mongo.getDB("sports");
		DBCollection collection = db.getCollection("sports_info");
		DBCursor cursor;
		
//		Create
		List<String> sports = new ArrayList<>();
		sports.add("Cricket");
		sports.add("Football");
		sports.add("Table Tennis");
		sports.add("Basketball");
		
		List<Long> phone = new ArrayList<>();
		phone.add(7894561230L);
		phone.add(7456320189L);
		phone.add(8746015562L);
		
		BasicDBObject address = new BasicDBObject("line_1","101/B Kisika Tower")
				.append("landmark","Rassaz Castle")
				.append("pincode", 400093)
				.append("city", "Mumbai")
				.append("state", "Maharashtra")
				.append("country", "India");
		
		DBObject obj = new BasicDBObject("_id",4).append("name", "Ankit Gupta")
							.append("plays",sports)
							.append("address", address)
							.append("age", 22)
							.append("phone", phone)
							.append("email","ankit@mail.com");
		collection.insert(obj);
		
		System.out.println("Cursor after insert : ");
		cursor = collection.find();
		while(cursor.hasNext()) {
			System.out.println(cursor.next());
		}
		
//		Update
		BasicDBObject search = new BasicDBObject().append("_id", 2);
		BasicDBObject update = new BasicDBObject();
		update.append("$set", new BasicDBObject().append("address.pincode", 400601));
		collection.updateMulti(search, update);
		
		System.out.println("Cursor after update : ");
		cursor = collection.find();
		while(cursor.hasNext()) {
			System.out.println(cursor.next());
		}
		
//		Delete
		BasicDBObject delete = new BasicDBObject().append("_id", 4);
		collection.remove(delete);
		
		System.out.println("Cursor after delete : ");
		cursor = collection.find();
		while(cursor.hasNext()) {
			System.out.println(cursor.next());
		}
		
		
//		Find
		BasicDBObject findQuery = new BasicDBObject().append("address.city", "Thane");
		BasicDBObject projection = new BasicDBObject().append("address.city", 1).append("name",1).append("phone", 1);
		DBCursor find = collection.find(findQuery,projection);
		
		System.out.println("Cursor after find : ");
		while(find.hasNext()) {
			System.out.println(find.next());
		}
		
	}
	

}
