package com.spring_boot.cricket.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.query.Query;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.spring_boot.cricket.util.Players;
import com.spring_boot.cricket.util.ResponseCreator;
import com.spring_boot.cricket.util.Teams;

@Repository
@Transactional
public class MyRepository {
	
	@PersistenceContext
	EntityManager em;
	
	public List<Object> getTeams() throws Exception{
		try {
			Query query = (Query) em.createNativeQuery("select * from teams");
			query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
			List<Object> s = query.list();
			if(s.size() == 0)
				return s;
			else
				return s;
		}
		catch(Exception e) {
			throw new Exception();
		}
		finally {
			em.close();
		}
	}
	
	public List<Object> getPlayers() throws Exception{
		try {
			Query query = (Query) em.createNativeQuery("select * from players");
			query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
			List<Object> s = query.list();
			if(s.size() == 0)
				return s;
			else
				return s;
		}
		catch(Exception e) {
			throw new Exception();
		}
		finally {
			em.close();
		}
	}
	
	public Teams updateTeamById(Teams team) throws Exception{
		try {
			Teams t = em.find(Teams.class, team.getTeam_id());
			if(t == null) {
				return t;
			}
			else {
				em.merge(team);
				em.flush();
				return team;
			}
		}
		catch(Exception e) {
			throw new Exception();
		}
		finally {
			em.close();
		}
	}
	
	public Players updatePlayerById(Players player, int id) throws Exception{
		try {
			player.setT_id(em.find(Teams.class,id));
			Players p = em.find(Players.class, player.getPlayer_id());
			if(p == null) {
				return p;
			}
			else {
				em.merge(player);
				em.flush();
				return player;
			}
		}
		catch(Exception e) {
			throw new Exception();
		}
		finally {
			em.close();
		}
	}
	
	public Teams newTeam(Teams team) throws Exception{
		try {
//				Teams t = em.find(Teams.class, team.getName());
//				if(t == null){
					em.persist(team);
					em.flush();
					return team;
//				}
//				else {
//					return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ResponseCreator.response("Team already exist", null, 400));
//	
//				}
		}
		catch(Exception e) {
			throw new Exception();
		}
		finally {
			em.close();
		}
	}
	
	public Players newPlayer(Players player, int team_id) throws Exception{
		try {
				player.setT_id(em.find(Teams.class, team_id));
				em.persist(player);
				em.flush();
				return player;
		}
		catch(Exception e) {
			throw new Exception();
		}
		finally {
			em.close();
		}
	}
	
	public Players deletePlayer(int player_id) throws Exception{
		try {
			Players player = em.find(Players.class,player_id);
			if(player == null) {
				return player;
			}
			else {
				em.remove(player);
				em.flush();
				return player;
			}
		}
		catch(Exception e) {
			throw new Exception();
		}
		finally {
			em.close();
		}
	}
	
	public Teams deleteTeam(int team_id) throws Exception{
		try {
			Teams team = em.find(Teams.class,team_id);
			if(team == null) {
				return team;
			}
			else {
				em.remove(team);
				em.flush();
				return team;
			}
		}
		catch(Exception e) {
			throw new Exception();
		}
		finally {
			em.close();
		}
	}

}
