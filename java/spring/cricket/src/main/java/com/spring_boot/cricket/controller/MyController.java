package com.spring_boot.cricket.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.spring_boot.cricket.service.MyService;
import com.spring_boot.cricket.util.Players;
import com.spring_boot.cricket.util.Teams;

@RestController
@RequestMapping("/api")
public class MyController {
	
	@Autowired
	MyService ms;
	
	@GetMapping("/getPlayers")
	public ResponseEntity<String> getPlayers(){
		return ms.getPlayers();
	}
	
	@GetMapping("/getTeams")
	public ResponseEntity<String> getTeams(){
		return ms.getTeams();
	}
	
	@PutMapping("/updateTeam/{id}")
	public ResponseEntity<Object> updateTeamById(@RequestBody Teams team,@PathVariable("id") int team_id){
		team.setTeam_id(team_id);
		return ms.updateTeamById(team);
	}
	
	@PutMapping("/updatePlayer/{id}")
	public ResponseEntity<Object> updatePlayerById(@RequestBody Map<String, Object> p,@PathVariable("id") int player_id){
		Players player = new Players();
		player.setAge((int) p.get("age"));
		player.setName((String) p.get("name"));
		player.setEmail((String) p.get("email"));
		player.setPlayer_id(player_id);
		System.out.println(player);
		return ms.updatePlayerById(player, (int) p.get("team_id"));
	}
	
	@PostMapping("/addTeam")
	public ResponseEntity<Object> newTeam(@RequestBody Teams team){
		return ms.newTeam(team);
	}
	
	@PostMapping("/addPlayer")
	public ResponseEntity<Object> newPlayer(@RequestBody Map<String, Object> p){
		Players player = new Players();
		player.setAge((int) p.get("age"));
		player.setName((String) p.get("name"));
		player.setEmail((String) p.get("email"));
		return ms.newPlayer(player, (int) p.get("team_id"));
	}
	
	@DeleteMapping("/deleteTeam/{t_id}")
	public ResponseEntity<String> deleteTeam(@PathVariable("t_id") int t_id){
		return ms.deleteTeam(t_id);
	}
	
	@DeleteMapping("/deletePlayer/{p_id}")
	public ResponseEntity<String> deletePlayer(@PathVariable("p_id") int p_id){
		return ms.deletePlayer(p_id);
	}

}
