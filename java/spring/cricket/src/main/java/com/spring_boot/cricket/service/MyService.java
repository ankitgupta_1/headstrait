package com.spring_boot.cricket.service;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.spring_boot.cricket.repository.MyRepository;
import com.spring_boot.cricket.util.Players;
import com.spring_boot.cricket.util.ResponseCreator;
import com.spring_boot.cricket.util.Teams;

@Service
public class MyService {
	
	@Autowired
	MyRepository rs;

	public ResponseEntity<String> getTeams() {
		try {
			List<Object> s = rs.getTeams();
			if(s.size() == 0) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ResponseCreator.response("Data retrieved for all the teams", "No teams present in database", 400));
			}
			else {
				return ResponseEntity.ok(ResponseCreator.response("Data retrieved for all the teams", s, 200));
			}
		}
		catch(Exception e) {
			System.out.println("Exception from getTeams() : " +e);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ResponseCreator.response("server access error", null, 400));
		}
	}
	
	public ResponseEntity<String> getPlayers() {
		try {
			List<Object> s = rs.getPlayers();
			if(s.size() == 0) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ResponseCreator.response("Data retrieved for all the players", "No players present in database", 400));
			}
			else {
				return ResponseEntity.ok(ResponseCreator.response("Data retrieved for all the players", s, 200));
			}
		}
		catch(Exception e) {
			System.out.println("Exception from getPlayers() : " +e);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ResponseCreator.response("server access error", null, 400));
		}
	}
	
	public ResponseEntity<Object> updateTeamById(Teams team) {		
		try {
			HashMap m = Validation.validate(team);
			if(m.isEmpty()) {
				Teams t = rs.updateTeamById(team);
				if(t == null) {
					return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ResponseCreator.response("Could not found team for given id", null, 400));
				}
				else {
					return ResponseEntity.ok(ResponseCreator.response("Updated team for given id", t, 200));
				}
			}
			else {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ResponseCreator.response(m, null, 400));
			}
			
		}
		catch(Exception e) {
			System.out.println("Exception from updateTeamById() : " +e);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ResponseCreator.response("server access error", null, 400));
		}
	}
	
	public ResponseEntity<Object> updatePlayerById(Players player, int id) {		
		try {
			HashMap m = Validation.validate(player);
			if(m.isEmpty()) {
				Players p = rs.updatePlayerById(player, id);
				if(p == null) {
					return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ResponseCreator.response("Could not found player for given id", null, 400));
				}
				else {
					return ResponseEntity.ok(ResponseCreator.response("Updated player for given id", p, 200));
				}
			}
			else {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ResponseCreator.response(m, null, 400));
			}
		}
		catch(Exception e) {
			System.out.println("Exception from updatePlayerById() : " +e);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ResponseCreator.response("server access error", null, 400));
		}
	}
	
	public ResponseEntity<Object> newTeam(Teams team) {
		try {
			HashMap m = Validation.validate(team);
			if(m.isEmpty()) {
				Teams t = rs.newTeam(team);
				if(t == null) {
					return ResponseEntity.ok(ResponseCreator.response("Added new team", t, 200));
				}
				else {
					return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ResponseCreator.response("Could not add new team", null, 400));
				}
			}
			else {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ResponseCreator.response(m, null, 400));
			}
		}
		catch(Exception e) {
			System.out.println("Exception from newTeam() : " +e);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ResponseCreator.response("server access error", null, 400));
		}
	}
	
	public ResponseEntity<Object> newPlayer(Players player, int team_id) {
		try {
			HashMap m = Validation.validate(player);
			if(m.isEmpty()) {
				Players p = rs.newPlayer(player, team_id);
				if(p == null) {
					return ResponseEntity.ok(ResponseCreator.response("Added new player", p, 200));
				}
				else {
					return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ResponseCreator.response("Could not add new player", null, 400));
				}
			}
			else {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ResponseCreator.response(m, null, 400));
			}			
		}
		catch(Exception e) {
			System.out.println("Exception from newPlayer() : " +e);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ResponseCreator.response("server access error", null, 400));
		}
	}
	
	public ResponseEntity<String> deleteTeam(int team_id) {
		try {
			Teams t = rs.deleteTeam(team_id);
			if(t == null) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ResponseCreator.response("Could not find team for given id", null, 400));
			}
			else {
				return ResponseEntity.ok(ResponseCreator.response("Deleted team successfully", t, 200));
			}
		}
		catch(Exception e) {
			System.out.println("Exception from deleteTeam() : " +e);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ResponseCreator.response("server access error", null, 400));
		}
	}
	
	public ResponseEntity<String> deletePlayer(int player_id) {
		try {
			Players p = rs.deletePlayer(player_id);
			if(p == null) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ResponseCreator.response("Could not find player for given id", null, 400));
			}
			else {
				return ResponseEntity.ok(ResponseCreator.response("Deleted player successfully", p, 200));
			}
		}
		catch(Exception e) {
			System.out.println("Exception from deletePlayer() : " +e);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ResponseCreator.response("server access error", null, 400));
		}
	}

}
