package com.spring_boot.cricket.service;

import java.util.HashMap;

import com.spring_boot.cricket.util.Players;
import com.spring_boot.cricket.util.Teams;

public class Validation {
	public static HashMap validate(Players player){
		HashMap<String, String> error = new HashMap<>();
		
		if(player.getName()=="") {
			error.put("name", "Name must be provided");
		}
		if(player.getName().length() < 3 || player.getName().length() > 20) {
			error.put("name", "Name should be between 3 to 20 characters");
		}
		
//		if(player.getEmail().matches("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z]{2,})$")) {
//			error.put("email", "Enter a valid email");
//		}
		
		if(player.getEmail()=="") {
			error.put("email", "Enter a valid email");
		}
		
//		if(player.getAge() == (int) player.getAge()) {
//					error.put("age", "Age must be a number");
//				}
//		
		if(player.getAge() < 10 && player.getAge() > 60) {
			error.put("age", "Age must be between 10 to 60");
		}
		
		return error;
	}
	
	public static HashMap validate(Teams team){
		HashMap<String, String> error = new HashMap<>();
		
		if(team.getName().length() < 3 || team.getName().length() > 40) {
			error.put("name", "Name should be between 3 to 40 characters");
		}
		
		if(team.getCountry().length() < 3 || team.getCountry().length() > 40) {
			error.put("country", "Country should be between 3 to 40 characters");
		}
		
		if(team.getWon() == (int) team.getWon()) {
					error.put("won", "Won must be a number");
				}
		
		if(Integer.toString(team.getWon()).length() > 0 && Integer.toString(team.getWon()).length() < 4) {
			error.put("won", "Enter correct value");
		}
		
		if(team.getDraw() == (int) team.getDraw()) {
			error.put("draw", "Draw must be a number");
		}

		if(Integer.toString(team.getDraw()).length() > 0 && Integer.toString(team.getDraw()).length() < 4) {
			error.put("draw", "Enter correct value");
		}
		
		if(team.getLost() == (int) team.getLost()) {
			error.put("lost", "Lost must be a number");
		}

		if(Integer.toString(team.getLost()).length() > 0 && Integer.toString(team.getLost()).length() < 4) {
			error.put("lost", "Enter correct value");
		}
		
		return error;
	}

}
