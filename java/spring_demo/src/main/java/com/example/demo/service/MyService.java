package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.example.demo.repository.MyRepository;
import com.example.demo.util.Contact;

@Service
public class MyService {
	
	@Autowired
	MyRepository repo;
	
	public ResponseEntity<String> getAll() {
		return repo.getAll();
	}
	
	public ResponseEntity<String> getById(int id) {
		return repo.getById(id);
	}
	
	public ResponseEntity<String> addNew(Contact contact) {
		return repo.addNew(contact);
	}
	
	public ResponseEntity<String> update(Contact contact) {
		return repo.update(contact);
	}
	
	public ResponseEntity<String> delete(int id) {
		return repo.delete(id);
	}

}
