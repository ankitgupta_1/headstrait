package com.example.demo.util;

import java.util.List;

public class ContactObject {
	private String message;	
	private Contact data;
	private int status;
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Contact getData() {
		return data;
	}
	public void setData(Contact data) {
		this.data = data;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
}
