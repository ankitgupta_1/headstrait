package com.example.demo.util;

import java.util.List;

public class ResponseCreator {
	public static String response(String msg, List<Object> data, int status) {
		MyObject o = new MyObject();
		o.setMessage(msg);
		o.setData(data);
		o.setStatus(status);
		System.out.println("obj: "+ObjToJSON.Obj2JSON(o));
		return ObjToJSON.Obj2JSON(o);
	}
}
