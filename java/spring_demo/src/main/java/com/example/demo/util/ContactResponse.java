package com.example.demo.util;

import java.util.List;

public class ContactResponse {
	public static String response(String msg, Contact data, int status) {
		ContactObject c = new ContactObject();
		c.setMessage(msg);
		c.setData(data);
		c.setStatus(status);
		System.out.println("obj: "+ObjToJSON.Obj2JSON(c));
		return ObjToJSON.Obj2JSON(c);
	}
}
