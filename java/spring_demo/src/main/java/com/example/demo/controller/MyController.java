package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.service.MyService;
import com.example.demo.util.Contact;

@RestController
@RequestMapping("/controller")
public class MyController {
	@Autowired
	MyService ms;
	
	@GetMapping("/url")
	public ResponseEntity<String> getRequest() {
		return ms.getAll();
	}
	
	@GetMapping("/url/{id}")
	public ResponseEntity<String> getById(@PathVariable("id") int id) {
		return ms.getById(id);
		
	}
	
	@PostMapping("/addNew")
	public ResponseEntity<String> addNew(@RequestBody Contact contact) {
		return ms.addNew(contact);
		
	}
	
	@PutMapping("/update/{id}")
	public ResponseEntity<String> update(@RequestBody Contact contact, @PathVariable("id") int id) {
		contact.setId(id);
		return ms.update(contact);
		
	}
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<String> delete(@PathVariable("id") int id) {
		return ms.delete(id);
		
	}
}
