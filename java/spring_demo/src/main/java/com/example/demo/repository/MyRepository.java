package com.example.demo.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.Criteria;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.util.Contact;
import com.example.demo.util.ContactResponse;
import com.example.demo.util.ResponseCreator;

@Repository
@Transactional
public class MyRepository {
	
	@PersistenceContext
	private EntityManager em;
	
	public ResponseEntity<String> getAll() {
		try {
			Query query = em.createNativeQuery("select * from contact");
			((org.hibernate.query.Query<Object>) query).setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
			List<Object> s = ((org.hibernate.query.Query<Object>) query).list();
			return ResponseEntity.ok(ResponseCreator.response("data retrieved successfully", s, 200));
		}
		catch(Exception e) {
			System.out.println("Exception : "+e);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ResponseCreator.response("server access error", null, 404));
		}
		finally {
			em.close();
		}
	}
	
	public ResponseEntity<String> getById(int id) {
		try {
			Query query = em.createNativeQuery("select * from contacts_info where id="+id);
			((org.hibernate.query.Query<Object>) query).setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
			List<Object> s = ((org.hibernate.query.Query<Object>) query).list();
			
			Contact c = em.find(Contact.class, id);
			System.out.println(c);
			if(c == null) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ContactResponse.response("contact not found for given id", null, 404));
//				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ResponseCreator.response("contact not found for given id", null, 404));
			}
			else {
//				return ResponseEntity.ok(ResponseCreator.response("contact for given id retrieved successfully", s, 200));
				return ResponseEntity.ok(ContactResponse.response("contact for given id retrieved successfully", c, 200));

			}
		}
		catch(Exception e) {
			System.out.println("Exception : "+e);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ContactResponse.response("server access error", null, 404));
//			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ResponseCreator.response("server access error", null, 404));
		}
		finally {
			em.close();
		}
	}
	
	public ResponseEntity<String> addNew(Contact contact) {
		try {
			Query query = em.createNativeQuery("select * from contacts_info where email='" + contact.getEmail()+"'");
			((org.hibernate.query.Query<Object>) query).setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
			List<Object> s = ((org.hibernate.query.Query<Object>) query).list();
			Contact c = em.find(Contact.class, contact.getId());
			if(c==null) {
//				em.getTransaction().begin();
//			int	queryInsert = em.createNativeQuery("insert into contacts_info(name, email, phone) values(:name, :email, :phone)")
//						.setParameter("name", contact.getName())
//						.setParameter("email", contact.getEmail())
//						.setParameter("phone", contact.getPhone()).executeUpdate();
//			em.getTransaction().commit();
				em.persist(contact);
				return ResponseEntity.ok(ContactResponse.response("contact created successfully", c, 200));
//				return ResponseEntity.ok(ResponseCreator.response("contact created successfully", s, 200));
			}
			else {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ContactResponse.response("server access error", null, 404));
//				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ResponseCreator.response("server access error", null, 404));
			}
		}
		catch(Exception e) {
//			em.getTransaction().rollback();
			System.out.println("Exception : "+e);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ContactResponse.response("server access error", null, 404));
//			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ResponseCreator.response("server access error", null, 404));
		}
		finally {
			em.close();
		}
	}
	
	public ResponseEntity<String> update(Contact contact) {
		try {
			Query query = em.createNativeQuery("select * from contacts_info where id=" + contact.getId());
			((org.hibernate.query.Query<Object>) query).setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
			List<Object> s = ((org.hibernate.query.Query<Object>) query).list();
			Contact c = em.find(Contact.class, contact.getId());
			if(c == null) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ContactResponse.response("contact not found for given id", null, 404));
//				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ResponseCreator.response("contact not found for given id", null, 404));
			}
			else {
//				em.getTransaction().begin();
//				int	queryInsert = em.createNativeQuery("update contacts_info set name=:name, email=:email, phone=:phone where id=:id")
//						.setParameter("id", id)
//						.setParameter("name", contact.getName())
//						.setParameter("email", contact.getEmail())
//						.setParameter("phone", contact.getPhone()).executeUpdate();
				em.merge(contact);
				em.flush();
//				em.getTransaction().commit();
//				return ResponseEntity.ok(ResponseCreator.response("contact for given id updated successfully", s, 200));
				return ResponseEntity.ok(ContactResponse.response("contact for given id updated successfully", c, 200));
			}
		}
		catch(Exception e) {
//			em.getTransaction().rollback();
			System.out.println("Exception : "+e);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ContactResponse.response("server access error", null, 404));
//			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ResponseCreator.response("server access error", null, 404));
		}
		finally {
			em.close();
		}
	}
	
	public ResponseEntity<String> delete(int id) {
		try {
			Query query = em.createNativeQuery("select * from contacts_info where id=" + id);
			((org.hibernate.query.Query<Object>) query).setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
			List<Object> s = ((org.hibernate.query.Query<Object>) query).list();
			Contact c = em.find(Contact.class, id);
			if(c == null) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ContactResponse.response("contact not found for given id", null, 404));
//				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ResponseCreator.response("contact not found for given id", null, 404));
			}
			else {
//				em.getTransaction().begin();
				em.remove(c);
//				int	queryInsert = em.createNativeQuery("delete from contacts_info where id="+id).executeUpdate();
//				em.getTransaction().commit();
				return ResponseEntity.ok(ResponseCreator.response("contact for given id deleted successfully", s, 200));
			}
		}
		catch(Exception e) {
//			em.getTransaction().rollback();
			System.out.println("Exception : "+e);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ContactResponse.response("server access error", null, 404));
//			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ResponseCreator.response("server access error", null, 404));
		}
		finally {
			em.close();
		}
	}
	
}
