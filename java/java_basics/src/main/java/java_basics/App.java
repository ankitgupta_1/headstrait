package java_basics;

import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import json.ObjToJSON;
import hibernate.Laptop;
import hibernate.Student;

/**
 * Hello world!
 *
 */
public class App {
    public static void main(String[] args) {
        // Scanner sc = new Scanner(System.in);
        // System.out.println("Hello World!");
        // sample obj = new sample();
        // obj.sampleMethod();

        // int x = 5;
        // passByValue val = new passByValue();
        // val.change(x);
        // System.out.println("App.java pass by value x = " + x);

        // passByReference ref_val = new passByReference(5);
        // ref_val.x = 100;
        // System.out.println("App.java pass by reference x = " + ref_val.x);

        // completableFuture comp = new completableFuture();
        // comp.mockMain();

        // jdbc db = new jdbc();
        // db.query();

        // jdbcPostgres conn = new jdbcPostgres();
        // conn.dbConn();

        // System.out.print("Enter database name : ");
        // conn.createDatabase(sc.next());

        // System.out.print("Enter table name : ");
        // conn.createTable(sc.next());

        // System.out.println("\nInsert data into table as name, strength, gender : ");
        // conn.insert(sc.next(), sc.next(), sc.next());
        // conn.getData();

        // System.out.println("\nInsert data into table as name, strength, gender : ");
        // conn.insert(sc.next(), sc.next(), sc.next());
        // conn.getData();

        // System.out.println("\nUpdate data into table as name, strength, gender and id
        // to update : ");
        // conn.updateTable(sc.next(), sc.next(), sc.next(), sc.nextInt());
        // conn.getData();

        // System.out.println("\nEnter id to delete data from table : ");
        // conn.delete(sc.nextInt());

        // conn.getData();

        // conn.closeConnection();

        Student stud = new Student();
        stud.setSname("gupta");
        stud.setCollname("APSIT");

        Laptop lap = new Laptop();
        // lap.setLid(11);
        lap.setLname("Lenovo");
        lap.setStud(stud);
        // stud.getLap().add(lap);

        Laptop lap1 = new Laptop();
        // lap1.setLid(12);
        lap1.setLname("Dell");
        lap1.setStud(stud);
        // stud.getLap().add(lap1);

        // Student stud1 = new Student();
        // stud1.setSname("gupta");
        // stud1.setCollname("APSIT");

        // Laptop lap2 = new Laptop();
        // lap2.setLid(13);
        // lap2.setLname("Dell");
        // // lap.setStud(stud);
        // stud1.getLap().add(lap2);

        // Laptop lap3 = new Laptop();
        // lap3.setLid(14);
        // lap3.setLname("Lenovo");
        // // lap1.setStud(stud);
        // stud1.getLap().add(lap3);

        Configuration config = new Configuration().configure().addAnnotatedClass(Student.class)
                .addAnnotatedClass(Laptop.class);
        SessionFactory sf = config.buildSessionFactory();
        Session session = sf.openSession();
        Transaction tx = session.beginTransaction();
        List<Object[]> obj = null;

        try {
            session.save(stud);
            session.save(lap);
            session.save(lap1);

            // session.save(stud1);
            // session.save(lap1);
            // session.save(lap2);
            Query query = session.createQuery("from Student");
            obj = query.getResultList();

            System.out.println("JSON response: " + ObjToJSON.Obj2JSON(obj));

            tx.commit();
        } catch (Exception e) {
            tx.rollback();
            e.printStackTrace();
        }
    }
}
