package jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class jdbc {
    public static void query() {
        Connection c = null;
        PreparedStatement pst = null;

        try {
        	Class.forName("org.postgresql.Driver");
            c = DriverManager
            .getConnection("jdbc:postgresql://localhost:5432/contacts", "postgres", "");
            System.out.println("Database Connected Successfully");
            pst = c.prepareStatement("select * from contacts_info where id=? and name=?");
            pst.setInt(1, 1);
            pst.setString(2, "ankit");
            ResultSet rs = pst.executeQuery();
            System.out.println("ID\tName\tEmail\tPhone");
            while(rs.next()){
                System.out.println(""+rs.getInt("id")+"\t"+rs.getString("name")+"\t"+rs.getString("email")+"\t"+rs.getBigDecimal("phone"));
            }
            rs.close();
            pst.close();
            c.close();

        }
        catch(Exception e){
            System.out.println(e.getClass().getName() + " : "+e.getMessage());
            System.exit(0);
        }
        finally{
            System.out.println("Connection closed");
        }
    }
}