package hibernate;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "sid_col")
    private int sid;

    private String sname;
    private String collname;

    @OneToMany(mappedBy = "stud")
    private List<Laptop> lap = new ArrayList<Laptop>();

    public int getSid() {
        return sid;
    }

    public String getCollname() {
        return collname;
    }

    public void setCollname(String collname) {
        this.collname = collname;
    }

    public void setSid(int sid) {
        this.sid = sid;
    }

    // public List<Laptop> getLap() {
    // return lap;
    // }

    // public void setLap(List<Laptop> lap) {
    // this.lap = lap;
    // }

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

}