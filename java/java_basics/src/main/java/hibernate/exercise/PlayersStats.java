package hibernate.exercise;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "players_stats")
public class PlayersStats {

    // @Id
    // @GeneratedValue(strategy = GenerationType.IDENTITY)
    // @Transient
    // private int ps_id;

    @Id
    private int player_id;

    @OneToOne
    @JoinColumn(name = "player_id")
    @MapsId
    private Players p_id;

    // @OneToOne(mappedBy = "team")
    // @Column(name = "team_id")
    // private Player t_id;

    private int total_runs;
    private int total_wickets;
    private int total_matches;

    public Players getP_id() {
        return p_id;
    }

    public void setP_id(Players p_id) {
        this.p_id = p_id;
    }

    public int getTotal_runs() {
        return total_runs;
    }

    public void setTotal_runs(int total_runs) {
        this.total_runs = total_runs;
    }

    public int getTotal_wickets() {
        return total_wickets;
    }

    public void setTotal_wickets(int total_wickets) {
        this.total_wickets = total_wickets;
    }

    public int getTotal_matches() {
        return total_matches;
    }

    public void setTotal_matches(int total_matches) {
        this.total_matches = total_matches;
    }

}