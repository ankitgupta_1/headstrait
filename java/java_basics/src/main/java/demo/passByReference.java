package demo;

public class passByReference {
    public int x;

    public passByReference(int i) {
        x = i;
    }

    public passByReference() {
        x = 0;
    }

}