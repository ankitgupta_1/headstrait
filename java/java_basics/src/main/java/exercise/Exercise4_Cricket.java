package exercise;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.persistence.Query;
import org.hibernate.Transaction;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import hibernate.exercise.Players;
import hibernate.exercise.PlayersStats;
import hibernate.exercise.Teams;
import json.ObjToJSON;

public class Exercise4_Cricket {

    static Configuration config = new Configuration().configure().addAnnotatedClass(Teams.class)
            .addAnnotatedClass(Players.class).addAnnotatedClass(PlayersStats.class);
    static SessionFactory sf = config.buildSessionFactory();
    static Session session = sf.openSession();
    static Transaction tx = session.beginTransaction();
    static List<Object[]> obj = null;
    static Teams team = new Teams();
    static List<Players> player = new ArrayList<Players>();
    static List<PlayersStats> playerstats = new ArrayList<PlayersStats>();

    public static void main(String args[]) {
        Exercise4_Cricket ap = new Exercise4_Cricket();
        ap.mockMain();

    }

    public void mockMain() {
        System.out.println(Runtime.getRuntime().availableProcessors());
        ExecutorService exec = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
        long st = System.currentTimeMillis();

        // for (int i = 0; i < 10; i++) {
        CompletableFuture.supplyAsync(() -> addTeam(), exec).thenApply(op -> addPlayer(op))
                .thenApply(op -> addPlayerStats(op)).thenAccept(op -> {
                    try {
                        viewPlayers();
                        viewTeams();
                        viewPlayersStats();
                        viewPlayersByTeam();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });
        // System.out.println("Loop : " + i);
        // }

        long et = System.currentTimeMillis();
        exec.shutdown();
        System.out.println("Start time: " + st + "\nEnd time: " + et + "\nDifference: " + (et - st));

        try {

            // viewPlayers();
            // viewTeams();
            // viewPlayersStats();
            // viewPlayersByTeam();

            // session.flush();
            tx.commit();
            // session.clear();

            // tx = session.beginTransaction();
            // updatePlayersById();
            // updateTeamsById();

            // session.flush();
            // tx.commit();
            // session.clear();
        } catch (Exception e) {
            tx.rollback();
            e.printStackTrace();
        }

    }

    // Add team to database
    public static Teams addTeam() {
        try {
            session.flush();
            session.clear();
            team.setT_name("India");
            team.setWon(128);
            team.setLost(30);
            team.setDraw(10);
            team.setHome("India");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return team;
    }

    // Add player to database
    public static List<Players> addPlayer(Teams op) {
        try {
            System.out.println("\n" + op.getDraw() + "\n");
            session.save(op);
            Thread.sleep(8000);
            Players p1 = new Players();
            p1.setP_name("MS Dhoni");
            p1.setGender("male");
            p1.setHometown("India");
            p1.setTeam(team);

            Players p2 = new Players();
            p2.setP_name("Virat Kohli");
            p2.setGender("male");
            p2.setHometown("India");
            p2.setTeam(team);

            player.add(p1);
            player.add(p2);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return player;
    }

    public static String addPlayerStats(List<Players> op) {
        try {
            System.out.println();
            System.out.println(op);
            System.out.println(op.get(0));

            System.out.println();
            // session.save(op.get(0));
            // session.save(op.get(1));

            // PlayersStats ps1 = new PlayersStats();
            // ps1.setP_id(op.get(0));
            // ps1.setTotal_runs(17896);
            // ps1.setTotal_wickets(102);
            // ps1.setTotal_matches(616);

            // PlayersStats ps2 = new PlayersStats();
            // ps2.setP_id(op.get(0));
            // ps2.setTotal_runs(19657);
            // ps2.setTotal_wickets(216);
            // ps2.setTotal_matches(587);
            // session.save(ps1);
            // session.save(ps2);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void viewPlayers() {
        try {
            Thread.sleep(8000);
            Query query = session.createNativeQuery("select * from players");
            obj = query.getResultList();

            System.out.println("\nPlayers Table : " + ObjToJSON.Obj2JSON(obj));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void viewPlayersByTeam() {
        try {
            Query query = session.createNativeQuery(
                    "select p.player_name, p.gender, p.hometown, t.team_name from players as p, teams as t where p.team_team_id = t.team_id order by p.team_team_id");
            obj = query.getResultList();

            System.out.println("\nPlayers Table By Team : " + ObjToJSON.Obj2JSON(obj));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void viewTeams() {
        try {
            Query query = session.createNativeQuery("select * from teams");
            obj = query.getResultList();

            System.out.println("\nTeams Table : " + ObjToJSON.Obj2JSON(obj));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void viewPlayersStats() {
        try {

            Query query = session.createNativeQuery("select * from players_stats");
            obj = query.getResultList();

            System.out.println("\nPlayersStats Table : " + ObjToJSON.Obj2JSON(obj));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void updatePlayersById() {
        try {
            Players updateKohli = new Players();
            updateKohli.setP_id(4);
            updateKohli.setP_name("V Kohli");
            updateKohli.setGender("male");

            session.saveOrUpdate(updateKohli);

            viewPlayers();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void updateTeamsById() {
        try {
            Teams updateTeam = new Teams();
            updateTeam.setT_id(1);
            updateTeam.setWon(180);
            updateTeam.setLost(34);

            session.saveOrUpdate(updateTeam);

            viewTeams();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}