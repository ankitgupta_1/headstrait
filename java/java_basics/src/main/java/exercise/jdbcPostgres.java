package exercise;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Savepoint;
import java.sql.Statement;

import logger.LoggerFunc;

interface postgresInterface {

    public void dbConn();

    public void getData();

    public void insert(String name, String strengths, String gender);

    public void createDatabase(String dbName);

    public void createTable(String tableName);

    public void updateTable(String name, String strengths, String gender, int id);

    public void delete(int id);

    public void closeConnection();
}

public class jdbcPostgres implements postgresInterface {

    Connection c = null;
    ResultSet rs = null;
    PreparedStatement ps = null;
    String dbName, tableName;
    Savepoint save1 = null;

    public void dbConn() {
        try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/", "postgres", "");
            // c.setAutoCommit(false);
            LoggerFunc.consoleLogMsg("Postgres server Connected Successfully");
        } catch (Exception e) {
            LoggerFunc.errorLog(e.getClass().getName() + " : " + e.getMessage());
            System.exit(0);
        }
    }

    public void createDatabase(String dbName) {
        this.dbName = dbName;
        try {

            LoggerFunc.consoleLogMsg("\nCreating Database " + dbName);
            ps = c.prepareStatement("DROP DATABASE IF EXISTS " + dbName);
            ps.executeUpdate();
            ps = c.prepareStatement("CREATE DATABASE " + dbName);
            ps.executeUpdate();
            LoggerFunc.consoleLogMsg("Database " + dbName + " created successfully");

            c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/" + dbName, "postgres", "");
            c.setAutoCommit(false);

        } catch (Exception e) {
            LoggerFunc.errorLog(e.getClass().getName() + " : " + e.getMessage());
            System.exit(0);
        }
    }

    public void createTable(String tableName) {
        this.tableName = tableName;
        try {
            save1 = c.setSavepoint();
            LoggerFunc.consoleLogMsg("\nCreating Table " + tableName);
            ps = c.prepareStatement("CREATE TABLE " + tableName
                    + " (id serial primary key, name varchar(88), strengths varchar(88), gender varchar(10))");
            ps.execute();
            // LoggerFunc.consoleLogMsg(ps);

            LoggerFunc.consoleLogMsg("Table " + tableName + " created successfully");
            c.commit();
        } catch (Exception e) {
            try {
                c.rollback(save1);
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            // LoggerFunc.errorLog(e.getClass().getName() + " : " + e.getMessage());
            LoggerFunc.errorLog(e.getClass().getName() + " : " + e.getMessage());
            System.exit(0);
        }
    }

    public void insert(String name, String strengths, String gender) {
        try {
            save1 = c.setSavepoint();
            LoggerFunc.consoleLogMsg("\nInserting values in table " + this.tableName);
            ps = c.prepareStatement("insert into " + this.tableName + "(name, strengths, gender) values('" + name
                    + "','" + strengths + "','" + gender + "')");
            ps.execute();
            c.commit();
            // LoggerFunc.consoleLogMsg(ps);
        } catch (Exception e) {
            try {
                c.rollback(save1);
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            LoggerFunc.errorLog(e.getClass().getName() + " : " + e.getMessage());
            System.exit(0);
        }
    }

    public void getData() {
        try {
            LoggerFunc.consoleLogMsg("\nFetching values from table");
            ps = c.prepareStatement("select * from " + this.tableName);
            rs = ps.executeQuery();
            // LoggerFunc.consoleLogMsg(ps);
            System.out.println("\tID\tName\tStrength\tGender");
            while (rs.next()) {
                System.out.println("\t" + rs.getInt("id") + "\t" + rs.getString("name") + "\t"
                        + rs.getString("strengths") + "\t" + rs.getString("gender"));
            }
        } catch (Exception e) {
            LoggerFunc.errorLog(e.getClass().getName() + " : " + e.getMessage());
            System.exit(0);
        }
    }

    public void updateTable(String name, String strengths, String gender, int id) {
        try {
            save1 = c.setSavepoint();
            LoggerFunc.consoleLogMsg("\nUpdating values in table " + this.tableName);
            ps = c.prepareStatement("update " + this.tableName + " set name = '" + name + "', strengths = '" + strengths
                    + "', gender = '" + gender + "' where id = " + id);
            ps.execute();
            c.commit();
            // LoggerFunc.consoleLogMsg(ps);
        } catch (Exception e) {
            try {
                c.rollback(save1);
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            LoggerFunc.errorLog(e.getClass().getName() + " : " + e.getMessage());
            System.exit(0);
        }
    }

    public void delete(int id) {
        try {
            save1 = c.setSavepoint();
            LoggerFunc.consoleLogMsg("\nUpdating values in table " + this.tableName);
            ps = c.prepareStatement("delete from " + this.tableName + " where id = " + id);
            ps.execute();
            c.commit();
            // LoggerFunc.consoleLogMsg(ps);
        } catch (Exception e) {
            try {
                c.rollback(save1);
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            LoggerFunc.errorLog(e.getClass().getName() + " : " + e.getMessage());
            System.exit(0);
        }
    }

    public void closeConnection() {
        try {
            c.close();
            rs.close();
            ps.close();
        } catch (Exception e) {
            LoggerFunc.errorLog(e.getClass().getName() + " : " + e.getMessage());
        }
    }
}