package logger;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

public class LoggerFunc {
	private static Logger logger = LoggerFactory.getLogger(LoggerFunc.class);
	
	public static void consoleLogMsg(String msg) {
		logger.info(msg);
	}
	
	public static void errorLog(String err){
		logger.error(err);
	}
}