package multithreading.exercise4;

import java.util.concurrent.Executors;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ExecutionException;;

public class multithreading {

    int k = 0;

    public static void main(String args[]) {
        multithreading ap = new multithreading();
        ap.mockMain();

    }

    public void mockMain() {
        System.out.println(Runtime.getRuntime().availableProcessors());
        ExecutorService exec = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
        long st = System.currentTimeMillis();

        for (int i = 0; i < 10; i++) {
            CompletableFuture<String> cf1 = CompletableFuture.supplyAsync(() -> func1(k), exec);
            CompletableFuture<String> cf2 = CompletableFuture.supplyAsync(() -> func2("Ankit "), exec);
            CompletableFuture<String> cf3 = CompletableFuture.supplyAsync(() -> func3("Gupta "), exec);
            CompletableFuture.allOf(cf1, cf2, cf3).join();
            try {
                System.out.println("CF1 : " + cf1.get());
                System.out.println("CF2 : " + cf2.get());
                System.out.println("CF3 : " + cf3.get());
            } catch (Exception e) {
                e.printStackTrace();
            }
            System.out.println("Loop : " + i);
        }

        long et = System.currentTimeMillis();
        exec.shutdown();
        System.out.println("Start time: " + st + "\nEnd time: " + et + "\nDifference: " + (et - st));

    }

    public String func1(int i) {
        System.out.println("Task: " + i);
        try {
            Thread.sleep(8000);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ("Func1() reached");
    }

    public String func2(String str) {
        return str + ("Func2 reached");
    }

    public String func3(String str) {
        return str + ("Func3 reached");
    }

    // public String func4(String str) {
    // return str + ("Func4 reached");
    // }

    // public String func5(String str) throws InterruptedException {
    // System.out.println("Thread: " + Thread.currentThread().getName());
    // k++;
    // if (k % 2 == 0) {
    // Thread.sleep(4000);
    // }
    // return str + ("Func5 reached");
    // }

}