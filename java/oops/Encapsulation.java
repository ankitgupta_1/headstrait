import java.util.Scanner;

class getEncapsulation {
    private String item;
    private Boolean remove;

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public Boolean getRemove() {
        return remove;
    }

    public void setRemove(Boolean remove) {
        this.remove = remove;
    }
}

public class Encapsulation {
    public static void main(String args[]) {
        Scanner sc = new Scanner(System.in);
        getEncapsulation encap = new getEncapsulation();
        System.out.println("Enter an item");
        encap.setItem(sc.next());
        System.out.println("Item is " + encap.getItem());
        sc.close();
    }
}