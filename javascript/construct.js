var Questions = function (question, options, answer) {
        this.question = question;
        this.options = options;
        this.answer = answer;
}

var firstQuestion = new Questions("What is Ankit's age?", [20, 23, 21, 22], 2);
var secondQuestion = new Questions("Where does Ankit stay?", ["Kalyan", "Andheri", "Thane", "Bhayander"], 1);

questionsArray = [firstQuestion, secondQuestion];

askQuestion = questionsArray[Math.floor(Math.random() * 2)];

console.log("**********************************")
console.log(askQuestion.question);

for (i = 0; i < askQuestion.options.length; i++) {
        console.log(i + ". " + askQuestion.options[i]);
}

userAnswer = prompt("Enter your option");

if (userAnswer == askQuestion.answer) {
        console.log("Correct answer");
}
else {
        console.log("Wrong answer");
}
console.log("");

