let us = [],
	india = [];

// Function used to convert into Indian format
function indianFormat(number) {
	while (number.length) {
		india.unshift(",");
		for (let i = 0; i < 2; i++) {
			india.unshift(number.pop());
		}
	}
	console.log(india);
	return india;
}

// Function used to convert into Indian format
function usFormat(number) {
	while (number.length) {
		us.unshift(",");
		for (let i = 0; i < 3; i++) {
			us.unshift(number.pop());
		}
	}
	console.log(us);
	return us;
}

length = 20;

for (i = 0; i < 10; i++) {
	// Defines two array to store digits
	let indCurrency = [],
		usdCurrency = [];

	// Iteratively adds random digits to the arrays of length from 10 to 1
	for (let i = length; i >= 1; i--) {
		amountDigit = Math.floor(Math.random() * 10);

		// To skip zero (0) at the start of the amount otherwise adds the random digits to the array
		if (i == 1 && amountDigit == 0) {
			indCurrency.unshift(1);
			usdCurrency.unshift(1);
		} else {
			indCurrency.unshift(amountDigit);
			usdCurrency.unshift(amountDigit);
		}
	}

	currency = indCurrency;
	currency = currency.join("");
	length--;

	// Reset the output variable to empty
	(us = []), (india = []);

	// Adds last three digit to respective currency variable which is common for both format
	for (let i = 0; i < 3; i++) {
		india.unshift(indCurrency.pop());
		us.unshift(usdCurrency.pop());
	}

	// Runs if loop on if there are more than 3 digits
	if (indCurrency != 0) {
		india.concat(indianFormat(indCurrency));
		us.concat(usFormat(usdCurrency));
	}

	// Printing the output on console
	console.log(
		currency +
			" converted to INR currency is Rs. " +
			india.join("") +
			" and into USD currency is USD. " +
			us.join("")
	);
}

// Adding a new comment line
