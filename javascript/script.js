// =========================
//       Prime Number
// =========================

// startNum = 2;
// endNum = 100;

// for (startNum; startNum < endNum; startNum++) {
//     for (j = 2; j < startNum; j++) {
//         if (startNum % j == 0) {
//             break;
//         }
//     }
//     if (startNum == j) {
//         console.log(startNum);
//     }
// }




// =========================
//     Fibonacci Series
// =========================

start = 0;
max = 100;

fibo_series = [start, start + 1];

while (fibo_series[fibo_series.length - 1] < max) {
    fibo_series.push(
        fibo_series[fibo_series.length - 2]
        + fibo_series[fibo_series.length - 1]
    );
}

if (fibo_series[fibo_series.length - 1] > max) {
    fibo_series.pop();
}
console.log(fibo_series);