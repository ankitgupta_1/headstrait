//function which returns promise

function validation(username, password) {
    var promise = new Promise((resolve, reject) => {
        setTimeout(() => {
            // checks and executes the condition if the length of variable username and password 
            // is less than 5 and sends reject message
            if (username.length < 5 || password.length < 5) {
                reject(Error("\n Please follow the format to register"));
            }

            resolve(`Congratulations ${username} you have succesfully registered`);
        }, 1000);

    });
    return promise;
}

console.log("This line is before function call");
console.log();

// Function call for promises
validation("Ankit", "123456")
    .then(result => {
        console.log();
        console.log(result);
        return validation("Johnny", "47856aa");
    })
    .then(result => {
        console.log();
        console.log(result);
        return validation("Jolly", 478564);
    })
    .then(result => {
        console.log();
        console.log(result);
        return validation("Jane", "47856a");
    })
    .catch(error => {
        console.log();
        console.log(error);
    })

console.log("This line is after function call");