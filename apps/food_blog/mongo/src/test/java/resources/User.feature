Feature: User Service test

    Scenario: Calls add new user function with success
        When the client makes the call to /auth/register with new email id
        # Then it checks if email id entered by user already exists in database
        # Then it checks if password is encoded
        Then the client recieves status code 200
        And the client recieves a message "Registered user successfully"

    Scenario: Calls add new user function with failure
        When the client makes a call to auth/register with registered email id
        Then the client recieves status code 400
        And the client recieves a message "Email already exist"

    Scenario: Calls login with success
        When the client makes a call to auth/login with correct credentials
        Then the client recieves status code 200
        And the client recieves a message "Log in successful"

    Scenario: Calls login with failure
        When the client makes a call to auth/login with incorrect credentials
        Then the client recieves status code 400
        And the client recieves a message "server access error"

