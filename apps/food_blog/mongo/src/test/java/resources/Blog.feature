Feature: Blog Service test

    Scenario: Calls getBlogs function with success
        When the client requests to get blogs it makes a call to /blog/getBlogs
        Then the client recieves a status code 200
        And the client recieves a message "All blogs retrieved successfully"