package com.exercise.food_blog.repositories;

import java.util.List;

import com.exercise.food_blog.models.Blogs;
import com.exercise.food_blog.models.Users;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BlogRepository extends JpaRepository<Blogs, Integer> {
    Blogs findById(int blogId);

    List<Blogs> findByUser(Users user);
}