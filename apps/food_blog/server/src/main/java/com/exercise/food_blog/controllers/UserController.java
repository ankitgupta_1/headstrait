package com.exercise.food_blog.controllers;

import java.util.Map;

import com.exercise.food_blog.models.Users;
import com.exercise.food_blog.services.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class UserController {

    @Autowired
    UserService userService;

    // Registeration of users
    @PostMapping("/auth/register")
    public ResponseEntity<Object> register(@RequestBody Map<String, Object> userMap) {
        Users user = new Users((String) userMap.get("userName"), (String) userMap.get("userPassword"),
                (String) userMap.get("userEmail"));
        return userService.register(user);
    }

    @PostMapping("/auth/login")
    public ResponseEntity<Object> login(@RequestBody Map<String, Object> userMap) {
        return userService.login((String) userMap.get("userEmail"), (String) userMap.get("userPassword"));
    }

    @DeleteMapping("/auth/deleteUser/{id}")
    public ResponseEntity<String> deleteUser(@PathVariable("id") int id) {
        return userService.deleteUser(id);
    }

}