package com.exercise.food_blog.controllers;

import com.exercise.food_blog.services.TagService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/auth/tag")
public class TagController {

    @Autowired
    TagService tagService;

    @GetMapping("/getTags")
    public ResponseEntity<Object> getTags() {
        return tagService.getTags();
    }

}