package com.exercise.food_blog.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoggerFunc {
    private static Logger logger = LoggerFactory.getLogger(LoggerFunc.class);

    public static void consoleLogMsg(String msg) {
        logger.info(msg);
    }

    public static void errorLogMsg(Exception e) {
        logger.error(e.toString());
    }
}