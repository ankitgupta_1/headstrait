package com.exercise.food_blog.models;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
public class Blogs {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "blog_id")
    private int blogId;

    @Column(name = "blog_title")
    private String blogTitle;

    @Column(name = "blog_description", columnDefinition = "varchar(5000)")
    private String blogDescription;

    @Column(name = "likes", columnDefinition = "integer default 0")
    private int likes;

    @Column(name = "dislikes", columnDefinition = "integer default 0")
    private int dislikes;

    @Column(name = "posted_on")
    private Timestamp postedOn;

    @JsonManagedReference
    @OneToMany(mappedBy = "blog")
    private List<Comments> comments;

    @JsonManagedReference
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id", nullable = false)
    private Users user;

    @JsonManagedReference
    @ManyToMany(cascade = CascadeType.ALL)
    // @JoinTable(name = "blogs_tags", joinColumns = @JoinColumn(name = "blog_id",
    // nullable = false), inverseJoinColumns = @JoinColumn(name = "tag_id", nullable
    // = false))
    private List<Tags> tags = new ArrayList<>();

    public int getBlogId() {
        return blogId;
    }

    public void setBlogId(int blogId) {
        this.blogId = blogId;
    }

    public String getBlogTitle() {
        return blogTitle;
    }

    public void setBlogTitle(String blogTitle) {
        this.blogTitle = blogTitle;
    }

    public String getBlogDescription() {
        return blogDescription;
    }

    public void setBlogDescription(String blogDescription) {
        this.blogDescription = blogDescription;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public int getDislikes() {
        return dislikes;
    }

    public void setDislikes(int dislikes) {
        this.dislikes = dislikes;
    }

    public Timestamp getPostedOn() {
        return postedOn;
    }

    public void setPostedOn(Timestamp postedOn) {
        this.postedOn = postedOn;
    }

    public Users getUser() {
        return user;
    }

    public void setUser(Users user) {
        this.user = user;
    }

    public List<Tags> getTags() {
        return tags;
    }

    public void setTags(List<Tags> tags) {
        this.tags = tags;
    }

    public Blogs() {
    }

    public List<Comments> getComments() {
        return comments;
    }

    public void setComments(List<Comments> comments) {
        this.comments = comments;
    }

    public Blogs(String blogTitle, String blogDescription, Timestamp postedOn, Users user, List<Tags> tags) {
        this.blogTitle = blogTitle;
        this.blogDescription = blogDescription;
        this.postedOn = postedOn;
        this.user = user;
        this.tags = tags;
    }

}