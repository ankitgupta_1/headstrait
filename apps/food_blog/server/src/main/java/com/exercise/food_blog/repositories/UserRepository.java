package com.exercise.food_blog.repositories;

import java.util.Optional;

import com.exercise.food_blog.models.Users;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<Users, Integer> {

    Users findById(int userId);

    Optional<Users> findByUserEmail(String userEmail);

    Boolean existsByUserEmail(String userEmail);

}