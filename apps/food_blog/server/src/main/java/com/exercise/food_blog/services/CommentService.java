package com.exercise.food_blog.services;

import java.util.List;
import java.util.Map;

import com.exercise.food_blog.models.Blogs;
import com.exercise.food_blog.models.Comments;
import com.exercise.food_blog.models.Users;
import com.exercise.food_blog.repositories.BlogRepository;
import com.exercise.food_blog.repositories.CommentRepository;
import com.exercise.food_blog.repositories.UserRepository;
import com.exercise.food_blog.utils.ResponseCreator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class CommentService {
    @Autowired
    CommentRepository commentRepository;

    @Autowired
    BlogRepository blogRepository;

    @Autowired
    UserRepository userRepository;

    public ResponseEntity<Object> getCommentsByBlogId(int blog_id) {
        try {
            Blogs blog = blogRepository.findById(blog_id);
            List<Comments> comments = commentRepository.findByBlog(blog);
            if (comments != null) {
                return ResponseEntity
                        .ok(ResponseCreator.response("All comments are retrieved sucessfully", comments, 200));
            }
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(ResponseCreator.response("No comments are present", null, 400));
        } catch (Exception e) {
            System.out.println("Exeption in getCommentsByBlogId(): " + e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(ResponseCreator.response("server access error", null, 400));
        }
    }

    public ResponseEntity<String> addComment(Map<String, Object> c) {
        try {
            Users user = userRepository.findById((int) c.get("userId"));

            if (user != null) {

                Blogs blog = blogRepository.findById((int) c.get("blogId"));
                if (blog != null) {
                    Comments comment = new Comments((String) c.get("comment"), user, blog);
                    commentRepository.save(comment);

                    return ResponseEntity.ok(ResponseCreator.response("Comment added successfully", comment, 200));
                }
                return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                        .body(ResponseCreator.response("blog not found for given id", null, 400));
            }

            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(ResponseCreator.response("user not found for given id", null, 400));
        } catch (Exception e) {
            System.out.println("Exeption in addComment(): " + e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(ResponseCreator.response("server access error", null, 400));
        }
    }
}