package com.exercise.food_blog.repositories;

import com.exercise.food_blog.models.Tags;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TagRepository extends JpaRepository<Tags, Integer> {

    Tags findById(int id);

}