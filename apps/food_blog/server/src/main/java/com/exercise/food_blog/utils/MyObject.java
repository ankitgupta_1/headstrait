package com.exercise.food_blog.utils;

import java.util.HashMap;
import java.util.List;

public class MyObject {
	private String message;	
	private Object data;
	private List<Object> listData;
	private int status;
	private HashMap hashMessage;
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
	public List<Object> getListData() {
		return listData;
	}
	public void setListData(List<Object> listData) {
		this.listData = listData;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public HashMap getHashMessage() {
		return hashMessage;
	}
	public void setHashMessage(HashMap hashMessage) {
		this.hashMessage = hashMessage;
	}
}
