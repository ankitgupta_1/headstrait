package com.exercise.food_blog.services;

import java.util.List;

import com.exercise.food_blog.models.Tags;
import com.exercise.food_blog.repositories.TagRepository;
import com.exercise.food_blog.utils.ResponseCreator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class TagService {

    @Autowired
    TagRepository tagRepository;

    public ResponseEntity<Object> getTags() {
        try {
            List<Tags> t = tagRepository.findAll();
            if (t != null)
                return ResponseEntity.ok(ResponseCreator.response("All tags retrieved successfully", t, 200));
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(ResponseCreator.response("No tags were found", null, 400));
        } catch (Exception e) {
            System.out.println("Exception in getTags(): " + e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(ResponseCreator.response("server access error", null, 400));
        }
    }

}