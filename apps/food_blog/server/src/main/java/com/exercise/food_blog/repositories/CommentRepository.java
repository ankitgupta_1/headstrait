package com.exercise.food_blog.repositories;

import java.util.List;

import com.exercise.food_blog.models.Blogs;
import com.exercise.food_blog.models.Comments;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CommentRepository extends JpaRepository<Comments, Integer> {

    List<Comments> findByBlog(Blogs blog);

}