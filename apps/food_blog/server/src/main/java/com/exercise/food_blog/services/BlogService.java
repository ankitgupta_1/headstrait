package com.exercise.food_blog.services;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.exercise.food_blog.models.Blogs;
import com.exercise.food_blog.models.Users;
import com.exercise.food_blog.repositories.BlogRepository;
import com.exercise.food_blog.repositories.TagRepository;
import com.exercise.food_blog.repositories.UserRepository;
import com.exercise.food_blog.utils.ResponseCreator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class BlogService {

    @Autowired
    BlogRepository blogRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    TagRepository tagRepository;

    public ResponseEntity<Object> getBlogs() {
        try {
            List<Blogs> b = blogRepository.findAll();
            if (b != null) {
                return ResponseEntity.ok(ResponseCreator.response("All blogs retrieved successfully", b, 200));
            }
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(ResponseCreator.response("No blogs present", null, 400));
        } catch (Exception e) {
            System.out.println("Exception login(): " + e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(ResponseCreator.response("server access error", null, 400));
        }
    }

    public ResponseEntity<Object> newBlog(Map<String, Object> blogMap) {
        try {
            if (((String) blogMap.get("blogTitle")).equals("")
                    || ((String) blogMap.get("blogDescription")).equals("")) {
                Date date = new Date();
                Timestamp timestamp = new Timestamp(date.getTime());

                Users blogUser = userRepository.findById((int) blogMap.get("user"));

                Blogs blog = new Blogs();
                for (int i : (ArrayList<Integer>) blogMap.get("tags")) {
                    System.out.println("tag id: " + i);
                    blog.getTags().add(tagRepository.findById(i));
                }

                blog.setBlogTitle((String) blogMap.get("blogTitle"));
                blog.setBlogDescription((String) blogMap.get("blogDescription"));
                blog.setPostedOn(timestamp);
                blog.setUser(blogUser);
                System.out.println(blog.getTags());

                blogRepository.save(blog);

                return ResponseEntity.ok(ResponseCreator.response("blog created successfully", null, 200));
            }
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(ResponseCreator.response("fields are invalid", null, 400));
        } catch (Exception e) {
            System.out.println("Exception login(): ");
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(ResponseCreator.response("server access error", null, 400));
        }
    }

    public ResponseEntity<Object> getBlog(int blogId) {
        Blogs blog = blogRepository.findById(blogId);
        if (blog != null) {
            return ResponseEntity.ok(ResponseCreator.response("Blog retrieved successfully", blog, 200));
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(ResponseCreator.response("No blog present for given id", null, 400));
    }

    public ResponseEntity<Object> getBlogsByUser(int userId) {
        try {
            Users user = userRepository.findById(userId);
            List<Blogs> blog = blogRepository.findByUser(user);
            if (blog != null) {
                return ResponseEntity.ok(ResponseCreator.response("All blog are retrieved sucessfully", blog, 200));
            }
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(ResponseCreator.response("No blog are present", null, 400));
        } catch (Exception e) {
            System.out.println("Exeption in getBlogsByUser(): " + e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(ResponseCreator.response("server access error", null, 400));
        }
    }

}