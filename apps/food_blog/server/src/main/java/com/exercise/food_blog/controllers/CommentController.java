package com.exercise.food_blog.controllers;

import java.util.Map;

import com.exercise.food_blog.services.CommentService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/auth/comment")
public class CommentController {
    @Autowired
    CommentService commentService;

    @GetMapping("/getCommentsByBlogId/{blog_id}")
    public ResponseEntity<Object> getCommentsByBlogId(@PathVariable int blog_id) {
        return commentService.getCommentsByBlogId(blog_id);
    }

    @PostMapping("addComment")
    public ResponseEntity<String> addComment(@RequestBody Map<String, Object> comment) {
        return commentService.addComment(comment);
    }

}