package com.exercise.food_blog.controllers;

import java.util.Map;

import com.exercise.food_blog.services.BlogService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/auth/blog")
public class BlogController {

    @Autowired
    BlogService blogService;

    @GetMapping("/getBlogs")
    public ResponseEntity<Object> getBlogs() {
        return blogService.getBlogs();
    }

    @GetMapping("/getBlog/{blogId}")
    public ResponseEntity<Object> getBlog(@PathVariable int blogId) {
        return blogService.getBlog(blogId);
    }

    @PostMapping("/newBlog")
    public ResponseEntity<Object> newBlog(@RequestBody Map<String, Object> blogMap) {
        return blogService.newBlog(blogMap);
    }

    @GetMapping("/getBlogsByUser/{userId}")
    public ResponseEntity<Object> getBlogsByUser(@PathVariable int userId) {
        return blogService.getBlogsByUser(userId);
    }

}