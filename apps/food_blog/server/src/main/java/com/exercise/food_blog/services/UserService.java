package com.exercise.food_blog.services;

import com.exercise.food_blog.models.Users;
import com.exercise.food_blog.repositories.UserRepository;
import com.exercise.food_blog.security.jwt.JwtProvider;
import com.exercise.food_blog.security.jwt.JwtResponse;
import com.exercise.food_blog.security.jwt.UserPrincipal;
import com.exercise.food_blog.utils.ResponseCreator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserService implements UserDetailsService {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserRepository userRepository;

    @Autowired
    PasswordEncoder encoder;

    @Autowired
    JwtProvider jwtProvider;

    public ResponseEntity<Object> register(Users user) {
        try {
            if (userRepository.existsByUserEmail(user.getUserEmail())) {
                return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                        .body(ResponseCreator.response("Email already exist", null, 400));
            }
            user.setUserPassword(encoder.encode(user.getUserPassword()));
            userRepository.save(user);
            return ResponseEntity.ok(ResponseCreator.response("Registered user successfully", user, 200));

        } catch (Exception e) {
            System.out.println("Exception register(): " + e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(ResponseCreator.response("server access error", null, 400));
        }
    }

    public ResponseEntity<Object> login(String userEmail, String userPassword) {
        try {
            Authentication authentication = authenticationManager
                    .authenticate(new UsernamePasswordAuthenticationToken(userEmail, userPassword));

            SecurityContextHolder.getContext().setAuthentication(authentication);
            String jwt = jwtProvider.generateJwtToken(authentication);

            return ResponseEntity.ok(ResponseCreator.response("Log in successful", new JwtResponse(jwt), 200));

        } catch (Exception e) {
            System.out.println("Exception login(): " + e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(ResponseCreator.response("server access error", null, 400));
        }
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Users user = userRepository.findByUserEmail(username)
                .orElseThrow(() -> new UsernameNotFoundException("Email Not Found with -> username : " + username));
        System.out.println(user.getUserEmail() + " " + username);
        return UserPrincipal.build(user);
    }

    public ResponseEntity<String> deleteUser(int id) {
        try {
            Users u = userRepository.findById(id);
            if (u != null) {
                userRepository.delete(u);
                return ResponseEntity.ok(ResponseCreator.response("User deleted successfully", null, 200));
            }
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(ResponseCreator.response("User not found for given id", null, 400));
        } catch (Exception e) {
            System.out.println("Exception deleteUser(): " + e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                    .body(ResponseCreator.response("server access error", null, 400));
        }
    }

}