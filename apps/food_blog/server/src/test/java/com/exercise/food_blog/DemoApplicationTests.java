package com.exercise.food_blog;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(plugin = { "pretty", "html:target/cucumber-html-report" }, features = { "src/test/java/resources" })
@SpringBootTest
public class DemoApplicationTests {

	@Test
	public void contextLoads() {
	}

}
