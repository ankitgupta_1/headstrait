package com.exercise.food_blog;

import static org.junit.Assert.assertEquals;

import org.json.simple.JSONObject;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import cucumber.api.java8.En;

public class userStepDefinition implements En {
    TestRestTemplate restTemplate = new TestRestTemplate();
    HttpHeaders headers = new HttpHeaders();
    JSONObject body = null;
    ResponseEntity<JSONObject> response = null;
    String url = "http://localhost:2020/api";

    public userStepDefinition() {
        When("the client makes the call to \\/auth\\/register", () -> {
            JSONObject obj = new JSONObject();
            obj.put("userName", "Test");
            obj.put("userEmail", "test@testing.com");
            obj.put("userPassword", "testing");
            HttpEntity<JSONObject> entity = new HttpEntity<>(obj, headers);
            response = restTemplate.exchange(url + "/auth/register", HttpMethod.POST, entity, JSONObject.class);
            body = response.getBody();
            System.out.println(body);
        });
        Then("the client recieves status code {int}", (Integer statusCode) -> {
            assertEquals(HttpStatus.valueOf(statusCode), response.getStatusCode());
        });
        And("the client recieves a message {string}", (String message) -> {
            assertEquals(message, response.getBody().get("message"));
        });

        When("the client makes the call to \\/auth\\/register with registered email id", () -> {
            JSONObject obj = new JSONObject();
            obj.put("userName", "Test");
            obj.put("userEmail", "test@testing.com");
            obj.put("userPassword", "testing");
            HttpEntity<JSONObject> entity = new HttpEntity<>(obj, headers);
            response = restTemplate.exchange(url + "/auth/register", HttpMethod.POST, entity, JSONObject.class);
            body = response.getBody();
            System.out.println(body);
        });

        When("the client makes the call to \\/auth\\/login with correct credentials", () -> {
            JSONObject obj = new JSONObject();
            obj.put("userEmail", "agupta.ag19@gmail.com");
            obj.put("userPassword", "ankit123");
            HttpEntity<JSONObject> entity = new HttpEntity<>(obj, headers);
            response = restTemplate.exchange(url + "/auth/login", HttpMethod.POST, entity, JSONObject.class);
            body = response.getBody();
            System.out.println(body);
        });

        When("the client makes the call to \\/auth\\/login with incorrect credentials", () -> {
            JSONObject obj = new JSONObject();
            obj.put("userEmail", "agupta.ag19@gmail.com");
            obj.put("userPassword", "ankit13");
            HttpEntity<JSONObject> entity = new HttpEntity<>(obj, headers);
            response = restTemplate.exchange(url + "/auth/login", HttpMethod.POST, entity, JSONObject.class);
            body = response.getBody();
            System.out.println(body);
        });

    }

}