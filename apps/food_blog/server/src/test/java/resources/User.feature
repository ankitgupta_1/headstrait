Feature: User service test

    Scenario: add new User success
        When the client makes the call to /auth/register
        Then the client recieves status code 200
        And the client recieves a message "Registered user successfully"

    Scenario: add new User failure
        When the client makes the call to /auth/register with registered email id
        Then the client recieves status code 400
        And the client recieves a message "Email already exist"

    Scenario: login User success
        When the client makes the call to /auth/login with correct credentials
        Then the client recieves status code 200
        And the client recieves a message "Log in successful"

    Scenario: login User failure
        When the client makes the call to /auth/login with incorrect credentials
        Then the client recieves status code 400
        And the client recieves a message "server access error"