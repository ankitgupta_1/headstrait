Feature: Blog service test

    Scenario: add new Blog success
        When the client makes the call to /auth/blog/newBlog
        Then the client recieves status code 200
        And the client recieves a message "blog created successfully"

    Scenario: add new User failure
        When the client makes the call to /auth/blog/newBlog with invalid fields
        Then the client recieves status code 400
        And the client recieves a message "fields are invalid"