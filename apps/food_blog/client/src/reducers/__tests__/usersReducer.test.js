import usersReducer from "../usersReducer";
import { LOGIN_USER, REGISTER_USER, ERROR_TYPE } from "../../actions/Types";

describe("Testing usersReducer for type LOGIN_USER", () => {
	// test with initialState
	it("with initialState - returns a state object with loggedInUser equal to the payload.data in action of type LOGIN_USER (when the state is in initial state)", () => {
		const action = {
			type: LOGIN_USER,
			payload: { data: {} }
		};
		const returnedState = usersReducer(undefined, action);
		expect(returnedState).toEqual({ loggedInUser: action.payload.data });
	});

	// test without initialState
	it("w/o initialState - returns a state object with loggedInUser equal to the payload.data in action of type LOGIN_USER", () => {
		const initialState = {
			loggedInUser: {}
		};
		const action = {
			type: LOGIN_USER,
			payload: { data: {} }
		};
		const returnedState = usersReducer(initialState, action);
		expect(returnedState).toEqual({
			loggedInUser: action.payload.data
		});
	});
});

describe("Testing usersReducer for type REGISTER_USER", () => {
	// test with initialState
	it("with initialState - returns the same state action of type REGISTER_USER", () => {
		const action = {
			type: REGISTER_USER,
			payload: { data: {} }
		};
		const returnedState = usersReducer(undefined, action);
		expect(returnedState).toEqual({ loggedInUser: {} });
	});

	// test without initialState
	it("w/o initialState - returns the same state action of type REGISTER_USER", () => {
		const initialState = {
			loggedInUser: {}
		};
		const action = {
			type: REGISTER_USER,
			payload: { data: {} }
		};
		const returnedState = usersReducer(initialState, action);
		expect(returnedState).toEqual({
			loggedInUser: initialState.loggedInUser
		});
	});
});

describe("Testing usersReducer for type ERROR_TYPE", () => {
	// test with initialState
	it("with initialState - return same state action of type ERROR_TYPE", () => {
		const action = {
			type: ERROR_TYPE
		};
		const returnedState = usersReducer(undefined, action);
		expect(returnedState).toEqual({ loggedInUser: {} });
	});

	// test without initialState
	it("w/o initialState - return same state action of type ERROR_TYPE", () => {
		const initialState = {
			loggedInUser: {}
		};
		const action = {
			type: ERROR_TYPE
		};
		const returnedState = usersReducer(initialState, action);
		expect(returnedState).toEqual({
			loggedInUser: initialState.loggedInUser
		});
	});
});

describe("Testing usersReducer for type SOME_TYPE", () => {
	// test with initialState
	it("with initialState - return same state action of type SOME_TYPE as it goes to default case", () => {
		const action = {
			type: "SOME_TYPE"
		};
		const returnedState = usersReducer(undefined, action);
		expect(returnedState).toEqual({ loggedInUser: {} });
	});

	// test without initialState
	it("w/o initialState - return same state action of type SOME_TYPE as it goes to default case", () => {
		const initialState = {
			loggedInUser: {}
		};
		const action = {
			type: "SOME_TYPE"
		};
		const returnedState = usersReducer(initialState, action);
		expect(returnedState).toEqual({
			loggedInUser: initialState.loggedInUser
		});
	});
});
