import blogsReducer from "../blogsReducer";
import {
	GET_BLOGS,
	GET_BLOG,
	CREATE_BLOG,
	ERROR_TYPE
} from "../../actions/Types";

describe("Testing blogsReducer for type GET_BLOGS", () => {
	// test with initialState
	it("with initialState - returns a state object with blogs array equal to the payload.data in action of type GET_BLOGS", () => {
		const action = {
			type: GET_BLOGS,
			payload: { data: [{}, {}, {}] }
		};
		const returnedState = blogsReducer(undefined, action);
		expect(returnedState).toEqual({
			blogs: action.payload.data,
			blog: {}
		});
	});

	// test without initialState
	it("w/o initialState - returns a state object with blogs array equal to the payload.data in action of type GET_BLOGS", () => {
		const initialState = {
			blogs: [1, 2, 3],
			blog: {}
		};
		const action = {
			type: GET_BLOGS,
			payload: { data: [{}, {}, {}] }
		};
		const returnedState = blogsReducer(initialState, action);
		expect(returnedState).toEqual({ blogs: action.payload.data, blog: {} });
	});
});

describe("Testing blogsReducer for type GET_BLOG", () => {
	// test with initialState
	it("with initialState - returns a state object with a single blog array equal to the payload.data in action of type GET_BLOG", () => {
		const action = {
			type: GET_BLOG,
			payload: { data: {} }
		};
		const returnedState = blogsReducer(undefined, action);
		expect(returnedState).toEqual({
			blogs: [],
			blog: action.payload.data
		});
	});

	// test without initialState
	it("w/o initialState - return a state object with a single blog array equal to the payload in action of type GET_BLOG", () => {
		const initialState = {
			blogs: [],
			blog: { blogId: 1, blogTitle: "Dummy title for test" }
		};
		const action = {
			type: GET_BLOG,
			payload: { data: { blogId: 1, blogTitle: "Dummy title for test" } }
		};
		const returnedState = blogsReducer(initialState, action);
		expect(returnedState).toEqual({ blog: action.payload.data, blogs: [] });
	});
});

describe("Testing blogsReducer for type CREATE_BLOG", () => {
	// test with initialState
	it("with initialState - return same state action of type CREATE_BLOG", () => {
		const action = {
			type: CREATE_BLOG
		};
		const returnedState = blogsReducer(undefined, action);
		expect(returnedState).toEqual({ blogs: [], blog: {} });
	});

	// test without initialState
	it("w/o initialState - return same state action of type CREATE_BLOG", () => {
		const initialState = {
			blogs: [1, 2, 3],
			blog: {}
		};
		const action = {
			type: CREATE_BLOG
		};
		const returnedState = blogsReducer(initialState, action);
		expect(returnedState).toEqual({
			blogs: initialState.blogs,
			blog: initialState.blog
		});
	});
});

describe("Testing blogsReducer for type ERROR_TYPE", () => {
	// test with initialState
	it("with initialState - return same state action of type ERROR_TYPE", () => {
		const action = {
			type: ERROR_TYPE
		};
		const returnedState = blogsReducer(undefined, action);
		expect(returnedState).toEqual({ blogs: [], blog: {} });
	});

	// test without initialState
	it("w/o initialState - return same state action of type ERROR_TYPE", () => {
		const initialState = {
			blogs: [1, 2, 3],
			blog: {}
		};
		const action = {
			type: ERROR_TYPE
		};
		const returnedState = blogsReducer(initialState, action);
		expect(returnedState).toEqual({
			blogs: initialState.blogs,
			blog: initialState.blog
		});
	});
});

describe("Testing blogsReducer for type SOME_TYPE", () => {
	// test with initialState
	it("with initialState - return same state action of type SOME_TYPE as it goes to default case", () => {
		const action = {
			type: "SOME_TYPE"
		};
		const returnedState = blogsReducer(undefined, action);
		expect(returnedState).toEqual({ blogs: [], blog: {} });
	});

	// test without initialState
	it("w/o initialState - return same state action of type SOME_TYPE as it goes to default case", () => {
		const initialState = {
			blogs: [1, 2, 3],
			blog: {}
		};
		const action = {
			type: "SOME_TYPE"
		};
		const returnedState = blogsReducer(initialState, action);
		expect(returnedState).toEqual({
			blogs: initialState.blogs,
			blog: initialState.blog
		});
	});
});
