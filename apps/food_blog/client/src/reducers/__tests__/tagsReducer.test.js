import tagsReducer from "../tagsReducer";
import { GET_TAGS, ERROR_TYPE } from "../../actions/Types";

describe("Testing tagsReducer for type GET_TAGS", () => {
	// test with initialState
	it("with initialState - returns a state object with tags array equal to the payload.data in action of type GET_TAGS", () => {
		const action = {
			type: GET_TAGS,
			payload: { data: [{}, {}, {}] }
		};
		const returnedState = tagsReducer(undefined, action);
		expect(returnedState).toEqual({
			tags: action.payload.data
		});
	});

	// test without initialState
	it("w/o initialState - returns a state object with tags array equal to the payload.data in action of type GET_TAGS", () => {
		const initialState = {
			tags: [1, 2, 3]
		};
		const action = {
			type: GET_TAGS,
			payload: { data: [{}, {}, {}] }
		};
		const returnedState = tagsReducer(initialState, action);
		expect(returnedState).toEqual({ tags: action.payload.data });
	});
});

describe("Testing tagsReducer for type ERROR_TYPE", () => {
	// test with initialState
	it("with initialState - return same state action of type ERROR_TYPE", () => {
		const action = {
			type: ERROR_TYPE
		};
		const returnedState = tagsReducer(undefined, action);
		expect(returnedState).toEqual({ tags: [] });
	});

	// test without initialState
	it("w/o initialState - return same state action of type ERROR_TYPE", () => {
		const initialState = {
			tags: [1, 2, 3]
		};
		const action = {
			type: ERROR_TYPE
		};
		const returnedState = tagsReducer(initialState, action);
		expect(returnedState).toEqual({
			tags: initialState.tags
		});
	});
});

describe("Testing tagsReducer for type SOME_TYPE", () => {
	// test with initialState
	it("with initialState - return same state action of type SOME_TYPE as it goes to default case", () => {
		const action = {
			type: "SOME_TYPE"
		};
		const returnedState = tagsReducer(undefined, action);
		expect(returnedState).toEqual({ tags: [] });
	});

	// test without initialState
	it("w/o initialState - return same state action of type SOME_TYPE as it goes to default case", () => {
		const initialState = {
			tags: [1, 2, 3]
		};
		const action = {
			type: "SOME_TYPE"
		};
		const returnedState = tagsReducer(initialState, action);
		expect(returnedState).toEqual({
			tags: initialState.tags
		});
	});
});
