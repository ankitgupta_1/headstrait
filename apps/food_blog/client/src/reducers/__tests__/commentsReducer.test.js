import commentsReducer from "../commentsReducer";
import { GET_COMMENTS, CREATE_COMMENT, ERROR_TYPE } from "../../actions/Types";

describe("Testing commentsReducer for type GET_COMMENTS", () => {
	// test with initialState
	it("with initialState - returns a state object with comments array equal to the payload.data in action of type GET_COMMENTS", () => {
		const action = {
			type: GET_COMMENTS,
			payload: { data: [{}, {}, {}] }
		};
		const returnedState = commentsReducer(undefined, action);
		expect(returnedState).toEqual({
			comments: action.payload.data
		});
	});

	// test without initialState
	it("w/o initialState - returns a state object with comments array equal to the payload.data in action of type GET_COMMENTS", () => {
		const initialState = {
			comments: [1, 2, 3]
		};
		const action = {
			type: GET_COMMENTS,
			payload: { ata: [{}, {}, {}] }
		};
		const returnedState = commentsReducer(initialState, action);
		expect(returnedState).toEqual({ comments: action.payload.data });
	});
});

describe("Testing commentsReducer for type CREATE_COMMENT", () => {
	// test with initialState
	it("with initialState - return same state action of type CREATE_COMMENT", () => {
		const action = {
			type: CREATE_COMMENT
		};
		const returnedState = commentsReducer(undefined, action);
		expect(returnedState).toEqual({ comments: [] });
	});

	// test without initialState
	it("w/o initialState - return same state action of type CREATE_COMMENT", () => {
		const initialState = {
			comments: [1, 2, 3]
		};
		const action = {
			type: CREATE_COMMENT
		};
		const returnedState = commentsReducer(initialState, action);
		expect(returnedState).toEqual({ comments: initialState.comments });
	});
});

describe("Testing commentsReducer for type ERROR_TYPE", () => {
	// test with initialState
	it("with initialState - return same state action of type ERROR_TYPE", () => {
		const action = {
			type: ERROR_TYPE
		};
		const returnedState = commentsReducer(undefined, action);
		expect(returnedState).toEqual({ comments: [] });
	});

	// test without initialState
	it("w/o initialState - return same state action of type ERROR_TYPE", () => {
		const initialState = {
			comments: [1, 2, 3]
		};
		const action = {
			type: ERROR_TYPE
		};
		const returnedState = commentsReducer(initialState, action);
		expect(returnedState).toEqual({
			comments: initialState.comments
		});
	});
});

describe("Testing commentsReducer for type SOME_TYPE", () => {
	// test with initialState
	it("with initialState - return same state action of type SOME_TYPE as it goes to default case", () => {
		const action = {
			type: "SOME_TYPE"
		};
		const returnedState = commentsReducer(undefined, action);
		expect(returnedState).toEqual({ comments: [] });
	});

	// test without initialState
	it("w/o initialState - return same state action of type SOME_TYPE as it goes to default case", () => {
		const initialState = {
			comments: [1, 2, 3]
		};
		const action = {
			type: "SOME_TYPE"
		};
		const returnedState = commentsReducer(initialState, action);
		expect(returnedState).toEqual({
			comments: initialState.comments
		});
	});
});
