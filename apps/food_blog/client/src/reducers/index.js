import { combineReducers } from "redux";
import usersReducer from "./usersReducer";
import blogsReducer from "./blogsReducer";
import tagsReducer from "./tagsReducer";
import commentsReducer from "./commentsReducer";

export default combineReducers({
	usersReducer,
	blogsReducer,
	tagsReducer,
	commentsReducer
});
