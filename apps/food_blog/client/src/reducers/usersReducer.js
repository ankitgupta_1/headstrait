import { REGISTER_USER, LOGIN_USER, ERROR_TYPE } from "../actions/Types";

const initialState = {
	loggedInUser: {}
};

export default function(state = initialState, action) {
	switch (action.type) {
		case REGISTER_USER:
			return state;
		case LOGIN_USER:
			return {
				...state,
				loggedInUser: action.payload.data
			};
		case ERROR_TYPE:
			return state;
		default:
			return state;
	}
}
