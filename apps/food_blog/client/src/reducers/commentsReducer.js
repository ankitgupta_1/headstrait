import { GET_COMMENTS, CREATE_COMMENT, ERROR_TYPE } from "../actions/Types";

const initialState = {
	comments: []
};

export default function(state = initialState, action) {
	switch (action.type) {
		case GET_COMMENTS:
			return {
				...state,
				comments: action.payload.data
			};
		case CREATE_COMMENT:
			return state;

		case ERROR_TYPE:
			return state;

		default:
			return state;
	}
}
