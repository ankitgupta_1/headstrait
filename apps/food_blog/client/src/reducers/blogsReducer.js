import { GET_BLOGS, GET_BLOG, CREATE_BLOG, ERROR_TYPE } from "../actions/Types";

const initialState = {
	blogs: [],
	blog: {}
};

export default function(state = initialState, action) {
	switch (action.type) {
		case GET_BLOGS:
			return {
				...state,
				blogs: action.payload.data
			};
		case GET_BLOG:
			return {
				...state,
				blog: action.payload.data
			};
		case CREATE_BLOG:
			return state;
		case ERROR_TYPE:
			return state;
		default:
			return state;
	}
}
