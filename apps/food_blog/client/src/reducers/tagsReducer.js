import { GET_TAGS, ERROR_TYPE } from "../actions/Types";

const initialState = {
	tags: []
};

export default function(state = initialState, action) {
	switch (action.type) {
		case GET_TAGS:
			return {
				...state,
				tags: action.payload.data
			};
		case ERROR_TYPE:
			return state;
		default:
			return state;
	}
}
