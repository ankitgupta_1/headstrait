import { CREATE_BLOG, GET_BLOGS, ERROR_TYPE, GET_BLOG } from "./Types";

import axios from "axios";

const url = "http://localhost:2020/api/auth/blog";

export const createBlog = blogDetails => dispatch => {
	return axios
		.post(url + "/newBlog", blogDetails)
		.then(res => {
			console.log("Response data : " + res.data);
			dispatch({
				type: CREATE_BLOG,
				payload: {
					successMessage: res.data.message
				}
			});
		})
		.catch(err => {
			console.log(err);
			dispatch({
				type: ERROR_TYPE
				// payload: {
				// 	errorMessage: err.response.data.message
				// }
			});
		});
};

export const getBlogs = () => dispatch => {
	return axios
		.get(url + "/getBlogs")
		.then(res => {
			dispatch({
				type: GET_BLOGS,
				payload: {
					data: res.data.data,
					successMessage: res.data.message
				}
			});
		})
		.catch(err => {
			console.log(err);
			dispatch({
				type: ERROR_TYPE
				// payload: {
				// 	errorMessage: err.response.data.message
				// }
			});
		});
};

export const getBlog = blogId => dispatch => {
	return axios
		.get(url + "/getBlog/" + blogId)
		.then(res => {
			dispatch({
				type: GET_BLOG,
				payload: {
					data: res.data.data,
					successMessage: res.data.message
				}
			});
		})
		.catch(err => {
			console.log(err);
			dispatch({
				type: ERROR_TYPE
				// payload: {
				// 	errorMessage: err.response.data.message
				// }
			});
		});
};

export const getBlogsByUser = userId => dispatch => {
	return axios
		.get(url + "/getBlogsByUser/" + userId)
		.then(res => {
			dispatch({
				type: GET_BLOGS,
				payload: {
					data: res.data.data,
					successMessage: res.data.message
				}
			});
		})
		.catch(err => {
			console.log(err);
			dispatch({
				type: ERROR_TYPE
				// payload: {
				// 	errorMessage: err.response.data.message
				// }
			});
		});
};
