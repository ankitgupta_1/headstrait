// import { CREATE_BLOG, GET_BLOGS, GET_BLOG, ERROR_TYPE } from "../Types";

// import * as action from "../blogAction";
// import moxios from "moxios";
// import configureMockStore from "redux-mock-store";
// import thunk from "redux-thunk";

// const middlewares = [thunk];
// const mockStore = configureMockStore(middlewares);

// const url = "http://localhost:2020/api/auth/blog";

// describe("Testing for CREATE_BLOG action", () => {
// 	beforeEach(() => {
// 		moxios.install();
// 	});
// 	afterEach(() => {
// 		moxios.uninstall();
// 	});

// 	it("when a user creates a new blog it should fire the createBlog action", () => {
// 		const store = mockStore({});
// 		const blogDetails = {
// 			blogTitle: "Food Hopping at Churchgate",
// 			blogDescription:
// 				"1. Candies\nCandies has to top this list, there is no other way. In particular, the outlet on Pali Hill is magical. A Portugese-style villa that embraces the hill as it entwines along it, the alfresco seating is best when the weather allows it. Candies is not known for spectacular food, though the soft chicken sandwiches (served with old-school wafers) that get sold out by 11am are the best I have had anywhere. It is all about comfort food in a relaxed environment, great for breakfasts or a cold coffee with friends.  The pocket friendly prices are refreshing.\nWhere: Mac Ronells, Next to Learners Academy School, Pali Hill, Bandra West.\nCost for two: Rs. 700",
// 			user: 1,
// 			tags: [1, 4, 6, 7, 10]
// 		};
// 		moxios.stubRequest(url + "/newBlog", blogDetails, {
// 			status: 200
// 		});
// 		const expectedActions = [
// 			{
// 				type: CREATE_BLOG
// 			}
// 		];

// 		return store.dispatch(action.createBlog()).then(() => {
// 			expect(store.getActions()).toEqual(expectedActions);
// 		});
// 	});

// 	it("when a user creates a new blog it should fire the createBlog action expects ERROR_TYPE as field user not exist", () => {
// 		const store = mockStore({});
// 		const blogDetails = {
// 			blogTitle: "Food Hopping at Churchgate",
// 			blogDescription:
// 				"1. Candies\nCandies has to top this list, there is no other way. In particular, the outlet on Pali Hill is magical. A Portugese-style villa that embraces the hill as it entwines along it, the alfresco seating is best when the weather allows it. Candies is not known for spectacular food, though the soft chicken sandwiches (served with old-school wafers) that get sold out by 11am are the best I have had anywhere. It is all about comfort food in a relaxed environment, great for breakfasts or a cold coffee with friends.  The pocket friendly prices are refreshing.\nWhere: Mac Ronells, Next to Learners Academy School, Pali Hill, Bandra West.\nCost for two: Rs. 700",
// 			user: 100,
// 			tags: [1, 4, 6, 7, 10]
// 		};
// 		moxios.stubRequest(url + "/newBlog", blogDetails, {
// 			status: 400
// 		});
// 		const expectedActions = [
// 			{
// 				type: ERROR_TYPE
// 			}
// 		];

// 		return store.dispatch(action.createBlog()).then(() => {
// 			expect(store.getActions()).toEqual(expectedActions);
// 		});
// 	});
// });

// // describe("Testing for GET_BLOGS actions ", () => {
// // 	beforeEach(() => {
// // 		moxios.install();
// // 	});

// // 	afterEach(() => {
// // 		moxios.uninstall();
// // 	});

// // 	it("should create an action of type GET_BLOGS and the payload should be same as the api response when the response is 200", () => {
// // 		const responseOfApi = [
// // 			{
// // 				blogs: undefined
// // 			}
// // 		];
// // 		moxios.stubRequest(url + "/getBlogs", {
// // 			status: 200,
// // 			response: { data: responseOfApi }
// // 		});
// // 		const store = mockStore({});
// // 		const expectedActions = [
// // 			{
// // 				type: GET_BLOGS,
// // 				payload: responseOfApi
// // 			}
// // 		];
// // 		return store.dispatch(action.getBlogs()).then(() => {
// // 			expect(store.getActions()).toEqual(expectedActions);
// // 		});
// // 	});

// // 	it("should create an action of type ERROR_TYPE and the payload should be empty for status 400", () => {
// // 		const responseOfApi = [{}, {}, {}];
// // 		moxios.stubRequest(url + "/getBlogs", {
// // 			status: 400,
// // 			response: { data: responseOfApi }
// // 		});
// // 		const store = mockStore({});
// // 		const expectedActions = [
// // 			{
// // 				type: ERROR_TYPE
// // 			}
// // 		];
// // 		return store.dispatch(action.getBlogs()).then(() => {
// // 			expect(store.getActions()).toEqual(expectedActions);
// // 		});
// // 	});

// // 	it("should create an action of type GET_BLOGS for blogs of a user and the payload should be same as the api response when the response is 200", () => {
// // 		const responseOfApi = [
// // 			{
// // 				blogs: undefined
// // 			}
// // 		];
// // 		moxios.stubRequest(url + "/getBlogsByUser/" + 1, {
// // 			status: 200,
// // 			response: { data: responseOfApi }
// // 		});
// // 		const store = mockStore({});
// // 		const expectedActions = [
// // 			{
// // 				type: GET_BLOGS,
// // 				payload: responseOfApi
// // 			}
// // 		];
// // 		return store.dispatch(action.getBlogsByUser()).then(() => {
// // 			expect(store.getActions()).toEqual(expectedActions);
// // 		});
// // 	});

// // 	it("should create an action of type GET_BLOGS for blogs of a user and the payload should be empty for status 400", () => {
// // 		const responseOfApi = [{}, {}, {}];
// // 		moxios.stubRequest(url + "/getBlogsByUser/" + 100, {
// // 			status: 400,
// // 			response: { data: responseOfApi }
// // 		});
// // 		const store = mockStore({});
// // 		const expectedActions = [
// // 			{
// // 				type: ERROR_TYPE
// // 			}
// // 		];
// // 		return store.dispatch(action.getBlogsByUser()).then(() => {
// // 			expect(store.getActions()).toEqual(expectedActions);
// // 		});
// // 	});
// // });

// // describe("Testing for GET_BLOG actions ", () => {
// // 	beforeEach(() => {
// // 		moxios.install();
// // 	});

// // 	afterEach(() => {
// // 		moxios.uninstall();
// // 	});

// // 	it("should create an action of type GET_BLOG and the payload should be same as the api response when the response is 200", () => {
// // 		const responseOfApi = [
// // 			{
// // 				blog: undefined
// // 			}
// // 		];
// // 		moxios.stubRequest(url + "/getBlog/" + 3, {
// // 			status: 200,
// // 			response: { data: responseOfApi }
// // 		});
// // 		const store = mockStore({});
// // 		const expectedActions = [
// // 			{
// // 				type: GET_BLOG,
// // 				payload: responseOfApi
// // 			}
// // 		];
// // 		return store.dispatch(action.getBlog()).then(() => {
// // 			expect(store.getActions()).toEqual(expectedActions);
// // 		});
// // 	});

// // 	it("should create an action of type ERROR_TYPE and the payload should be empty for status 400", () => {
// // 		const responseOfApi = [{}, {}, {}];
// // 		moxios.stubRequest(url + "/getBlog/" + 100, {
// // 			status: 400,
// // 			response: { data: responseOfApi }
// // 		});
// // 		const store = mockStore({});
// // 		const expectedActions = [
// // 			{
// // 				type: ERROR_TYPE
// // 			}
// // 		];
// // 		return store.dispatch(action.getBlog()).then(() => {
// // 			expect(store.getActions()).toEqual(expectedActions);
// // 		});
// // 	});
// // });
