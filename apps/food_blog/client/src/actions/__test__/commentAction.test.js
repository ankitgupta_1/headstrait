// import { GET_COMMENTS, CREATE_COMMENT, ERROR_TYPE } from "../Types";

// import * as action from "../commentAction";
// import moxios from "moxios";
// import configureMockStore from "redux-mock-store";
// import thunk from "redux-thunk";

// const middlewares = [thunk];
// const mockStore = configureMockStore(middlewares);

// const url = "http://localhost:2020/api/auth/comment";

// describe("Testing for GET_COMMENTS actions ", () => {
// 	beforeEach(() => {
// 		moxios.install();
// 	});

// 	afterEach(() => {
// 		moxios.uninstall();
// 	});

// 	it("should create an action of type GET_COMMENTS and the payload should be same as the api response when the response is 200", () => {
// 		const responseOfApi = [
// 			{
// 				comments: undefined
// 			}
// 		];
// 		moxios.stubRequest(url + "/getComments/" + 3, {
// 			status: 200,
// 			response: { data: responseOfApi }
// 		});
// 		const store = mockStore({});
// 		const expectedActions = [
// 			{
// 				type: GET_COMMENTS,
// 				payload: responseOfApi
// 			}
// 		];
// 		return store.dispatch(action.getComments()).then(() => {
// 			expect(store.getActions()).toEqual(expectedActions);
// 		});
// 	});

// 	it("should create an action of type ERROR_TYPE and the payload should be empty for status 400", () => {
// 		const responseOfApi = [{}, {}, {}];
// 		moxios.stubRequest(url + "/getComments/" + 100, {
// 			status: 400,
// 			response: { data: responseOfApi }
// 		});
// 		const store = mockStore({});
// 		const expectedActions = [
// 			{
// 				type: ERROR_TYPE
// 			}
// 		];
// 		return store.dispatch(action.getComments()).then(() => {
// 			expect(store.getActions()).toEqual(expectedActions);
// 		});
// 	});
// });

// describe("Testing for CREATE_COMMENT action", () => {
// 	beforeEach(() => {
// 		moxios.install();
// 	});
// 	afterEach(() => {
// 		moxios.uninstall();
// 	});

// 	it("when a user creates a new comment it should fire the createComment action expects response 200", () => {
// 		const store = mockStore({});
// 		const commentDetails = {
// 			comment: "This is so awesome",
// 			userId: 1,
// 			blogId: 3
// 		};
// 		moxios.stubRequest(url + "/addComment", commentDetails, {
// 			status: 200
// 		});
// 		const expectedActions = [
// 			{
// 				type: CREATE_COMMENT
// 			}
// 		];

// 		return store.dispatch(action.createComment()).then(() => {
// 			expect(store.getActions()).toEqual(expectedActions);
// 		});
// 	});

// 	it("fires the createComment action expects ERROR_TYPE as field user not exist", () => {
// 		const store = mockStore({});
// 		const commentDetails = {
// 			comment: "This is so awesome",
// 			userId: 100,
// 			blogId: 3
// 		};
// 		moxios.stubRequest(url + "/addComment", commentDetails, {
// 			status: 400
// 		});
// 		const expectedActions = [
// 			{
// 				type: ERROR_TYPE
// 			}
// 		];

// 		return store.dispatch(action.createComment()).then(() => {
// 			expect(store.getActions()).toEqual(expectedActions);
// 		});
// 	});

// 	it("fires the createComment action expects ERROR_TYPE as field blog not exist", () => {
// 		const store = mockStore({});
// 		const commentDetails = {
// 			comment: "This is so awesome",
// 			userId: 1,
// 			blogId: 300
// 		};
// 		moxios.stubRequest(url + "/addComment", commentDetails, {
// 			status: 400
// 		});
// 		const expectedActions = [
// 			{
// 				type: ERROR_TYPE
// 			}
// 		];

// 		return store.dispatch(action.createComment()).then(() => {
// 			expect(store.getActions()).toEqual(expectedActions);
// 		});
// 	});
// });
