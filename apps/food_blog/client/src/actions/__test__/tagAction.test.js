// import { GET_TAGS, ERROR_TYPE } from "../Types";

// import * as action from "../blogAction";
// import moxios from "moxios";
// import configureMockStore from "redux-mock-store";
// import thunk from "redux-thunk";

// const middlewares = [thunk];
// const mockStore = configureMockStore(middlewares);

// const url = "http://localhost:2020/api/auth/tag";

// describe("Testing for GET_TAGS actions ", () => {
// 	beforeEach(() => {
// 		moxios.install();
// 	});

// 	afterEach(() => {
// 		moxios.uninstall();
// 	});

// 	it("should create an action of type GET_TAGS and the payload should be same as the api response when the response is 200", () => {
// 		const responseOfApi = [
// 			{
// 				tags: undefined
// 			}
// 		];
// 		moxios.stubRequest(url + "/getTags", {
// 			status: 200,
// 			response: { data: responseOfApi }
// 		});
// 		const store = mockStore({});
// 		const expectedActions = [
// 			{
// 				type: GET_TAGS,
// 				payload: responseOfApi
// 			}
// 		];
// 		return store.dispatch(action.getTags()).then(() => {
// 			expect(store.getActions()).toEqual(expectedActions);
// 		});
// 	});

// 	it("should create an action of type ERROR_TYPE and the payload should be empty for status 400", () => {
// 		const responseOfApi = [{}, {}, {}];
// 		moxios.stubRequest(url + "/getTags/", {
// 			status: 400,
// 			response: { data: responseOfApi }
// 		});
// 		const store = mockStore({});
// 		const expectedActions = [
// 			{
// 				type: ERROR_TYPE
// 			}
// 		];
// 		return store.dispatch(action.getTags()).then(() => {
// 			expect(store.getActions()).toEqual(expectedActions);
// 		});
// 	});
// });
