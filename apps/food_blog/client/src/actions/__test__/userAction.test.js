// import { REGISTER_USER, ERROR_TYPE, LOGIN_USER } from "../Types";

// import * as action from "../userAction";
// import moxios from "moxios";
// import configureMockStore from "redux-mock-store";
// import thunk from "redux-thunk";

// const middlewares = [thunk];
// const mockStore = configureMockStore(middlewares);

// const url = "http://localhost:2020/api/auth";

// describe("Testing for REGISTER_USER action", () => {
// 	beforeEach(() => {
// 		moxios.install();
// 	});
// 	afterEach(() => {
// 		moxios.uninstall();
// 	});

// 	it("when a user registers it should fire the registerUser action", () => {
// 		const store = mockStore({});
// 		const userDetails = {
// 			userName: "Ankit Gupta",
// 			userEmail: "ankitg@mail.com",
// 			userPassword: "ankit123"
// 		};
// 		moxios.stubRequest(url + "/register", userDetails, {
// 			status: 200
// 		});
// 		const expectedActions = [
// 			{
// 				type: REGISTER_USER
// 			}
// 		];

// 		return store.dispatch(action.registerUser()).then(() => {
// 			expect(store.getActions()).toEqual(expectedActions);
// 		});
// 	});
// });

// describe("Testing for LOGIN_USER action", () => {
// 	beforeEach(() => {
// 		moxios.install();
// 	});
// 	afterEach(() => {
// 		moxios.uninstall();
// 	});

// 	it("when a user logins it should fire the loginUser action for success status 200", () => {
// 		const store = mockStore({});
// 		const userDetails = {
// 			userEmail: "ankitgupta1697@gmail.com",
// 			userPassword: "ankit123"
// 		};
// 		const responseOfApi = [
// 			{
// 				loggedInUser: ""
// 			}
// 		];
// 		moxios.stubRequest(url + "/login", userDetails, {
// 			status: 200
// 		});
// 		const expectedActions = [
// 			{
// 				type: LOGIN_USER,
// 				payload: responseOfApi
// 			}
// 		];

// 		return store.dispatch(action.loginUser()).then(() => {
// 			expect(store.getActions()).toEqual(expectedActions);
// 		});
// 	});

// 	it("when a user logins it should fire the loginUser action for success status 200", () => {
// 		const store = mockStore({});
// 		const userDetails = {
// 			userEmail: "ankitgupt1697@mail.com",
// 			userPassword: "ankit123"
// 		};
// 		moxios.stubRequest(url + "/login", userDetails, {
// 			status: 400
// 		});
// 		const expectedActions = [
// 			{
// 				type: ERROR_TYPE
// 			}
// 		];

// 		return store.dispatch(action.loginUser()).then(() => {
// 			expect(store.getActions()).toEqual(expectedActions);
// 		});
// 	});
// });
