import { REGISTER_USER, LOGIN_USER, ERROR_TYPE } from "./Types";
import decode from "jwt-decode";

import axios from "axios";

const url = "http://localhost:2020/api";

export const registerUser = userDetails => dispatch => {
	return axios
		.post(url + "/auth/register", userDetails)
		.then(res => {
			dispatch({
				type: REGISTER_USER
				// payload: {
				// 	successMessage: res.data.message
				// }
			});
		})
		.catch(err => {
			console.log(err);
			dispatch({
				type: ERROR_TYPE
				// payload: {
				// 	errorMessage: err.response.data.message
				// }
			});
		});
};

export const loginUser = credentials => dispatch => {
	return axios
		.post(url + "/auth/login", credentials)
		.then(res => {
			console.log("Response data : " + res.data);
			localStorage.setItem("token", res.data.data.accessToken);
			const loggedInUser = decode(localStorage.getItem("token"));
			dispatch({
				type: LOGIN_USER,
				payload: {
					data: loggedInUser,
					successMessage: res.data.message
				}
			});
			console.log(res.data.data);
		})
		.catch(err => {
			console.log(err);
			dispatch({
				type: ERROR_TYPE
				// payload: {
				// 	errorMessage: err.response.data.message
				// }
			});
		});
};
