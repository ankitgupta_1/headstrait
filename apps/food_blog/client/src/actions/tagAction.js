import { GET_TAGS, ERROR_TYPE } from "./Types";

import axios from "axios";

const url = "http://localhost:2020/api/auth/tag";

export const getTags = () => dispatch => {
	return axios
		.get(url + "/getTags")
		.then(res => {
			dispatch({
				type: GET_TAGS,
				payload: {
					data: res.data.data,
					successMessage: res.data.message
				}
			});
		})
		.catch(err => {
			console.log(err);
			dispatch({
				type: ERROR_TYPE
				// payload: {
				// 	errorMessage: err.response.data.message
				// }
			});
		});
};
