import { GET_COMMENTS, CREATE_COMMENT, ERROR_TYPE } from "./Types";

import axios from "axios";

const url = "http://localhost:2020/api/auth/comment";

export const getComments = blogId => dispatch => {
	return axios
		.get(url + "/getCommentsByBlogId/" + blogId)
		.then(res => {
			dispatch({
				type: GET_COMMENTS,
				payload: {
					data: res.data.data,
					successMessage: res.data.message
				}
			});
		})
		.catch(err => {
			console.log(err);
			dispatch({
				type: ERROR_TYPE
				// payload: {
				// 	errorMessage: err.response.data.message
				// }
			});
		});
};

export const createComment = commentDetails => dispatch => {
	return axios
		.post(url + "/addComment", commentDetails)
		.then(res => {
			console.log("Response data : " + res.data);
			dispatch({
				type: CREATE_COMMENT,
				payload: {
					successMessage: res.data.message
				}
			});
		})
		.catch(err => {
			console.log(err);
			dispatch({
				type: ERROR_TYPE
				// payload: {
				// 	errorMessage: err.response.data.message
				// }
			});
		});
};
