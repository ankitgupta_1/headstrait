export const ERROR_TYPE = "ERROR_TYPE";

export const REGISTER_USER = "REGISTER_USER";
export const LOGIN_USER = "LOGIN_USER";

export const CREATE_BLOG = "CREATE_BLOG";
export const GET_BLOGS = "GET_BLOGS";
export const GET_BLOG = "GET_BLOG";

export const GET_TAGS = "GET_TAGS";

export const GET_COMMENTS = "GET_COMMENTS";
export const CREATE_COMMENT = "CREATE_COMMENT";
