import React from "react";
import "./App.css";
import { BrowserRouter as Router, Route } from "react-router-dom";

import Login from "./components/Login";
import Register from "./components/Register";
import CreateBlog from "./components/CreateBlog";
import DisplayBlog from "./components/DisplayBlog";
import Home from "./components/Home";
import MyBlogs from "./components/MyBlogs";

function App() {
	return (
		<Router>
			<Route path="/" exact component={Home} />
			<Route path="/login" exact component={Login} />
			<Route path="/register" exact component={Register} />
			<Route path="/createBlog" exact component={CreateBlog} />
			<Route path="/displayBlog/:blogId" exact component={DisplayBlog} />
			<Route path="/myBlog" exact component={MyBlogs} />
		</Router>
	);
}

export default App;
