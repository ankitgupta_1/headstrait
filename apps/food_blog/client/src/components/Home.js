import React, { Component } from "react";
import Navbar from "./Navbar";
import TimeStamp from "react-timestamp";
import { Link } from "react-router-dom";

import "./css/Home.css";

import { getBlogs } from "../actions/blogAction";
import { getTags } from "../actions/tagAction";
import { connect } from "react-redux";

export class Home extends Component {
	componentWillMount() {
		this.props.getBlogs();
		this.props.getTags();
	}
	render() {
		return (
			<div>
				<Navbar />
				<div className="mt-40 pt-40 coverImg"></div>
				<div className="parentDiv">
					<div className="container">
						<h2 className="homePageTitle">
							Home For All Food Lovers
						</h2>
						<div className="blogsContainer">
							{this.props.blogs ? (
								this.props.blogs.map(blog => (
									<Link
										className="link"
										to={"/displayBlog/" + blog.blogId}
									>
										<div
											className="blogList p-20 mb-20"
											id={blog.blogId}
										>
											<h3 className="blogListTitle mt-0 mb-20">
												{blog.blogTitle}
											</h3>
											<h4 className="authorName">
												{blog.user.userName}
											</h4>
											<TimeStamp
												data={blog.postedOn}
												className="postedOn"
											/>
										</div>
									</Link>
								))
							) : (
								<div>
									<h4>Loading blogs......</h4>
								</div>
							)}
						</div>
						<div className="categoriesContainer p-20">
							<h4 className="categoryTitle mt-0 mb-10">
								Categories
							</h4>
							<hr />
							<div className="categories mt-20">
								{this.props.tags.map(tag => (
									<p
										id={tag.tagId}
										style={{ fontSize: "14px" }}
									>
										{tag.tagName}
									</p>
								))}
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => ({
	blogs: state.blogsReducer.blogs,
	tags: state.tagsReducer.tags
});

export default connect(
	mapStateToProps,
	{ getBlogs, getTags }
)(Home);
