import React, { Component } from "react";
import { connect } from "react-redux";
import { getBlog } from "../actions/blogAction";
import { getComments } from "../actions/commentAction";
import Navbar from "./Navbar";
import "./css/DisplayBlog.css";
import ReactHtmlParser from "react-html-parser";
import avatar from "./images/male_icon.svg";
import { Link } from "react-router-dom";

export class DisplayBlog extends Component {
	state = {
		inputFieldComment: "",
		likes: 0,
		dislikes: 0,
		isLikeChecked: false,
		isDislikeChecked: false,
		isCommentClicked: false
	};

	componentDidMount() {
		this.props.getBlog(this.props.match.params.blogId);
		this.props.getComments(this.props.match.params.blogId);
	}

	handleTextChange = e => {
		this.setState({ [e.target.name]: e.target.value });
	};

	onLikeClick = () => {
		this.setState({ isLikeChecked: !this.state.isLikeChecked });
		this.state.isLikeChecked
			? this.setState({ isDislikeChecked: true })
			: this.setState({ isDislikeChecked: false });
	};

	onDislikeClick = () => {
		this.setState({ isDislikeChecked: !this.state.isDislikeChecked });
		this.state.isDislikeChecked
			? this.setState({ isLikeChecked: true })
			: this.setState({ isLikeChecked: false });
	};

	onCommentClick = () => {
		this.setState({ isCommentClicked: !this.state.isCommentClicked });
	};

	render() {
		return (
			<>
				{this.props.blog ? (
					<div>
						<Navbar />
						<div className="mt-40 pt-40 parentDiv">
							<div className="blogContainer p-40">
								<h2 className="mt-0 mb-40">
									{this.props.blog.blogTitle}
								</h2>
								<div className="blogsCategories">
									<div className="tag p-10 ml-10">Indian</div>
									<div className="tag p-10 ml-10">
										Continental
									</div>
								</div>
								<div className="authorContainer mt-20">
									<img
										src={avatar}
										className="authorAvatar"
									/>
									<span className="authorBlogName ml-20">
										{this.props.blog.user.userName}
									</span>
								</div>
								<div className="blogDescription mt-40 mb-20">
									{ReactHtmlParser(
										this.props.blog.blogDescription
									)}
								</div>
								<div className="division"></div>
								<div className="icons my-20">
									<i
										class={
											this.state.isLikeChecked
												? "fas fa-thumbs-up"
												: "far fa-thumbs-up"
										}
										onClick={this.onLikeClick}
									></i>
									<i
										class={
											this.state.isDislikeChecked
												? "fas fa-thumbs-down"
												: "far fa-thumbs-down"
										}
										onClick={this.onDislikeClick}
									></i>
									<i
										class="far fa-comment-dots"
										onClick={this.onCommentClick}
									></i>
								</div>

								<form
									className="commentForm mb-20"
									onSubmit={this.onCommentClick}
									style={{
										display: this.state.isCommentClicked
											? "contents"
											: "none"
									}}
								>
									{localStorage.getItem("token") ? (
										<>
											<textarea
												type="text"
												name="inputFieldComment"
												className="inputField my-10"
												value={
													this.state.inputFieldComment
												}
												onChange={
													this.handleInputChange
												}
												placeholder="Add your comment"
												style={{
													resize: "none",
													height: "100px"
												}}
												required
											></textarea>
											<input
												name="submit"
												type="submit"
												value="Comment"
												className="btn btn-primary mb-10"
											/>
										</>
									) : (
										<Link className="link" to="/login">
											<button className="btn btn-primary mb-10">
												Login to Comment
											</button>
										</Link>
									)}
								</form>
								<div className="commentsList">
									<hr />
									{this.props.comments.map(comment => (
										<>
											<div>
												<p className="comment">
													{comment.comment}
												</p>
												<div
													style={{
														display: "flex",
														flexDirection: "row",
														alignItems: "center"
													}}
												>
													<img
														src={avatar}
														className="commentAvatar"
													/>
													<p className="commentUser ml-10">
														{comment.user.userName}
													</p>
												</div>
												<hr />
											</div>
										</>
									))}
								</div>
							</div>
						</div>
					</div>
				) : (
					console.log("do nothing")
				)}
			</>
		);
	}
}

const mapStateToProps = state => ({
	blog: state.blogsReducer.blog,
	comments: state.commentsReducer.comments
});

export default connect(
	mapStateToProps,
	{ getBlog, getComments }
)(DisplayBlog);
