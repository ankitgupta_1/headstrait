import React from "react";
import { shallow } from "enzyme";
import { CreateBlog } from "../CreateBlog";
import draftToHtml from "draftjs-to-html";
import { EditorState, convertToRaw } from "draft-js";

const tags = [{}, {}];
const getTags = jest.fn();
const getCurrentContent = jest.fn();
const createBlog = jest.fn();

const wrapper = shallow(
	<CreateBlog
		getTags={getTags}
		tags={tags}
		getCurrentContent={getCurrentContent}
		createBlog={createBlog}
	/>
);

describe("testing the presence of texts, form and submit button", () => {
	it("should have a title Create Blog, placeholder for title as Click to Add Blog Title and a button with text Create Blog", () => {
		expect(wrapper.find(".formTitle").text()).toBe("Create Blog");
		expect(wrapper.find("textarea").prop("placeholder")).toBe(
			"Click to Add Blog Title"
		);
		expect(wrapper.find('[type="submit"]').length).toBe(1);
		expect(wrapper.find('[type="submit"]').prop("value")).toBe(
			"Create Blog"
		);
	});
	it("should have tags of form, input type submit", () => {
		expect(wrapper.find("form").length).toEqual(1);
		expect(wrapper.find("input").prop("type"));
	});
});

describe("testing the state for input fields", () => {
	it("should have input value as in the state in the property inputFieldTitle", () => {
		const e = {
			target: {
				name: "inputFieldTitle",
				value: "Blog title"
			}
		};
		wrapper.instance().handleInputChange(e);
		expect(wrapper.state().inputFieldTitle).toBe(e.target.value);
	});
	it("should have input value as in the state in the property inputFieldDescription", () => {
		const e = {
			target: {
				name: "inputFieldDescription",
				value: "Blog title"
			}
		};
		wrapper.instance().handleInputChange(e);
		expect(wrapper.state().inputFieldDescription).toBe(e.target.value);
	});
});

describe("test for functions", () => {
	it("onEditorStateChange -> should have editor value as in the state in the property editorState", () => {
		const editorState = {};
		wrapper.instance().onEditorStateChange(editorState);
		expect(wrapper.state().editorState).toBe(editorState);
	});
	it("onCreateBlogClick  -> should call createBlog action with blog details and preventDefault event", () => {
		const e = { preventDefault: () => {} };
		jest.spyOn(e, "preventDefault");
		wrapper.find("form").simulate("submit", e);
		expect(e.preventDefault).toBeCalled();

		// const editorState = EditorState.createEmpty();

		// expect(draftToHtml).toBeCalledWith(
		// 	convertToRaw(editorState.getCurrentContent())
		// );

		const blogDetails = {
			blogTitle: "",
			blogDescription: "",
			user: 1,
			tags: [1, 2, 3, 4]
		};

		wrapper.find('[type="submit"]').simulate("click");
		expect(createBlog).toBeCalledWith(blogDetails);
	});
});
