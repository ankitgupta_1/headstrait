import React from "react";
import { shallow } from "enzyme";
import { DisplayBlog } from "../DisplayBlog";

const getBlog = jest.fn();
const getComments = jest.fn();

const blog = {
	blogId: 3,
	blogTitle: "First testing - Food blog",
	blogDescription:
		'<h2 style="text-align:center;">Here goes the heading</h2>\n<h3>1. Ingredients</h3>\n<ul>\n<li>raza</li>\n<li>ghee 1-1/2</li>\n</ul>\n<h3 style="text-align:center;"><span style="color: rgb(0,0,0);font-size: 18.72;font-family: Work Sans", sans-serif;"> <em>Enjoy your food</em></span><em> </em></h3>\n',
	likes: 0,
	dislikes: 0,
	postedOn: 1568904158671,
	comments: [
		{
			commentId: 1,
			comment: "This is so awesome",
			user: {
				userId: 1,
				userName: "Ankit Gupta",
				userPassword:
					"$2a$10$OtwmoMDt/hNs04Tp7JewQ.OaIOtFB1FlleNveJCKlZBF6Uev3yUrS",
				userEmail: "ankitgupta1697@gmail.com"
			}
		}
	],
	user: {
		userId: 1,
		userName: "Ankit Gupta",
		userPassword:
			"$2a$10$OtwmoMDt/hNs04Tp7JewQ.OaIOtFB1FlleNveJCKlZBF6Uev3yUrS",
		userEmail: "ankitgupta1697@gmail.com"
	},
	tags: []
};
const comments = [
	{
		commentId: 1,
		comment: "This is so awesome",
		user: {
			userId: 1,
			userName: "Ankit Gupta",
			userPassword:
				"$2a$10$OtwmoMDt/hNs04Tp7JewQ.OaIOtFB1FlleNveJCKlZBF6Uev3yUrS",
			userEmail: "ankitgupta1697@gmail.com"
		}
	},
	{
		commentId: 2,
		comment: "This is so awesome",
		user: {
			userId: 1,
			userName: "Ankit Gupta",
			userPassword:
				"$2a$10$OtwmoMDt/hNs04Tp7JewQ.OaIOtFB1FlleNveJCKlZBF6Uev3yUrS",
			userEmail: "ankitgupta1697@gmail.com"
		}
	}
];

const wrapper = shallow(
	<DisplayBlog
		match={{ params: { blogId: 3 }, isExact: true, path: "", url: "" }}
		getBlog={getBlog}
		getComments={getComments}
		blog={blog}
		comments={comments}
	/>
);

describe("checks for the sections are present to check the pattern", () => {
	it("checks for the blog's texts", () => {
		expect(wrapper.find("h2").length).toBe(2);
		expect(
			wrapper
				.find("h2")
				.at(0)
				.text()
		).toBe(blog.blogTitle);
	});
});
