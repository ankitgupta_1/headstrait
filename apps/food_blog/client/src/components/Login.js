import React, { Component } from "react";
import { loginUser } from "../actions/userAction";
import { connect } from "react-redux";

export class Login extends Component {
	state = {
		inputFieldEmail: "",
		inputFieldPassword: ""
	};

	handleTextChange = e => {
		this.setState({ [e.target.name]: e.target.value });
	};

	onLoginClick = e => {
		e.preventDefault();
		this.props.loginUser({
			userEmail: this.state.inputFieldEmail,
			userPassword: this.state.inputFieldPassword
		});
	};

	componentDidMount() {
		console.log(this.props);
		localStorage.getItem("token")
			? this.props.history.push("/")
			: console.log("do nothing");
	}

	render() {
		return (
			<div className="formParent">
				<form className="form p-40" onSubmit={this.onLoginClick}>
					<span className="formTitle mb-40">Login</span>
					<input
						name="inputFieldEmail"
						placeholder="Email"
						type="email"
						value={this.state.inputFieldEmail}
						className="inputField my-10"
						onChange={this.handleTextChange}
						required
					/>
					<input
						name="inputFieldPassword"
						placeholder="Password"
						type="password"
						pattern=".{6,30}"
						title="Password should be between 5 to 31 characters"
						value={this.state.inputFieldPassword}
						className="inputField my-10"
						onChange={this.handleTextChange}
						required
					/>
					<input
						name="submit"
						type="submit"
						value="Login"
						className="btn btn-primary my-10"
					/>
					<span className="textLink my-20">Forgot Password</span>
					<span>
						Not a user ?{" "}
						<span className="textLink">Register Now</span>
					</span>
				</form>
			</div>
		);
	}
}

const mapStateToProps = state => ({
	users: state.usersReducer.users
});

export default connect(
	mapStateToProps,
	{ loginUser }
)(Login);
