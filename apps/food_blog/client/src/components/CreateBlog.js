import React, { Component } from "react";
import Navbar from "./Navbar";
import { connect } from "react-redux";
import { createBlog, getBlogs } from "../actions/blogAction";
import { getTags } from "../actions/tagAction";
import "./css/CreateBlog.css";
import { EditorState, convertToRaw } from "draft-js";
import { Editor } from "react-draft-wysiwyg";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import draftToHtml from "draftjs-to-html";

export class CreateBlog extends Component {
	state = {
		inputFieldTitle: "",
		inputFieldDescription: "",
		tags: [],
		editorState: EditorState.createEmpty()
	};

	onEditorStateChange = editorState => {
		this.setState({ editorState });
	};

	handleInputChange = e => {
		const { id } = e.target;
		this.props.tags.map(tag =>
			tag.tagId == id
				? (tag.isChecked = !tag.isChecked)
				: (tag.isChecked = tag.isChecked)
		);
		this.setState({ [e.target.name]: e.target.value });
	};

	onCreateBlogClick = e => {
		e.preventDefault();
		const blogDetails = {
			blogTitle: this.state.inputFieldTitle,
			blogDescription: draftToHtml(
				convertToRaw(this.state.editorState.getCurrentContent())
			),
			user: 1,
			tags: [1, 2, 3, 4]
		};
		this.props.createBlog(blogDetails);
	};

	componentWillMount() {
		this.props.getTags();
	}

	render() {
		return (
			<div>
				<Navbar />
				<div className="mt-40 pt-40 parentDiv">
					<span className="formTitle mb-40">Create Blog</span>
					<form
						className="blogForm p-40"
						onSubmit={this.onCreateBlogClick}
					>
						<textarea
							// type="text"
							name="inputFieldTitle"
							className="addTitleInput m-10 "
							value={this.state.inputFieldTitle}
							onChange={this.handleInputChange}
							placeholder="Click to Add Blog Title"
						>
							{this.state.inputFieldTitle}
						</textarea>

						<div className="editorParent">
							<Editor
								editorClassName="customEditor"
								editorState={this.state.editorState}
								onEditorStateChange={this.onEditorStateChange}
								toolbar={{
									options: [
										"inline",
										"blockType",
										"fontSize",
										"list",
										"textAlign",
										"link"
									],
									inline: {
										inDropdown: true
									},
									blockType: {
										inDropdown: true,
										options: [
											"Normal",
											"H1",
											"H2",
											"H3",
											"H4",
											"H5",
											"H6",
											"Blockquote"
										]
									},
									fontSize: {
										inDropdown: true,
										icon: 16,
										options: [
											8,
											9,
											10,
											11,
											12,
											14,
											16,
											18,
											24,
											30,
											36,
											48
										]
									},
									list: {
										inDropdown: true
									},
									textAlign: {
										inDropdown: true
									},
									link: {
										inDropdown: true
									}
								}}
							/>
						</div>
						<input
							name="submit"
							type="submit"
							value="Create Blog"
							className="btn btn-primary my-10"
						/>
					</form>
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => ({
	blogs: state.blogsReducer.blogs,
	tags: state.tagsReducer.tags
});

export default connect(
	mapStateToProps,
	{ createBlog, getTags, getBlogs }
)(CreateBlog);
