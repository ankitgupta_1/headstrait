import React, { Component } from "react";
import { getBlogsByUser } from "../actions/blogAction";
import { connect } from "react-redux";
import decode from "jwt-decode";
import TimeStamp from "react-timestamp";
import { Link } from "react-router-dom";
import Navbar from "./Navbar";
import "./css/MyBlogs.css";

export class MyBlogs extends Component {
	componentDidMount() {
		const decodedToken = decode(localStorage.getItem("token"));
		localStorage.getItem("token")
			? this.props.getBlogsByUser(decodedToken.userId)
			: this.props.history.push("/login");
	}
	render() {
		return (
			<div>
				<Navbar />
				<div className="parentDiv mt-40 pt-40">
					<div className="myBlogs">
						{this.props.blogs ? (
							this.props.blogs.map(blog => (
								<Link
									className="link"
									to={"/displayBlog/" + blog.blogId}
								>
									<div
										className="blogList p-20 mb-20"
										id={blog.blogId}
									>
										<h3 className="blogListTitle mt-0 mb-20">
											{blog.blogTitle}
										</h3>
										<TimeStamp
											data={blog.postedOn}
											className="postedOn"
										/>
									</div>
								</Link>
							))
						) : (
							<div>
								<h4>Loading blogs......</h4>
							</div>
						)}
					</div>
				</div>
			</div>
		);
	}
}

const mapStateToProps = state => ({
	blogs: state.blogsReducer.blogs
});

export default connect(
	mapStateToProps,
	{ getBlogsByUser }
)(MyBlogs);
