import React, { Component } from "react";
import { connect } from "react-redux";
import { registerUser } from "../actions/userAction";

export class Register extends Component {
	state = {
		inputFieldName: "",
		inputFieldEmail: "",
		inputFieldPassword: "",
		inputFieldConfirmPassword: ""
	};

	handleTextChange = e => {
		this.setState({ [e.target.name]: e.target.value });
	};

	componentDidMount() {
		console.log(this.props);
		localStorage.getItem("token")
			? this.props.history.push("/")
			: console.log("do nothing");
	}

	onRegisterClick = () => {
		if (
			this.state.inputFieldPassword ===
			this.state.inputFieldConfirmPassword
		)
			this.props.registerUser({
				userName: this.state.inputFieldName,
				userEmail: this.state.inputFieldEmail,
				userPassword: this.state.inputFieldPassword
			});
	};

	render() {
		return (
			<div className="formParent">
				<form className="form p-40" onSubmit={this.onRegisterClick}>
					<span className="formTitle mb-40">Register</span>
					<input
						name="inputFieldName"
						placeholder="Full Name"
						type="text"
						value={this.state.inputFieldName}
						className="inputField my-10"
						onChange={this.handleTextChange}
						required
					/>
					<input
						name="inputFieldEmail"
						placeholder="Email"
						type="email"
						value={this.state.inputFieldEmail}
						className="inputField my-10"
						onChange={this.handleTextChange}
						required
					/>
					<input
						name="inputFieldPassword"
						placeholder="Password"
						type="password"
						pattern=".{6,30}"
						title="Password should be between 5 to 31 characters"
						value={this.state.inputFieldPassword}
						className="inputField my-10"
						onChange={this.handleTextChange}
						required
					/>
					<input
						name="inputFieldConfirmPassword"
						placeholder="Confirm Password"
						type="password"
						pattern=".{6,30}"
						title="Confirm Password doesnt match"
						value={this.state.inputFieldConfirmPassword}
						className="inputField my-10"
						onChange={this.handleTextChange}
						required
					/>
					<input
						name="submit"
						type="submit"
						value="Register"
						className="btn btn-primary my-10"
					/>
					<span>
						Already a user ?{" "}
						<span className="textLink my-20">Login</span>
					</span>
				</form>
			</div>
		);
	}
}

const mapStateToProps = state => ({
	loggedInUser: state.usersReducer.loggedInUser
});

export default connect(
	mapStateToProps,
	{ registerUser }
)(Register);
