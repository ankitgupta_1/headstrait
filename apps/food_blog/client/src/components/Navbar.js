import React, { Component } from "react";
import "./css/Navbar.css";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

export class Navbar extends Component {
	render() {
		return (
			<div className="navParent">
				<nav>
					<span className="navBrand">Curry Tales</span>
					<div className="navLinks">
						<ul>
							{localStorage.getItem("token") ? (
								<>
									<Link className="link" to="/myBlog">
										<li>My Blogs</li>
									</Link>
									<Link className="link" to="/createBlog">
										<li>Create Blog</li>
									</Link>
									<li>{this.props.loggedInUser.userName}</li>
								</>
							) : (
								<>
									<Link className="link" to="/login">
										<li>Login</li>
									</Link>
									<Link className="link" to="/register">
										<li>Register</li>
									</Link>
								</>
							)}
						</ul>
					</div>
				</nav>
			</div>
		);
	}
}

const mapStateToProps = state => ({
	loggedInUser: state.usersReducer.loggedInUser
});

export default connect(mapStateToProps)(Navbar);
