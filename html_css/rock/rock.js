draw = [];
system = [];
user = [];
systemArray = ['rock', 'paper', 'scissors'];
systemTotal = 0;
userTotal = 0;
drawTotal = 0;
userChoice = "";
systemChoice = "";
ids = ['round_', 'user_', 'system_', 'result_'];
rounds = 0;

function compare(choice) {

    rounds++;
    userChoice = choice.value;
    systemChoice = systemArray[Math.floor(Math.random() * 3)];
    if (userChoice == systemChoice) {
        draw[rounds] = 1;
        system[rounds] = 0;
        user[rounds] = 0;
        drawTotal += draw[rounds];
        addScore("Draw");
    }
    else {
        if (userChoice == 'rock') {
            if (systemChoice == 'paper') {
                system[rounds] = 1;
                user[rounds] = 0;
                draw[rounds] = 0;
                systemTotal += system[rounds];
                addScore("Computer Won");
            }
            else {
                system[rounds] = 0;
                user[rounds] = 1;
                draw[rounds] = 0;
                userTotal += user[rounds];
                addScore("You Won");
            }
        }
        else if (userChoice == 'paper') {
            if (systemChoice == 'scissors') {
                system[rounds] = 1;
                user[rounds] = 0;
                draw[rounds] = 0;
                systemTotal += system[rounds];
                addScore("Computer Won");
            }
            else {
                system[rounds] = 0;
                user[rounds] = 1;
                draw[rounds] = 0;
                userTotal += user[rounds];
                addScore("You Won");
            }
        }
        else {
            if (systemChoice == 'rock') {
                system[rounds] = 1;
                user[rounds] = 0;
                draw[rounds] = 0;
                systemTotal += system[rounds];
                addScore("Computer Won");
            }
            else {
                system[rounds] = 0;
                user[rounds] = 1;
                draw[rounds] = 0;
                userTotal += user[rounds];
                addScore("You Won");
            }
        }
    }
    if (rounds == 10) {
        for (i = 0; i < 3; i++) {
            document.getElementById(systemArray[i]).style.display = "none";
        }

        document.getElementById("resetButton").style.display = "block";
        document.getElementById("currentScore").style.display = "none";
        document.getElementById("finalResults").style.display = "block";

        if (userTotal > systemTotal) {
            message = "Congratulations you won the game by <span>" + userTotal + " - " + systemTotal + "</span";
            showResults(message);
        }
        else if (userTotal == systemTotal) {
            message = "Game was drawn by <span>" + userTotal + " - " + systemTotal + "</span";
            showResults(message);
        }
        else {
            message = "You lose the game by <span>" + userTotal + " - " + systemTotal + "</span";
            showResults(message);
        }
    }
}

function addScore(result) {
    parent = document.getElementById("playHistory");
    parent_2 = document.createElement("div");
    parent_2.setAttribute("class", "historyScore");
    parent_2.setAttribute("id", "historyScore_" + rounds);
    parent_2 = parent.appendChild(parent_2);

    for (i = 0; i < 4; i++) {
        child = document.createElement("span");
        parent_2.appendChild(child).setAttribute("id", ids[i] + rounds);
    }

    document.getElementById("round_" + rounds).innerHTML = rounds;
    document.getElementById("user_" + rounds).innerHTML = userChoice;
    document.getElementById("system_" + rounds).innerHTML = systemChoice;
    document.getElementById("result_" + rounds).innerHTML = result;
    document.getElementById("userScore").innerHTML = userTotal;
    document.getElementById("systemScore").innerHTML = systemTotal;
}

function reset() {
    for (i = 0; i < 3; i++) {
        document.getElementById(systemArray[i]).style.display = "block";
    }
    for (i = 1; i <= 10; i++) {
        child = document.getElementById("historyScore_" + i);
        document.getElementById("playHistory").removeChild(child);
    }
    document.getElementById("resetButton").style.display = "none";

    rounds = 0;
    userTotal = 0;
    systemTotal = 0;
    document.getElementById("userScore").innerHTML = userTotal;
    document.getElementById("systemScore").innerHTML = systemTotal;
    document.getElementById("currentScore").style.display = "normal";
    document.getElementById("finalResults").style.display = "none";
}

function showResults(message) {
    console.log(message);
    document.getElementById("finalResults").innerHTML = message;

}