balls = 0;
over = 0;
runs = 0;
batsman = 2;
ball_no = 0
extras = 0;
extras_run = [1, 2, 3, 5, 7];
wicket = 0;
striker = "Batsman_1";
non_striker = "Batsman_2";

function start_match() {

    if (over != 5) {

        if (wicket != 10) {

            random = Math.floor(Math.random() * 9);

            if (random == 8) {
                wicket++;
                batsman++;
                balls++;
                ball_no = balls % 6;
                striker = "Batsman_" + batsman;
                if (ball_no == 0) {
                    [striker, non_striker] = swap();
                }
            }

            else if (random == 7) {
                extras_random = extras_run[Math.floor(Math.random() * extras_run.length)];
                extras = extras + extras_random;
                runs = runs + extras_random
                    ;
                if (extras == 2) {
                    [striker, non_striker] = swap();
                }
            }

            else if (random != 5) {
                runs = runs + random;
                balls++;
                ball_no = balls % 6;
            }

            if (ball_no == 0 && random != 5 && random != 7) {
                over++;
            }

            if (ball_no == 0 && (random == 0 || random == 2 || random == 4 || random == 6)) {
                [striker, non_striker] = swap();
            }
            else if (ball_no != 0 && (random == 1 || random == 3)) {
                [striker, non_striker] = swap();
            }

        }
        if (random != 5) {
            document.getElementById("runs").innerHTML = runs;
            document.getElementById("wickets").innerHTML = wicket;
            document.getElementById("over").innerHTML = over;
            document.getElementById("ball_no").innerHTML = ball_no;
            document.getElementById("extras").innerHTML = extras;
            document.getElementById("striker").innerHTML = striker;
            document.getElementById("non-striker").innerHTML = non_striker;

            console.log("              ");
            console.log("***********************")
            console.log(runs + " - " + wicket + " (" + over + "." + ball_no + ")");
            console.log("Extras: " + extras);
            console.log("Striker: " + striker);
            console.log("Non-striker: " + non_striker);
            console.log("Random: " + random);
        }
        setTimeout(start_match, 1000);

    }

}

function swap() {
    temp = striker;
    striker = non_striker;
    non_striker = temp;
    return [striker, non_striker];
}

function disable() {
    document.getElementById("start").setAttribute("disabled", "true");
    start_match();
}